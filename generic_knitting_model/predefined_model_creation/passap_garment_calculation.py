#!/usr/bin/env python
# coding:utf-8
"""

 This file is part of SoftKnit21 framework

License
=======

 - B{SofKnit21} (U{http://www.softkni21.org}) is Copyright:
  - (C) Odile.Troulet-Lambert@orange.fr

This program is free software; you can redistribute it and/or modify
it under the terms of the Affero GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
Affero GNU General Public License for more details.

You should have received a copy of the Affero GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http://www.gnu.org/licenses/gpl.html}

Module purpose
==============
contains classes for kpart knitting model

Implements
==========


Usage
=====


Documentation
=============


Usage
=====


@author: Odile Troulet-Lambert
@copyright: Odile Troulet-Lambert
@license: AGPL

"""
import generic_knitting_model.kpart_model as kpm
from svgpathtools import Line


class PASSAPBodyMeasurement:
     """ contains the body measurement for a sweater"""

     PASSAP_FRENCH_SIZES = [
         [(38, 8), 82, 62, 84, 38, 34, 56, 50, 30, 18],
         [(40, 9), 82, 62, 86, 38, 35, 57, 51, 32, 18],
         [(42, 10), 84, 64, 88, 39, 36, 57, 52, 34, 20],
         [(44, 11), 84, 68, 90, 40, 37, 58, 53, 35, 20],
         [(46, 12), 88, 72, 94, 41, 38, 59, 54, 36, 21],
         [(48, 13), 92, 78, 98, 42, 39, 60, 55, 37, 21],
         [(50, 14), 100, 84, 104, 43, 40, 60, 56, 39, 22],
         [(52, 15), 110, 92, 110, 44, 42, 62, 58, 42, 23],
     ]
     SHOULDER_BIAIS = {'baby': 20, 'lady': 40, 'child': 30}
     ARMHOLE_ROUND = {'lady': 40, 'man': 40, 'child': 30, 'baby': 20, 'adult': 40}

     def __init__(self, size):
          """returns the line corresponding to the requested size multiplied by 10 because
          measurements are in cm and KPartModel is expressed in mm"""
          index = next(i for i, numlist in
                       enumerate(PASSAPBodyMeasurement.PASSAP_FRENCH_SIZES) if size in numlist[0])
          measurements = PASSAPBodyMeasurement.PASSAP_FRENCH_SIZES[index]
          measurements_in_mm = [n * 10 for n in measurements[1:]]

          self.hips, self.waist, self.chest, self.frontNeckToWaist, self.shoulders, self.total_length, \
              self.arm_length, self.upperArmCircle, self.wrist = measurements_in_mm


def trPoint(point):
     """ it is esaier to calculate the pattern with 0,0 coordinate in the lower left corner.
     svg supposes O point is in the upper left corner
     thus make negative all coordinates
     also KPartModel is defined in mms because PASSAP measurements are in mms multiply by 10"""
     return complex(point.real, -point.imag)


class PassapBuiltInArmSweater(kpm.KPartModel):

     def __init__(self, size, wearer, ease):
          """ create the pieces for a predefined sweater with built-in-sleeves"""

          self.m = PASSAPBodyMeasurement(size)
          self.wearer = wearer
          self.ease = ease
          self.shoulder_biais = self.m.SHOULDER_BIAIS.get(wearer, self.m.SHOULDER_BIAIS['lady'])
          back = self.compute_back()
          kwargs = {'modelName': self.__class__.__name__,
                    'kpPiecesList': [back, ]
                    }
          super().__init__(**kwargs)

     def compute_armHole_height(self):
          height = (self.m.frontNeckToWaist - self.shoulder_biais) / 2
          if not (160 < height < 220) and (self.wearer == "adult" or self.wearer == "lady"):
               othertry = self.m.shoulders / 2 - 1
               if 160 < othertry < 220:
                    return othertry, None
               elif height < 160:
                    return 160, None
               elif height > 220:
                    return 220, height - 220
          return height, None

     def compute_back(self):
          armHole_height, extra = self.compute_armHole_height()
          height_to_armHole = self.m.total_length - armHole_height - self.shoulder_biais
          if extra:
               height_to_armHole += extra
          width_under_armHole = (self.m.chest + self.ease) / 2
          extra_width_under_armHole = (width_under_armHole - (self.m.hips / 2)) / 2
          armHole_depth = (width_under_armHole - self.m.shoulders) / 2
          armHole_round_height = self.m.ARMHOLE_ROUND.get(self.wearer, self.m.ARMHOLE_ROUND['adult'])
          """base0 is the bottom of the sweater"""
          self.base0 = base0 = Line(start=complex(0, 0),
                                    end=complex(self.m.hips / 2, 0))
          """base1 is the horizontal line before armhole"""
          self.base1 = base1 = Line(start=complex(- extra_width_under_armHole, height_to_armHole),
                                    end=complex(self.m.hips / 2 + extra_width_under_armHole, height_to_armHole))
          neckbase = self.m.shoulders / 3

          self.end_armHole_round = end_armHole_round = Line(start=complex(base1[0].real + armHole_depth, base1.start.imag + armHole_round_height),
                                                            end=complex(base1[1].real - armHole_depth, base1.start.imag + armHole_round_height)
                                                            )
          self.shoulder = shoulder = Line(start=complex(end_armHole_round.start.real, base1.start.imag + armHole_height),
                                          end=complex(end_armHole_round.end.real, base1.start.imag + armHole_height)
                                          )
          self.neck = neck = Line(start=complex(shoulder.start.real + neckbase, shoulder.start.imag + self.shoulder_biais),
                                  end=complex(shoulder.end.real - neckbase, shoulder.end.imag + self.shoulder_biais)
                                  )
          kpart0_dic = {'BaseSeg': kpm.BaseSeg(start=trPoint(base0.start), end=trPoint(base0.end)),
                        'partNumber': 0,
                        'nextsnum': [1],
                        'LeftBordersList': [kpm.LineBorder(start=trPoint(base0.start), end=trPoint(base1.start)),
                                            kpm.LineBorder(start=trPoint(base1.start), end=trPoint(end_armHole_round.start)),
                                            kpm.VerticalBorder(start=trPoint(end_armHole_round.start), end=trPoint(shoulder.start)),
                                            kpm.LineBorder(start=trPoint(shoulder.start), end=trPoint(neck.start))
                                            ],
                        'RightBordersList': [kpm.LineBorder(start=trPoint(base0.end), end=trPoint(base1.end)),
                                             kpm.LineBorder(start=trPoint(base1.end), end=trPoint(end_armHole_round.end)),
                                             kpm.VerticalBorder(start=trPoint(end_armHole_round.end), end=trPoint(shoulder.end)),
                                             kpm.LineBorder(start=trPoint(shoulder.end), end=trPoint(neck.end)),
                                             ]
                        }

          kpart1_dic = {'BaseSeg': kpm.BaseSeg(start=trPoint(neck.start), end=trPoint(neck.end)),
                        'partNumber': 1,
                        'previousnum': [0],
                        }

          kpart0 = kpm.CastOnPartSeg(**kpart0_dic)
          kpart1 = kpm.CastOffPartSeg(**kpart1_dic)
          back = kpm.KpPiece(**{"pieceName": 'back',
                                "kPartsList": [kpart0, kpart1]
                                })

          return back


if __name__ == '__main__':
     import unittest

     from common.tools_for_log import logconf

     mlog = logconf('__name__', test=True)

     from pathlib import Path
     current = Path(__file__)
     tests_dir = Path(current.parent, 'tests')
     loader = unittest.TestLoader()
     tests_suite = loader.discover(tests_dir, pattern='tests_' + current.stem + '*.py')
     testRunner = unittest.runner.TextTestRunner()
     testRunner.run(tests_suite)




