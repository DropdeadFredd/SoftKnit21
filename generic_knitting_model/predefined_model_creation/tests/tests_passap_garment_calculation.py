#!/usr/bin/env python
# coding:utf-8
"""  SoftKnit21 framework

License
=======

 Copyright (C) <2019>  <Odile Troulet-Lamberttex@orange.fr>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or  any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.



Module purpose
==============

tests for PASSAP predefined garments

Implements
==========


Documentation
=============


Usage
=====


@author: Odile Troulet-Lambert
@copyright: Odile Troulet-Lambert
@license: GPL
"""
from pathlib import Path
import unittest

import svgpathtools as svp
from generic_knitting_model.tests.tests_super_class import Test_super_class
import generic_knitting_model.helpers as helpers
from generic_knitting_model.predefined_model_creation.passap_garment_calculation import PassapBuiltInArmSweater
import generic_knitting_model.kpart_model as kpm


# @unittest.skip
class Test_PassapBuiltInArmSweater (Test_super_class, unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.path = super().get_tests_datadir(cls, __file__)

    # @unittest.skip
    def test_back_creation(self):
        sweater = PassapBuiltInArmSweater(14, 'lady', 60)
        self.assertTrue(isinstance(sweater, kpm.KPartModel))
        todisplay = sweater.kpPiecesList['back'].draw()

        helpers.visualize_path(todisplay, Path(self.path, 'back_with_bases_piece_drawn.svg'),
                               )

    ## @ unittest.skip
    #def test_back_draw(self):

        #back_path = self.sweater.pieces[0].draw_piece()
        #self.assertTrue(back_path.iscontinuous())
        #self.assertTrue(back_path.isclosed())
        #visualize_path(back_path, Path(self.path, 'back_draw_piece.svg'))


if __name__ == '__main__':
    import unittest

    from commons.tools_for_log import logconf

    mlog = logconf('__name__', test=True)

    unittest.main()
