#!/usr/bin/env python
# coding:utf-8
"""

 This file is part of SoftKnit21 framework

License
=======

 - B{SofKnit21} (U{http://www.softkni21.org}) is Copyright:
  - (C) Odile.Troulet-Lambert@orange.fr

This program is free software; you can redistribute it and/or modify
it under the terms of the Affero GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
Affero GNU General Public License for more details.

You should have received a copy of the Affero GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http://www.gnu.org/licenses/gpl.html}

Module purpose
==============
contains classes describing a generic knitting model
Implements
==========


Usage
=====
instance of generic knitting model are created either by using the module predefined model creation which produces a dict
or by decoding an xml model description using the kpart_model_xml_decode module

Documentation
=============


@author: Odile Troulet-Lambert
@copyright: Odile Troulet-Lambert
@license: AGPL

"""

import svgpathtools as svp
import cmath
import xmlschema as xs

import generic_knitting_model.helpers as helpers


def print_path(path):
    """ print the elements of a svg path"""
    for i in range(len(path._segments) - 1):
        if path._segments[i].end != path._segments[i + 1].start:
            print(f'segment {i} start {path._segments[i].end}, {path._segments[i+1].start}')


def add_paths(path1, path2):
    """concat 2 paths which are contiguous"""
    if path1.end == path2.start:
        return svp.concatpaths([path1, path2])
    if path1.start == path2.start:
        return svp.concatpaths([path2.reversed(), path1])
    if path1.start == path2.end:
        return svp.concatpaths([path2, path1])
    if path1.end == path2.end:
        return svp.concatpaths([path1, path2.reversed()])
    raise ValueError(f'path1 {path1} and path2 {path2} are not contiguous')


class KnitBorder:

    """ a border of a knitPart;

    @seg : path of the border
    """

    def __init__(self, **kwargs):
        cls = kwargs.pop('class')
        self.seg = cls(**kwargs)

    #@classmethod
    #def get_parent(cls):
        #return ['LeftBordersList', 'RightBordersList']

    #@classmethod
    #def get_mainattr(cls):
        #return 'None'

    def __repr__(self):
        try:
            k = self.kpart
            return f'segment {self.seg.__class__.__name__} in {self.__class__.__name__} in kpart {k}'
        except AttributeError:
            return f'segment {self.seg.__class__.__name__} in {self.__class__.__name__}'


class LineBorder (KnitBorder):

    """ a KnitBorder whose geometric form is a line; """

    def __init__(self, **kwargs):
        cls = vars(svp)['Line']
        kwargs.update({'class': cls})
        super().__init__(**kwargs)


class VerticalBorder (LineBorder):
    """ a knit border which is vertical"""
    pass


class HorIncreaseBorder (LineBorder):
    """a knitBorder whose geometric form is a horizontal line and which is added to a side
    """
    pass


class HorDecreaseBorder (LineBorder):
    """a knitBorder whose geometric form is a horizontal line and which is casted off on the side
    """
    pass


class HorHoldBorder (LineBorder):
    """ a knit Border whose geometry is a horizontal line and which requires a particular treatment before starting an adjacent  part"""
    pass


class HorJoinBorder (LineBorder):
    """ a knit Border whose geometry is a horizontal line and which requires a particular treatment before starting knitting the next part
    """
    pass


class CurveBorder (KnitBorder):

    """ a KnitBorder whose geometric function is a curve;
        @seg: the curve describing the border
        """
    pass


class ArcBorder (CurveBorder):

    """ a KnitBorder whose geometric function is an arc;
        @seg: the curve describing the border
        """

    def __init__(self, **kwargs):
        args = ['start', 'radius', 'rotation', 'large_arc', 'sweep', 'end']
        clean_kwargs = {key: value for key, value in kwargs.items() if key in args}
        cls = vars(svp)['Arc']
        clean_kwargs.update({'class': cls})
        super().__init__(**clean_kwargs)


class CubicBezierBorder (CurveBorder):

    """ a KnitBorder whose geometric function is a CubicBezier;
        @seg: the curve describing the border
        """

    def __init__(self, **kwargs):
        args = ['start', 'end', 'control1', 'control2']
        clean_kwargs = {key: value for key, value in kwargs.items() if key in args}
        cls = vars(svp)['CubicBezier']
        clean_kwargs.update({'class': cls})
        super().__init__(**clean_kwargs)


class QuadraticBezierBorder (CurveBorder):

    """ a KnitBorder whose geometric function is a quadraticBezier;
        @seg: the curve describing the border
        """

    def __init__(self, **kwargs):
        args = ['start', 'end', 'control']
        clean_kwargs = {key: value for key, value in kwargs.items() if key in args}
        cls = vars(svp)['QuadraticBezier']
        clean_kwargs.update({'class': cls})
        super().__init__(**clean_kwargs)


class BordersList (list):

    def __init__(self, borders):
        super().__init__(borders)

    #@classmethod
    #def get_parent(cls):
        #return LeftBordersList.get_parent()

    #@classmethod
    #def get_mainattr(cls):
        #return None


class LeftBordersList(BordersList):
    """ a left borders of a KPart made of a list of KnitBorders"""
    pass


class RightBordersList(BordersList):
    """ right borders of a kPart made of a list of KnitBorders"""
    pass


class KPart:
    """ a knitting Part described by its base (1 or 2 points , and 2 KnitBorders for left and right sides)
    aKPart is defined in a coordinate system where ys are expressed in mms and the 0 is in the lower left corners.
    because svgcoordinates system has it point 0 in the upper left corner ys are all negatives.

    @base : the base on which the KPart starts being knitted
    @type : Point object or list of 2 Point objects

    @bordersList : the KnitBorders which make the sides of the part
    @type : list of 2 decreasing ys ordered list of KnitBorders

    @number : the number of the part which uniquely identifies it within the piece
    @type : integer

    @previousParts : preceding part(s)(knitted below)
    @type : list of Part instances

    @nextParts ; following part(s)(knitted above)
    @type : list of Part instances

    @height : maximum height of the part that is max of start of first pathPoint to yHoriz of endsing fir keft and right side
    @type : float

    @piecename : piece name
    @type : string
    """

    def __init__(self, **kwargs):
        leftborders = kwargs.get('LeftBordersList', [])
        rightborders = kwargs.get('RightBordersList', [])
        self.bordersList = [leftborders, rightborders]
        self.nextsnum = kwargs.get('nextsnum', [])
        self.previousnum = kwargs.get('previousnum', [])
        self.base = kwargs.get('BaseSeg', None) or kwargs.get('BasePoint', None)
        self.basePoints = [self.base.start, self.base.end] if isinstance(self.base, BaseSeg) else [self.base.start, None]
        self.partNumber = kwargs.get('partNumber')
        for bord in leftborders + rightborders:
            bord.kpart = self.partNumber
        assert self.base is not None
        assert self.partNumber is not None

    def configure(self, kpl):
        self.nexts = [kpl[i] for i in self.nextsnum]
        self.previous = [kpl[i] for i in self.previousnum]

    @ property
    def endPoints(self):
        try:
            return (self.bordersList[0][-1].seg.end, self.bordersList[1][-1].seg.end)
        except IndexError:
            raise IndexError

    def is_contiguous(self, path):
        return (path.start in self.endPoints or
                path.end in self.endPoints or
                path.start in self.basePoints or
                path.end in self.basePoints)

    @property
    def height(self):
        return self.basePoints[0].imag - self.endPoints[0].imag

    def __lt__(self, other):
        """ returns True if self to the left of other"""
        s1, s2 = self.basePoints
        o1, o2 = other.basePoints
        if not cmath.isclose(s1.imag, o1.imag, abs_tol=helpers.CMATH_ABS_TOLERANCE) or not s2 or not o2:
            # look for enpoints
            s1, s2 = self.endPoints
            o1, o2 = other.endPoints
            if not cmath.isclose(s1.imag, o1.imag, abs_tol=helpers.CMATH_ABS_TOLERANCE) or not s2 or not o2:
                raise NotImplementedError
        if s2.real <= o1.real:
            return True
        else:
            return False

    #def part_from_number(self, number):
        #if number == self.partNumber:
            #return self
        #else:
            #return self.kPartsList[number]

    def __repr__(self):
        return str(self.partNumber)

    def to_path(self):
        """ this method is overwritten for all CastonPart"""
        raise NotImplementedError

    def add_to_path(self, path):
        """ add this part to the path : adds the leftborder to the beginning of the path
        if possible add the right border to the path
        if not possible, return the right border to put it in stack
        """

        stack = []
        """ some parts have borders whose start == end"""
        if self.basePoints[0] == self.endPoints[0]:
            return path, []
        right = left = False
        leftb = svp.Path(*[b.seg for b in self.bordersList[0]])
        rightb = svp.Path(*[b.seg for b in self.bordersList[1]])
        if path.start in self.basePoints or path.end in self.basePoints:
            if path.start == self.basePoints[0]:
                path = svp.concatpaths([leftb.reversed(), path])
                left = True
            if path.start == self.basePoints[1]:
                path = svp.concatpaths([rightb, path])
                right = True
            if path.end == self.basePoints[0]:
                path = svp.concatpaths([path, leftb.reversed()])
                left = True
            if path.end == self.basePoints[1]:
                path = svp.concatpaths([path, rightb])
                right = True
            if not right:
                return path, rightb
            if not left:
                return path, leftb
            return path, []
        if path.start in self.endPoints or path.end in self.endPoints:
            leftp, rightp = self.endPoints
            if path.start == rightp:
                return svp.concatpaths([rightb, path]), leftb
            if path. start == leftp:
                return svp.concatpaths([leftb.reversed(), path]), rightb
            if path.end == rightp:
                return svp.concatpaths([path, rightb.reversed()]), leftb
            if path.end == leftp:
                return svp.concatpaths([path, leftb.reversed()]), rightb

    #@classmethod
    #def get_parent(cls):
        #return 'KPartsList'

    #@classmethod
    #def get_mainattr(cls):
        #return 'partNumber'


class BaseSeg:
    """create a BaseSeg object"""

    def __init__(self, **kwargs):
        self.start = kwargs['start']
        self.end = kwargs['end']

    #@classmethod
    #def get_parent(cls):
        #""" parent is a subclass of a KPart like left and rightborders"""
        #classes = LeftBordersList.get_parent()
        #return [c for c in classes if not 'Point' in c]

    #@classmethod
    #def get_mainattr(cls):
        #return 'start'


class BasePoint:

    def __init__(self, **kwargs):
        self.start = kwargs['start']

    #@classmethod
    #def get_parent(cls):
        #""" parent is a subclass of a KPart whose name does not contain point"""
        #classes = LeftBordersList.get_parent()
        #return [c for c in classes if 'Point' in c]

    #@classmethod
    #def get_mainattr(cls):
        #return 'start'


class CastOffPart (KPart):
    """ a KPart containing a castOff
    leftborders and right Borders are None
    no part will be built on top of a castOff part"""

    @property
    def endPoints(self):
        return self.basePoints

    @property
    def height(self):
        return 0

    def add_to_path(self, path):
        """ add the base to the path"""
        if len(self.basePoints) == 1:
            return path, []
        base = svp.Path(svp.Line(start=self.basePoints[0], end=self.basePoints[1]))
        try:
            assert path.start == base.start
            return svp.concatpaths([base.reversed(), path]), []
        except AssertionError:
            try:
                assert path.start == base.end
                return svp.concatpaths([base, path]), []
            except:
                return path, base


class CastOffPartSeg (CastOffPart):
    """ a castoff part containing an horizontal segment in the middle"""

    pass


class CastOffPartPoint (CastOffPart):
    """ a castoff part terminating with a point"""

    pass


class CastOnPart (KPart):

    """ a KPart which starts with a castOn and continues with borders
    returns path after adding self drawing either before or after path
    If no common point return 2 paths"""

    def add_to_path(self, path):
        left, right = self.endPoints
        mypath = self.to_path()
        try:
            assert path.end == left
            newpath = svp.concatpaths([path, mypath])
            return newpath, []
        except AssertionError:
            try:
                assert path.start == right
                newpath = svp.concatpaths([mypath, path])
                return newpath, []
            except AssertionError:
                return path, mypath


class CastOnPartSeg (CastOnPart):
    """ a caston Part whose base is 2 points"""

    def to_path(self):
        leftb = svp.Path(*[b.seg for b in self.bordersList[0]]).reversed()
        base = svp.Path(svp.Line(start=self.basePoints[0], end=self.basePoints[1]))
        rightb = svp.Path(*[b.seg for b in self.bordersList[1]])
        p = svp.concatpaths([leftb, base, rightb])
        return p


class CastOnPartPoint (CastOnPart):
    """ a caston Part whose base is 1 point"""

    def to_path(self):
        leftb = svp.Path(*[b.seg for b in self.bordersList[0]]).reversed()
        rightb = svp.Path(*[b.seg for b in self.bordersList[1]])
        p = svp.concatpaths([leftb, rightb])
        return p


class JoinPart (KPart):
    """ a part which  continues several parts"""
    pass


class SplitPart (KPart):
    """ @neighbours: base of other split part at the same y
    @type : complex or list of complex"""

    pass


class SegSplitPart (SplitPart):
    """ aSplitPart with an horizontal segment in the middle"""

    @ property
    def endPoints(self):
        return self.basePoints

    @ property
    def height(self):
        return 0

    def add_to_path(self, path):
        """ add the base to the path"""
        if isinstance(self.base, BasePoint):
            return path, []
        base = svp.Path(svp.Line(start=self.basePoints[0], end=self.basePoints[1]))
        try:
            assert base.start == path.start
            return svp.concatpaths([base.reversed(), path]), []
        except:
            try:
                assert base.end == path.start
                return svp.concatpaths([base, path]), []
            except AssertionError:
                return path, base


class KPartsIterable:
    """ iterable over the parts to knit
    @splits: there is a split in the nexts"""

    def __init__(self, kPartsList, first_index=None, priority='left', split_segment_first=True):
        if not first_index:
            self.current = kPartsList[0]
        else:
            self.current = kPartsList[first_index]
        self.stack = []
        self.done = []
        self.alls = kPartsList
        self.priority = priority
        self.split_segment_first = split_segment_first
        self.splits = []

    def part_ready(self, part):
        return not part.previous or all(p in self.done for p in part.previous)

    # def stack_next_ready(self):
        # return self.part_ready(self.stack[-1])

    def next_ready(self, part):
        """returns the part above part with highest priority or the nexts to terminate a split"""
        """"if part has only one next and this next is ready and currrent part is in the previous of this next 
        return next"""
        if (part.nexts and len(part.nexts) == 1 and
            self.part_ready(part.nexts[0]) and
            part.nexts[0].previous and
                self.current in part.nexts[0].previous) :
            return part.nexts[0]
        """if there is a SegSplitPart in nexts which is ready take it if split segment is priority"""
        if self.split_segment_first and any(isinstance(kp, SegSplitPart) for kp in part.nexts if self.part_ready(kp)):
            return next(kp for kp in part.nexts if self.part_ready(kp) and isinstance(kp, SegSplitPart))
        """if there are splits return the part whose previousnum contains the last split"""
        if self.splits:
            current_split = self.splits[-1]
            nexts_ready = list(kp for kp in part.nexts if self.part_ready(kp) and kp.previous and current_split in kp.previous)
            """if there is no splits return the leftmost part in nexts which is ready"""
        else:
            nexts_ready = list(sorted(p for p in part.nexts if self.part_ready(p)))
        if self.priority == 'left' and nexts_ready:
            return nexts_ready[0]
        elif self.priority != 'left' and nexts_ready:
            return nexts_ready[-1]
        else:
            raise StopIteration

    def gen_all_previous(self, part):
        """ generator which recursively yields all previous parts of a given part which are not already in the stack nor done"""
        for p in part.previous:
            if (not p.partNumber == self.current.partNumber and
                not p in self.stack and
                    not p in self.done) :
                yield p
            if p.previous:
                yield from self.gen_all_previous(p)

    def stack_from_nexts(self):
        """put into stack all parts next of self.current and their previous"""
        if not self.current.nexts and not self.stack:
            # normally all done but better check
            remain = [kp.partNumber for kp in self.alls if not kp in self.done]
            if remain:
                raise ValueError(f'parts {remain} were not covered')
            else:
                return StopIteration
        all_nexts = (kp for kp in self.current.nexts if (not kp.partNumber == self.current.partNumber
                                                         and not kp in self.stack
                                                         and not kp in self.done))
        for kp in all_nexts:
            self.stack.append(kp)
            if kp.previous:
                gen = self.gen_all_previous(kp)
                self.stack += [p for p in gen]
        # if split in nexts, note the end y
        if any(isinstance(kp, SplitPart) for kp in self.current.nexts):
            self.splits.append(self.current)
        return None

    def get_next_ready_in_stack(self):
        """looks into stack to get next ready
        first priority is SplitSegStart ; second priority is terminate split
        third choose the lowest caston then return the leftest or the rightest according to priorities"""
        if self.stack:
            readys = (kp for kp in self.stack if self.part_ready(kp))
            if self.split_segment_first and any((isinstance(kp, SegSplitPart) for kp in readys)):
                readys = (kp for kp in self.stack if self.part_ready(kp))
                splitseg = next(kp for kp in readys if isinstance(kp, SegSplitPart))
                return splitseg
            readys = (kp for kp in self.stack if self.part_ready(kp))
            if self.splits:
                currentsplit = self.splits[-1]
                readys = (kp for kp in readys if currentsplit in kp.previous)
            elif any(isinstance(kp, CastOnPart) for kp in readys):
                # order CastonParts by their end points so as to find the leftmost or rightmost
                kp_by_ys = sorted(((kp, kp.endPoints[0].imag) for kp in self.stack
                                   if self.part_ready(kp) and isinstance(kp, CastOnPart)),
                                  key=lambda tup: tup[1],
                                  reverse=True)
                miny = kp_by_ys[0][1]
                readys = list(tup[0] for tup in kp_by_ys if tup[1] == miny)
            else:
                readys = list(kp for kp in self.stack if self.part_ready(kp))
            try:
                stack_readys = sorted(readys)
                if self.priority == 'left' and stack_readys:
                    return stack_readys[0]
                elif self.priority != 'left' and stack_readys:
                    return stack_readys[-1]
                else:
                    raise StopIteration
            except StopIteration:
                # nothing ready in stack, fill in stack then look again
                res = self.stack_from_nexts()
            # returns None or StopIteration if done
                if not res:
                    return self.get_next_ready_in_stack()
                else:
                    return res
        else:
            # normally all done but better check
            remain = [kp.number for kp in self.alls if not kp in self.done]
            if remain:
                raise ValueError(f'parts {remain} were not covered')
            else:
                return StopIteration

    def find_next(self):
        """put in stack all nexts of current then get next_ready"""
        if self.current.nexts:
            self.stack_from_nexts()
            try:
                return self.next_ready(self.current)
            except StopIteration:
                pass
        return self.get_next_ready_in_stack()

    def __iter__(self):
        yield self.current
        while not None:
            if self.current in self.stack:
                self.stack.remove(self.current)
            self.done.append(self.current)
            # check if split is terminated
            if self.splits:
                currentsplit = self.splits[-1]
                if all(kp in self.done for kp in currentsplit.nexts):
                    self.splits.pop()
            self.current = self.find_next()
            if self.current == StopIteration:
                break
            else:
                #print(self.current.partNumber)
                yield self.current


class KpPiece:
    """ description of a piece in KParts

    @number : the index of the KParts in the piece used to number different KParts
    @type : integer

    kPartsList : the list of KParts of the piece
    @type : list

    kPartNumber : list of KParts numbers
    @type : list of integer"""

    def __init__(self, **kwargs):
        #kPartsList is not ordered
        kpl = kwargs.pop('kPartsList')
        self.kPartsList = sorted(kpl, key=lambda x: x.partNumber)
        for key, value in kwargs.items():
            setattr(self, key, value)

    def configure_parts(self):
        """ replaces previousnum and nextsnum with real parts"""
        for index, part in enumerate(self.kPartsList):
            part.configure(self.kPartsList)

    #@classmethod
    #def get_parent(cls):
        #return 'KPartModel'

    def draw(self):
        """ this method draws a piece that is returns a svg path that can be displaid"""
        iterable = KPartsIterable(self.kPartsList, priority='left', split_segment_first=False)
        currentStack = []

        def endPoints(p):
            if isinstance(p, KPart):
                return p.endPoints
            if isinstance(p, svp.Path):
                return (p.start, p.end)

        def startp(p):
            if isinstance(p, KPart):
                return p.endPoints[0]
            if isinstance(p, svp.Path):
                return p.start

        def endp(p):
            if isinstance(p, KPart):
                return p.endPoints[1]
            if isinstance(p, svp.Path):
                return p.end

        for i, part in enumerate(iterable):
            if i == 0:
                path = part.to_path()
                continue
            if isinstance(part, CastOffPartPoint):
                continue
            if part.is_contiguous(path):
                path, stack = part.add_to_path(path)
                if stack:
                    currentStack.append(stack)
                stackcopy = currentStack.copy()
                while True:
                    try:
                        to_join = next(p for p in stackcopy if startp(path) in endPoints(p) or
                                       endp(path) in endPoints(p))
                        if isinstance(to_join, KPart):
                            path, stack = to_join.add_to_path(path)
                            if stack:
                                currentStack.append(stack)
                            currentStack.remove(to_join)
                            stackcopy.remove(to_join)
                        else:
                            try:
                                path = add_paths(path, to_join)
                                currentStack.remove(to_join)
                                stackcopy.remove(to_join)
                            except ValueError:
                                stackcopy.remove(to_join)
                    except StopIteration:
                        break
            else:
                currentStack.append(part)
        for i in range(len(path._segments) - 1):
                        if path._segments[i].end != path._segments[i + 1].start:
                            try:
                                assert cmath.isclose(path._segments[i].end, path._segments[i + 1].start, abs_tol=0.000000)
                                path._segments[i + 1].start = path._segments[i].end
                            except AssertionError:
                                raise ValueError(f'cannot draw piece {self} segments are not contiguous')
        try:
            assert path.isclosed()
        except AssertionError:
            if cmath.isclose(path.start, path.end, abs_tol=0.000000):
                path.end = path.start
            else:
                raise
        return path

    def part_numbers(self):
        return [kp.number for kp in self.kPartsList]


class DKpartModelConverter (xs.DataBindingConverter):
    """DataBindingConverter for kPartModel decoding"""

    def element_decode(self,
                       data: xs.ElementData,
                       xsd_element: 'XsdElement',
                       xsd_type=None,
                       level: int = 0):
        data_element = super().element_decode(data, xsd_element, xsd_type, level)
        obj = xsd_element.binding.xml_to_python(data_element)
        data_element.obj = obj
        return data_element


class KPartModel:
    """ a knitting model described with kParts
    a KPartModel is produced either from a SVGfile through an algorithm available on Ludd21 website
    or with a garment calculator included in the predefined_model_creation package """

    def __init__(self, **kwargs):
        """this is called only from binding class while decoding
        and from predefined garment
        use decode or from_dct as the  constuctor for this class"""
        for key, value in kwargs.items():
            setattr(self, key, value)
        if isinstance(self.kpPiecesList, list):
            self.kpPiecesList = {piece.pieceName: piece for piece in self.kpPiecesList}
        for _, piece in self.kpPiecesList.items():
            piece.configure_parts()

    #@classmethod
    #def get_parent(cls):
        #return None

    #@classmethod
    #def get_mainattr(cls):
        #return 'modelName'

    @classmethod
    def decode(cls, xml):
        """the object is in res.obje ; res is the bindingclass of kpartmodel"""
        schema = helpers.create_schema(cls)
        converter = DKpartModelConverter(namespaces=[schema.default_namespace])
        try:
            res = schema.decode(xml,
                                converter=converter
                                )
            if cls.__name__ == res.obj.__class__.__name__:
                try:
                    schema.validate(xml)
                    model = res.obj
                    return model
                except xs.XMLSchemaValidationError as e:
                    raise e
        except xs.XMLSchemaValidationError as e:
            raise e


if __name__ == '__main__':
    import unittest

    #from common.tools_for_log import logconf

    #mlog = logconf('__name__', test=True)

    from pathlib import Path
    current = Path(__file__)
    tests_dir = Path(current.parent, 'tests')
    loader = unittest.TestLoader()
    tests_suite = loader.discover(tests_dir, pattern='tests_' + current.stem + '*.py')
    testRunner = unittest.runner.TextTestRunner()
    testRunner.run(tests_suite)
