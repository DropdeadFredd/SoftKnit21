# -*- coding: utf-8 -*-
""" This file is part of SoftKnit21 framework

License
=======

  - (C) Odile.Troulet-Lambert@orange.fr

This program is free software; you can redistribute it and/or modify
it under the terms of the Affero GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
Affero GNU General Public License for more details.

You should have received a copy of the Affero GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http://www.gnu.org/licenses/gpl.html}

Module purpose
==============

Grid pattern management

Implements
==========

 - B{GridPattern}

Documentation
=============

    a  Grid Pattern (O or 1) which will be knitted with a stitch technique
    contains also all binary functions which can be applied to a line of the grid pattern. Their name start with B_
    those binary functions are used in the description of the stitch technique in the database.

Usage
=====

@author: Odile Troulet-Lambert
@copyright: Odile Troulet-Lambert
@license: AGPL
"""


import re
import inspect
import logging

from gettext import gettext as _

#def get_pat_func_list():
    #"""returns a list of patterning functions of the module"""
    #return GridPattern.patterning_functions

from SoftKnit21.commons.utils import HEnum

logger = logging.getLogger(__name__)


def selFunc(method):
    """ patterning decorator"""
    method.selFunc = True
    return method


class GridPattern:
    """ a grid Pattern  which will be knitted on one of the needlebeds


    internal variable :
    _lines_count(int) : index of the current pattern line

    number(string) : number of the grid pattern in the E6000 book; grid number 0 is equivalent to no grid used
    heightrepeated (boolean) : will be repeated in height
    repeated (boolean) : will be repeated in width
    placed (boolean) : will not be repeated but placed after (before) rows and start at y stitch
    before (integer) : number of carriage passes before starting the place pattern
    even (boolean): first stitch of the pattern is placed on an even needle should not be implemented in the gridpattern
    lines(int): number of lines of the grid pattern
    columns (int) : number of columns of the grid pattern


    numpos (int) : number of positions on which the pattern is applied """

    class Rules (HEnum):
        """rules for gridpattern"""

        ZERO_GRID = ('O', _('no_grid'), '')
        GRID_MANDATORY = ('M', _('grid mandatory'), '')
        GRID_OPTIONAL = ('P', _('grid optional'), '')

    class Constraints(HEnum):
        """ the constraints to be applied to the gridpattern"""
        CHECK_LINE = ('L', _('check line'), _('No 2 adjacent black squares in a line'))
        CHECK_COLUMN = ('C', _('check column'), _('No 2 adjacent black squares in a column'))
        CHECK_BOTH = ('B', _('check both'), _('No 2 adjacent black squares in line and column'))
        NO_CHECK = ('K', _('no constraints'), _('No constraints on the grid pattern'))
        ONE_LINE = ('1', _('single line'), _('One Line only'))

    class Groups (HEnum):
        """groups of GridPatterns mainly referring to where they are stored"""
        E6000 = ('E6000_GridPatterns', _('GridPatterns of the E6000Book'), '')

    @classmethod
    def create_fake(cls, lines=0, columns=1, **kwargs):
        """make a fake gridpattern"""
        kwargs.update({'number': 0, 'height': lines, 'width': columns, 'lines_list': ['1'] * columns})
        return GridPattern(**kwargs)

    def __init__(self,
                 height_repeat=True,
                 repeated=True,
                 placed=False,
                 before=0,
                 even=None,
                 **kwargs
                 ):
        self.heightrepeated = height_repeat
        self.repeated = repeated
        self.placed = placed
        self.before = before
        self.even = even
        if self.placed:
            self.repeated = False
            self.heightrepeated = False
        self._lines_count = 0
        self.number = kwargs.get('number', 0)
        self.width = kwargs.get('width', None)
        self.height = kwargs.get('height', None)
        self.lines_list = kwargs.get('lines_list', None)
        if self.width is None or self.height is None or self.lines_list is None:
            raise ValueError(f'cannot create GridPattern {self.number} with height :{self.height} width: {self.width} lines_list :{self.lines_list}')

        self.lines = len(self.lines_list)
        self.columns = len(self.lines_list[0])

        logger.debug(f'grid_pattern {self.number} created')

        return

    @ property
    def lines_count(self):
        """getter for __line_count"""
        return self._lines_count

    def check_equal_lines(self):
        """ checks that all lines have the same length """

        return all(len(line) == self.columns for line in self.lines_list)

    def check_line(self):
        """ no 2 consecutives 1 in one line """
        if self.repeated:
            # no 1 in extremities
            for index, line in enumerate(self.lines_list):
                if line[0] == '1' and line[0] == line[-1]:
                    return index
        for index, line in enumerate(self.lines_list):
            if re.search('1{2,}', line) != None:
                return index
        return True

    def check_column(self):
        """ no 2 consecutives black squares in one column """
        tr_zip = (zip(*self.lines_list))
        trans = (''.join(element) for element in tr_zip)
        if self.heightrepeated:
            # no 1 in same x
            for index, line in enumerate(trans):
                if line[0] == '1' and line[-1] == '1':
                    return index
        tr_zip = (zip(*self.lines_list))
        trans = (''.join(element) for element in tr_zip)
        for index, column in enumerate(trans):
            if re.search('1{2,}', column) != None:
                return index
        return True

    def check_constraints(self, grid_rule, grid_constraint):
        """ returns True if  grid_pattern constraint and grid_rule are satisfied
            see separate document for coherence of parameters"""
        #_grid_rule = [i[0] for i in GP.CHOICES]
        #_grid_rule_long = [i[1] for i in GP.CHOICES]
        #_grid_constraint = [i[0] for i in GP.CONSTRAINTS]
        #_grid_constraint_long = [i[1] for i in GP.CONSTRAINTS]

        #if not isinstance(self, GridPattern):
            #raise ValueError(
                #(f'in {inspect.getmodulename(__file__)} gridPattern {self} is not an instance of grid_pattern')
            #)

        if not self.check_equal_lines():
            raise ValueError(
                (f' lines of the grid_pattern {self} must be of equal length : ')
            )

        if grid_rule not in list(GridPattern.Rules):
            raise ValueError(
                (f'in gridpattern {self} grid_pattern rule {grid_rule} must be in {list(GridPattern.Rules)}')
            )

        if grid_constraint not in list(GridPattern.Constraints):
            raise ValueError(
                (f'in gridpattern {self} grid_pattern constraint {grid_constraint} must be in {list(GridPattern.Constraints)}')
            )

        if self.repeated not in (True, False):
            raise ValueError(
                (f'in gridpattern {self} repetition of grid_pattern {self.repeated} must be either True or False'))

        if grid_rule == GridPattern.Rules.GRID_MANDATORY and (grid_constraint == GridPattern.Constraints.NO_CHECK
                                                              and self.number != 0):
            return True

        if (grid_rule == GridPattern.Rules.ZERO_GRID
                or grid_rule == GridPattern.Rules.GRID_OPTIONAL) and self.number == 0:
            return True

        if self.number != 0 and grid_rule == GridPattern.Rules.ZERO_GRID:
            raise ValueError(
                ('No grid pattern allowed with this technique')
            )

        if grid_rule == GridPattern.Rules.GRID_MANDATORY and self.number == 0:
            raise ValueError(
                ('grid pattern is mandatory with this technique')
            )

        if ((grid_rule == GridPattern.Rules.GRID_MANDATORY or grid_rule == GridPattern.Rules.GRID_OPTIONAL) and
                grid_constraint == GridPattern.Constraints.NO_CHECK):
            return True
        if self.number != 0:
            if grid_constraint == GridPattern.Constraints.NO_CHECK:
                return True
            if grid_constraint == GridPattern.Constraints.CHECK_LINE or grid_constraint == GridPattern.Constraints.CHECK_BOTH:
                value = self.check_line()
                if value != True:
                    raise ValueError(
                        (f'in gridpattern {self} error in line {value} : grid pattern must not have 2 consecutives 1 in a line')
                    )
            if grid_constraint == GridPattern.Constraints.CHECK_COLUMN or grid_constraint == GridPattern.Constraints.CHECK_BOTH:
                value = self.check_column()
                if value != True:
                    raise ValueError(
                        (f'in gridpattern {self} error in column {value} grid pattern must not have 2 consecutives 1 in a column')
                    )
            return True

    def back_one_line(self):
        """ decrease by one the index of current line
        returns the corresponding pattern line"""
        if self._lines_count == 0:
            raise ValueError('no Line of grid pattern has been processed yet')
        self._lines_count -= 1
        if (self.placed and (self._lines_count <= self.before or
                             self._lines_count >= self.before + self.lines)) or (
            not self.heightrepeated and self._lines_count > self.lines
        ):
            c_pass = '1' * self.columns
            return c_pass
        c_pass = self.lines_list[(self._lines_count - self.before) % self.lines]
        return c_pass

    def get_relative(self, num_lines):
        """ gets the lines for previous or next passes
        but does not change current line

        @param num_lines number of line backward if <0 or forward if >0
        @type integer"""
        if self._lines_count + num_lines < 0:
            raise ValueError(f'number of lines used is lower than {num_lines}')
        value = self._lines_count + num_lines
        if (self.placed and (value < self.before or value > self.lines + self.before)) or (
                not self.heightrepeated and value > self.lines):
            res = '1' * self.columns
            return res
        res = self.lines_list[(value - self.before) % self.lines]
        return res

    def __str__(self):
        """ renders a grid_pattern to a string"""
        res1 = f"number : {self.number} , rows : {self.lines } , columns : {self.columns}"
        res2 = [f" row {i:<3} : {self.lines_list[i]:<5} \n" for i in range(0, self.columns)]
        return res1 + '\n' + ''.join(res2)

    @selFunc
    def nextSelect(self):
        """next line in the grid pattern"""
        if (self.placed and self._lines_count <= self.before) or (
                not self.heightrepeated and self._lines_count > self.lines):
            c_pass = '1' * self.columns
            self._lines_count += 1
            return c_pass
        c_pass = self.lines_list[(self._lines_count - self.before) % self.lines]
        self._lines_count += 1
        return c_pass

    @selFunc
    def idem(self):
        """ identical to the current selection"""
        if self._lines_count == 0:
            raise ValueError(
                (' B_idem cannot be used as first binary function')
            )
        else:
            return self.lines_list[self._lines_count - 1]

    @selFunc
    def inverse(self):
        """ inverse of current selection"""
        def g_inverse(char):
            if char == '0':
                return '1'
            if char == '1':
                return '0'
        if self._lines_count == 0:
            raise ValueError(
                (' B_inverse_select  cannot be used as first binary function')
            )
        return ''.join([g_inverse(i) for i in self.lines_list[self._lines_count - 1]])

    @selFunc
    def all0(self):
        """ all zeros"""
        return '0' * self.columns

    @selFunc
    def all1(self):
        """ all ones"""
        return '1' * self.columns

    @selFunc
    def oneEvo(self):
        """1 at first position (position  of the grid), grid pattern at the next positions"""
        if self._lines_count == 0:
            raise ValueError(
                (' B_1_evo  cannot be used as first binary function')
            )
        value = self.lines_list[self._lines_count - 1]
        return '1' + value[1:self.columns]

    @selFunc
    def zeroEvo(self):
        """ 0 at first position, grid_pattern at the next positions"""
        if self._lines_count == 0:
            raise ValueError(
                (' B_0_evo  cannot be used as first binary function')
            )
        value = self.lines_list[self._lines_count - 1]
        return '0' + value[1:self.columns]

    @selFunc
    def odd0(self):
        """0 at each odd position grid pattern at other positions"""
        if self._lines_count == 0:
            raise ValueError(
                (' B_odd_0 cannot be used as first binary function')
            )
        if self.even == None:
            raise ValueError(
                _(' no even and odd position are given for this grid_pattern')
            )
        if self.even not in [True, False]:
            raise ValueError(
                (' even position for the grid pattern must be either True or False')
            )
        if self.even == True:
            # the first position of the grid pattern in on even needle
            cur = self.lines_list[self._lines_count - 1]
            return ''.join([cur[i] if i % 2 == 0 else '0' for i in range(self.columns)])
        if self.even == False:
            # the first position of the grid pattern in on odd needle
            cur = self.lines_list[self._lines_count - 1]
            return ''.join([cur[i] if i % 2 != 0 else '0' for i in range(self.columns)])

    @selFunc
    def odd1(self):
        """1 at each odd position grid pattern  at other positions"""
        if self._lines_count == 0:
            raise ValueError(
                (' B_odd_1 cannot be used as first binary function')
            )
        if self.even == None:
            raise ValueError(
                (' no even and odd position are given for this grid_pattern')
            )
        if self.even not in [True, False]:
            raise ValueError(
                (' even position for the grid pattern must be either True or False')
            )
        if self.even == True:
            # the first position of the grid pattern in on even needle
            cur = self.lines_list[self._lines_count - 1]
            return ''.join([cur[i] if i % 2 == 0 else '1' for i in range(self.columns)])
        if self.even == False:
            # the first position of the grid pattern in on odd needle
            cur = self.lines_list[self._lines_count - 1]
            return ''.join([cur[i] if i % 2 != 0 else '1' for i in range(self.columns)])

    @selFunc
    def shiftLeft1(self):
        """grid pattern line shifted 1 bit towards the left complemented to 1 on right"""
        if self._lines_count == 0:
            raise ValueError(
                (' B_shift_left_1 cannot be used as first binary function')
            )
        cur = self.lines_list[self._lines_count - 1]
        return ''.join([char for char in cur[1::]]) + '1'

    @selFunc
    def shiftLeft0(self):
        """grid pattern line  shifted 1 bit towards the left complemented to 0 on right"""
        if self._lines_count == 0:
            raise ValueError(
                (' B_shift_left_0 cannot be used as first binary function')
            )
        cur = self.lines_list[self._lines_count - 1]
        return ''.join([char for char in cur[1::]]) + '0'

    @selFunc
    def shiftRight0(self):
        """row shifted 1 bit towards the right  complemented to 0 on left"""
        if self._lines_count == 0:
            raise ValueError(
                (' B_shift_right 0 cannot be used as first binary function')
            )
        cur = self.lines_list[self._lines_count - 1]
        return '0' + ''.join([char for char in cur[0:self.columns - 1]])

    @selFunc
    def shiftRight1(self):
        """row shifted 1 bit towards the right  complemented to 1 on left"""
        if self._lines_count == 0:
            raise ValueError(
                (' B_shift_right_1 cannot be used as first binary function')
            )
        cur = self.lines_list[self._lines_count - 1]
        return '1' + ''.join([char for char in cur[0:self.columns - 1]])

    def set_numpos(self, numpos, fbin=None, start=0):
        """ if pattern is repeated :extends the pattern to the number of positions numpos
        column 0 of the grid is on position 0
        if pattern is placed, returns only the number of positions of the pattern

        numpos(int) : number of knitting positions to which pattern is to be applied
        fbin(function): function to apply to the current line of the pattern before applying it
        if fbin is None then current line of the pattern is used
        start(int): gives the starting position for a place pattern
        """
        self.numpos = numpos
        self.column0 = start
        if fbin != None:
            fbin_to_call = getattr(self, 'B_' + fbin)
            line = fbin_to_call()
        else:
            line = self.B_next_select()
        if self.repeated:
            modulo = self.numpos % self.columns
            times = int(self.numpos / self.columns)
            self.rightSlice = slice(0, modulo)
            self.leftSlice = slice(0, 0)
            return line[self.leftSlice] + line * times + line[self.rightSlice]
        else:
            self.left = self.column0
            self.right = self.numpos - self.column0 - self.columns
            start = 0
            end = min(self.numpos - self.column0, self.columns)
            return 1 * self.left + line[slice(end, start)] + 1 * self.right

    def next_patternline(self, leftPlus=0, rightPlus=0, fbin=None):
        """ apply the line pattern after extending the number of positions of num to the left
        leftPlus(int) : number of positions to extend or to reduce on the left
        rightPlus(int) : number of positions to extend or to reduce on the right"""
        self.numpos = self.numpos + rightPlus + leftPlus
        if fbin != None:
            fbin_to_call = getattr(self, 'B_' + fbin)
            line = fbin_to_call()
        else:
            line = self.B_next_select()
        if self.repeated:
            if leftPlus != 0:
                self.leftSlice.start = self.columns - (self.leftSlice.end - self.leftSLice.start + leftPlus) % self.columns
                self.leftSlice.end = self.columns
            modulo = self.numpos % self.columns
            times = int(self.numpos / self.columns)
            self.rightSlice = slice(0, modulo)
            return line[self.leftSlice] + line * times + line[self.rightSlice]
        elif leftPlus != 0:
            self.left = self.left + leftPlus
            self.right = self.right + rightPlus
            start = - self.left if self.left < 0 else 0
            end = (self.right + self.columns) % self.columns if self.right < 0 else self.columns
            return 1 * self.left + line[slice(start, end)] + 1 * self.right

    #@classmethod
    #def patterning_functions_tuples(cls):
        #""" return (key , (name, _(name), help_text)) for all patterning functions to create an enum"""
        #return list((name.upper(), (name, _(name), func.__doc__)) for (name, func) in inspect.getmembers(cls) if hasattr(func, 'patterning'))

    @classmethod
    def get_SelFuncHEnum(cls):
        import enum
        dic = {name.upper(): (name, _(name), func.__doc__) for (name, func) in inspect.getmembers(cls) if hasattr(func, 'selFunc')}
        enumcls = enum.EnumMeta.__call__(HEnum, 'SelFuncEnum', names=dic, module=__name__)
        return enumcls


if __name__ == '__main__':
    import unittest

    from pathlib import Path
    current = Path(__file__)
    tests_dir = Path(current.parent, 'tests')
    loader = unittest.TestLoader()
    tests_suite = loader.discover(tests_dir, pattern='tests_' + current.stem + '*.py')
    testRunner = unittest.runner.TextTestRunner()
    testRunner.run(tests_suite)







