# -*- coding: utf-8 -*-
""" This file is part of SoftKnit21 framework

License
=======

 - B{SofKnit21} (U{http://www.softkni21.org}) is Copyright:
  - (C) Odile.Troulet-Lambert@orange.fr

This program is free software; you can redistribute it and/or modify
it under the terms of the affero GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
Affero GNU General Public License for more details.

You should have received a copy of the Affero GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http://www.gnu.org/licenses/gpl.html}

Module purpose
==============

gridPattern Tests

Implements
==========

 - B{test_grid_pattern}:


Documentation
=============



Usage
=====

@author: Odile Troulet-Lambert
@copyright: Odile Troulet-Lambert
@license: AGPL
"""

import unittest
from pathlib import Path
import copy

from generic_knitting_model.grid_pattern import GridPattern
from local_storage.grid_pattern_storage import decodeGridPatternCut
#from common.constant_classes import GridPatternPossibilities as _G
from generic_knitting_model.tests.tests_super_class import Test_super_class

MAND = GridPattern.Rules.GRID_MANDATORY
ZERO = GridPattern.Rules.ZERO_GRID
OPT = GridPattern.Rules.GRID_OPTIONAL
C_LINE = GridPattern.Constraints.CHECK_LINE
C_COL = GridPattern.Constraints.CHECK_COLUMN
NO_C = GridPattern.Constraints.NO_CHECK
ONE_LINE = GridPattern.Constraints.ONE_LINE
BOTH = GridPattern.Constraints.CHECK_BOTH


class Grid_Pattern_Constraints_Test(Test_super_class, unittest.TestCase):
    """ tests for the check_constraints method"""

    @classmethod
    def setUpClass(cls):
        datadir = super().get_tests_datadir(cls, __file__)
        file1008 = Path(datadir, 'data_1008.CUT')

        width, height, lines_list = decodeGridPatternCut(file1008)
        dic1008 = {'number': '1008', 'height': height, 'width': width, 'lines_list': lines_list}
        cls.gp_fake = GridPattern.create_fake()
        cls.gp_fake_notrepeated = GridPattern.create_fake(repeated=False)
        cls.gp_fake_notheightrepeated = GridPattern.create_fake(height_repeated=False)

        cls.gp = GridPattern(**dic1008)
        cls.gp_notrepeated = GridPattern(repeated=False, **dic1008)
        cls.gp_notheightrepeated = GridPattern(height_repeated=False, **dic1008)

        mdic1008 = copy.deepcopy(dic1008)
        cls.gp_length_error = GridPattern(**mdic1008)
        cls.gp_length_error.lines_list[3] = '110'

        ndic1008 = copy.deepcopy(dic1008)
        cls.gp_lineKO = GridPattern(**ndic1008)
        cls.gp_lineKO.lines_list[4] = '00011100'

        odic1008 = copy.deepcopy(dic1008)
        cls.gp_columnKO = GridPattern(**odic1008)
        cls.gp_columnKO.lines_list[6] = '00100000'

        pdic1008 = copy.deepcopy(dic1008)
        cls.gp_line_borderKO = GridPattern(**pdic1008)
        cls.gp_line_borderKO.lines_list[0] = '10000001'

        qdic1008 = copy.deepcopy(dic1008)
        cls.gp_column_borderKO = GridPattern(**qdic1008)
        cls.gp_column_borderKO.lines_list[0] = '00001000'

    def test_error_in_enum(self):
        self.assertRaisesRegex(ValueError, 'rule toto must be in', self.gp.check_constraints, 'toto', C_LINE)
        self.assertRaisesRegex(ValueError, 'constraint toto must be in', self.gp.check_constraints, ZERO, 'toto')

    def test_check_constraints_ZERO_GRID(self):
        self.assertTrue(self.gp_fake.check_constraints(ZERO, C_LINE))
        self.assertTrue(self.gp_fake.check_constraints(ZERO, BOTH))
        self.assertTrue(self.gp_fake.check_constraints(ZERO, C_COL))
        self.assertTrue(self.gp_fake.check_constraints(ZERO, NO_C))
        self.assertTrue(self.gp_fake_notrepeated.check_constraints(ZERO, C_LINE))
        self.assertTrue(self.gp_fake_notrepeated.check_constraints(ZERO, BOTH))
        self.assertTrue(self.gp_fake_notrepeated.check_constraints(ZERO, C_COL))
        self.assertTrue(self.gp_fake_notrepeated.check_constraints(ZERO, NO_C))
        self.assertTrue(self.gp_fake_notheightrepeated.check_constraints(ZERO, C_LINE))
        self.assertTrue(self.gp_fake_notheightrepeated.check_constraints(ZERO, BOTH))
        self.assertTrue(self.gp_fake_notheightrepeated.check_constraints(ZERO, C_COL))
        self.assertTrue(self.gp_fake_notheightrepeated.check_constraints(ZERO, NO_C))
        self.assertRaisesRegex(ValueError, 'grid pattern allowed', self.gp.check_constraints, ZERO, C_LINE)
        self.assertRaisesRegex(ValueError, 'grid pattern allowed', self.gp.check_constraints, ZERO, C_COL)
        self.assertRaisesRegex(ValueError, 'grid pattern allowed', self.gp.check_constraints, ZERO, BOTH)
        self.assertRaisesRegex(ValueError, 'grid pattern allowed', self.gp.check_constraints, ZERO, NO_C)
        self.assertRaisesRegex(ValueError, 'grid pattern allowed', self.gp_notrepeated.check_constraints, ZERO, C_LINE)
        self.assertRaisesRegex(ValueError, 'grid pattern allowed', self.gp_notrepeated.check_constraints, ZERO, BOTH)
        self.assertRaisesRegex(ValueError, 'grid pattern allowed', self.gp_notrepeated.check_constraints, ZERO, C_COL)
        self.assertRaisesRegex(ValueError, 'grid pattern allowed', self.gp_notrepeated.check_constraints, ZERO, NO_C)
        self.assertRaisesRegex(ValueError, 'grid pattern allowed', self.gp_notheightrepeated.check_constraints, ZERO, C_LINE)
        self.assertRaisesRegex(ValueError, 'grid pattern allowed', self.gp_notheightrepeated.check_constraints, ZERO, BOTH)
        self.assertRaisesRegex(ValueError, 'grid pattern allowed', self.gp_notheightrepeated.check_constraints, ZERO, C_COL)
        self.assertRaisesRegex(ValueError, 'grid pattern allowed', self.gp_notheightrepeated.check_constraints, ZERO, NO_C)

    def test_check_constraints_GRID_OPTIONAL(self):
        self.assertTrue(self.gp_fake.check_constraints(OPT, C_LINE))
        self.assertTrue(self.gp_fake.check_constraints(OPT, BOTH))
        self.assertTrue(self.gp_fake.check_constraints(OPT, C_COL))
        self.assertTrue(self.gp_fake.check_constraints(OPT, NO_C))
        self.assertTrue(self.gp_fake_notrepeated.check_constraints(OPT, C_LINE))
        self.assertTrue(self.gp_fake_notrepeated.check_constraints(OPT, BOTH))
        self.assertTrue(self.gp_fake_notrepeated.check_constraints(OPT, C_COL))
        self.assertTrue(self.gp_fake_notrepeated.check_constraints(OPT, NO_C))
        self.assertTrue(self.gp_fake_notheightrepeated.check_constraints(OPT, C_LINE))
        self.assertTrue(self.gp_fake_notheightrepeated.check_constraints(OPT, BOTH))
        self.assertTrue(self.gp_fake_notheightrepeated.check_constraints(OPT, C_COL))
        self.assertTrue(self.gp_fake_notheightrepeated.check_constraints(OPT, NO_C))

        self.assertRaisesRegex(ValueError, 'equal length', self.gp_length_error.check_constraints, OPT, C_COL)
        self.assertRaisesRegex(ValueError, 'equal length', self.gp_length_error.check_constraints, OPT, BOTH)
        self.assertRaisesRegex(ValueError, 'equal length', self.gp_length_error.check_constraints, OPT, C_LINE)
        self.assertRaisesRegex(ValueError, 'equal length', self.gp_length_error.check_constraints, OPT, NO_C)

        self.assertTrue(self.gp_fake.check_constraints(OPT, NO_C))
        self.assertTrue(self.gp_fake_notrepeated.check_constraints(OPT, NO_C))
        self.assertTrue(self.gp_fake_notheightrepeated.check_constraints(OPT, NO_C))

        self.assertTrue(self.gp.check_constraints(OPT, NO_C))
        self.assertTrue(self.gp_notrepeated.check_constraints(OPT, NO_C))
        self.assertTrue(self.gp_notheightrepeated.check_constraints(OPT, NO_C))

        self.assertRaisesRegex(ValueError, 'error in line 4', self.gp_lineKO.check_constraints, OPT, BOTH)
        self.assertRaisesRegex(ValueError, 'error in line 4', self.gp_lineKO.check_constraints, OPT, C_LINE)
        self.assertTrue(self.gp_lineKO.check_constraints(OPT, C_COL))
        self.assertTrue(self.gp_lineKO.check_constraints(OPT, NO_C))

        self.assertTrue(self.gp_columnKO.check_constraints(OPT, C_LINE))
        self.assertTrue(self.gp_columnKO.check_constraints(OPT, NO_C))
        self.assertRaisesRegex(ValueError, 'error in column 2', self.gp_columnKO.check_constraints, OPT, BOTH)
        self.assertRaisesRegex(ValueError, 'error in column 2', self.gp_columnKO.check_constraints, OPT, C_COL)

        self.assertTrue(self.gp_notrepeated.check_constraints(OPT, C_LINE))
        self.assertTrue(self.gp_notrepeated.check_constraints(OPT, C_COL))
        self.assertTrue(self.gp_notrepeated.check_constraints(OPT, BOTH))

        self.assertTrue(self.gp_line_borderKO.check_constraints(OPT, C_COL))
        self.assertTrue(self.gp_line_borderKO.check_constraints(OPT, NO_C))
        self.assertRaisesRegex(ValueError, 'error in line 0', self.gp_line_borderKO.check_constraints, OPT, BOTH)
        self.assertRaisesRegex(ValueError, 'error in line 0', self.gp_line_borderKO.check_constraints, OPT, C_LINE)

        self.assertTrue(self.gp_column_borderKO.check_constraints(OPT, C_LINE))
        self.assertTrue(self.gp_column_borderKO.check_constraints(OPT, NO_C))
        self.assertRaisesRegex(ValueError, 'error in column 4', self.gp_column_borderKO.check_constraints, OPT, BOTH)
        self.assertRaisesRegex(ValueError, 'error in column 4', self.gp_column_borderKO.check_constraints, OPT, C_COL)

    def test_check_constraints_GRID_MANDATORY(self):

        self.assertRaisesRegex(ValueError, 'mandatory with this technique', self.gp_fake.check_constraints, MAND, NO_C)
        self.assertRaisesRegex(ValueError, 'mandatory with this technique', self.gp_fake_notrepeated.check_constraints, MAND, NO_C)
        self.assertRaisesRegex(ValueError, 'mandatory with this technique', self.gp_fake_notheightrepeated.check_constraints, MAND, NO_C)

        self.assertRaisesRegex(ValueError, 'equal length', self.gp_length_error.check_constraints, MAND, C_COL)
        self.assertRaisesRegex(ValueError, 'equal length', self.gp_length_error.check_constraints, MAND, BOTH)
        self.assertRaisesRegex(ValueError, 'equal length', self.gp_length_error.check_constraints, MAND, C_LINE)
        self.assertRaisesRegex(ValueError, 'equal length', self.gp_length_error.check_constraints, MAND, NO_C)

        self.assertTrue(self.gp.check_constraints(MAND, NO_C))
        self.assertTrue(self.gp_notrepeated.check_constraints(MAND, NO_C))
        self.assertTrue(self.gp_notheightrepeated.check_constraints(MAND, NO_C))

        self.assertRaisesRegex(ValueError, 'error in line 4', self.gp_lineKO.check_constraints, MAND, BOTH)
        self.assertRaisesRegex(ValueError, 'error in line 4', self.gp_lineKO.check_constraints, MAND, C_LINE)
        self.assertTrue(self.gp_lineKO.check_constraints(MAND, C_COL))
        self.assertTrue(self.gp_lineKO.check_constraints(MAND, NO_C))

        self.assertTrue(self.gp_columnKO.check_constraints(MAND, C_LINE))
        self.assertTrue(self.gp_columnKO.check_constraints(MAND, NO_C))
        self.assertRaisesRegex(ValueError, 'error in column 2', self.gp_columnKO.check_constraints, MAND, BOTH)
        self.assertRaisesRegex(ValueError, 'error in column 2', self.gp_columnKO.check_constraints, MAND, C_COL)

        self.assertTrue(self.gp_notrepeated.check_constraints(MAND, C_LINE))
        self.assertTrue(self.gp_notrepeated.check_constraints(MAND, C_COL))
        self.assertTrue(self.gp_notrepeated.check_constraints(MAND, BOTH))

        self.assertTrue(self.gp_line_borderKO.check_constraints(MAND, C_COL))
        self.assertTrue(self.gp_line_borderKO.check_constraints(MAND, NO_C))
        self.assertRaisesRegex(ValueError, 'error in line 0', self.gp_line_borderKO.check_constraints, MAND, BOTH)
        self.assertRaisesRegex(ValueError, 'error in line 0', self.gp_line_borderKO.check_constraints, MAND, C_LINE)

        self.assertTrue(self.gp_column_borderKO.check_constraints(MAND, C_LINE))
        self.assertTrue(self.gp_column_borderKO.check_constraints(MAND, NO_C))
        self.assertRaisesRegex(ValueError, 'error in column 4', self.gp_column_borderKO.check_constraints, MAND, BOTH)
        self.assertRaisesRegex(ValueError, 'error in column 4', self.gp_column_borderKO.check_constraints, MAND, C_COL)

#@unittest.skip


class Grid_Pattern_methods_Test (Test_super_class, unittest.TestCase):
    """ tests for class methods other than check constraints"""

    @classmethod
    def setUpClass(cls):
        datadir = super().get_tests_datadir(cls, __file__)
        file1008 = Path(datadir, 'data_1008.CUT')

        width, height, lines_list = decodeGridPatternCut(file1008)
        cls.dic1008 = {'number': '1008', 'height': height, 'width': width, 'lines_list': lines_list}

    def setUp(self):
        self.gp = GridPattern(**self.dic1008)
        pass

    def test_back_one_line(self):
        # first line
        with self.assertRaises(ValueError):
            self.gp.back_one_line()

        # middle line
        self.gp._lines_count = 7
        res = self.gp.back_one_line()
        self.assertEqual(res, '00010100')
        self.assertEqual(self.gp.lines_count, 6)

        # last line
        self.gp._lines_count = 8
        res = self.gp.back_one_line()
        self.assertEqual(res, '00001000')
        self.assertEqual(self.gp.lines_count, 7)

        # after last line height repeat
        self.gp._lines_count = 9
        res = self.gp.back_one_line()
        self.assertEqual(res, '00010100')
        self.assertEqual(self.gp.lines_count, 8)

        # after last line not height repeat
        self.gp = GridPattern(height_repeat=False, **self.dic1008)
        self.gp._lines_count = 10
        res = self.gp.back_one_line()
        self.assertEqual(res, '1' * self.gp.columns)
        self.assertEqual(self.gp.lines_count, 9)

        # placed with before
        # before before
        self.gp = GridPattern(placed=True, before=5, **self.dic1008)
        self.gp._lines_count = 6
        res = self.gp.back_one_line()
        self.assertEqual(res, '1' * self.gp.columns)
        self.assertEqual(self.gp.lines_count, 5)
        # placed after before
        self.gp._lines_count = 8
        res = self.gp.back_one_line()
        self.assertEqual(res, '01000001')
        self.assertEqual(self.gp.lines_count, 7)

        # placed after the end
        self.gp._lines_count = 15
        res = self.gp.back_one_line()
        self.assertEqual(res, '1' * self.gp.columns)
        self.assertEqual(self.gp.lines_count, 14)

    def test_get_relative(self):

        # first line
        self.gp = GridPattern(**self.dic1008)
        with self.assertRaises(ValueError):
            self.gp.get_relative(-2)

        # middle line back to 0
        self.gp = GridPattern(**self.dic1008)
        self.gp._lines_count = 2
        res = self.gp.get_relative(-2)
        self.assertEqual(res, '00010100')
        self.assertEqual(self.gp.lines_count, 2)

        # middle line back too big
        self.gp = GridPattern(**self.dic1008)
        self.gp._lines_count = 2
        with self.assertRaises(ValueError):
            self.gp.get_relative(-3)

        # last line
        self.gp = GridPattern(**self.dic1008)
        self.gp._lines_count = 8
        res = self.gp.get_relative(-1)
        self.assertEqual(res, '00001000')
        self.assertEqual(self.gp.lines_count, 8)

        # after last line height repeat
        self.gp = GridPattern(**self.dic1008)
        self.gp._lines_count = 9
        res = self.gp.get_relative(-1)
        self.assertEqual(res, '00010100')
        self.assertEqual(self.gp.lines_count, 9)

        # after last line not height repeat
        self.gp = GridPattern(height_repeat=False, **self.dic1008)
        self.gp._lines_count = 10
        res = self.gp.get_relative(-1)
        self.assertEqual(res, '1' * self.gp.columns)
        self.assertEqual(self.gp.lines_count, 10)

        # placed with before
        # before before
        self.gp = GridPattern(placed=True, before=5, **self.dic1008)
        self.gp._lines_count = 6
        res = self.gp.get_relative(-2)
        self.assertEqual(res, '1' * self.gp.columns)
        self.assertEqual(self.gp.lines_count, 6)
        # placed after before
        self.gp._lines_count = 8
        res = self.gp.get_relative(-2)
        self.assertEqual(res, '00100010')
        self.assertEqual(self.gp.lines_count, 8)

        # placed after the end
        self.gp._lines_count = 15
        res = self.gp.get_relative(-2)
        self.assertEqual(res, '1' * self.gp.columns)
        self.assertEqual(self.gp.lines_count, 15)


#@unittest.skip
class Grid_Pattern_BinaryFunctions_Test(Test_super_class, unittest.TestCase):
    """ tests for binary functions of gridPatterns objects"""

    @classmethod
    def setUpClass(cls):
        datadir = super().get_tests_datadir(cls, __file__)
        file1008 = Path(datadir, 'data_1008.CUT')

        width, height, lines_list = decodeGridPatternCut(file1008)
        cls.dic1008 = {'number': '1008', 'height': height, 'width': width, 'lines_list': lines_list}

    def test_nextSelect(self):
        # first line
        self.gp = GridPattern(**self.dic1008)
        res = self.gp.nextSelect()
        self.assertEqual(res, '00010100')
        self.assertEqual(self.gp.lines_count, 1)

        # middle line
        self.gp = GridPattern(**self.dic1008)
        self.gp._lines_count = 1
        res = self.gp.nextSelect()
        self.assertEqual(res, '00100010')
        self.assertEqual(self.gp.lines_count, 2)

        # last line
        self.gp = GridPattern(**self.dic1008)
        self.gp._lines_count = 7
        res = self.gp.nextSelect()
        self.assertEqual(res, '00001000')
        self.assertEqual(self.gp.lines_count, 8)

        # after last line height repeat
        self.gp = GridPattern(**self.dic1008)
        self.gp._lines_count = 9
        res = self.gp.nextSelect()
        self.assertEqual(res, '00100010')
        self.assertEqual(self.gp.lines_count, 10)

        # after last line not height repeat
        self.gp = GridPattern(height_repeat=False, **self.dic1008)
        self.gp._lines_count = 9
        res = self.gp.nextSelect()
        self.assertEqual(res, '1' * self.gp.columns)
        self.assertEqual(self.gp.lines_count, 10)

        # placed with before
        # before before
        self.gp = GridPattern(placed=True, before=5, **self.dic1008)
        self.gp._lines_count = 4
        res = self.gp.nextSelect()
        self.assertEqual(res, '1' * self.gp.columns)
        self.assertEqual(self.gp.lines_count, 5)
        # placed after before
        self.gp._lines_count = 7
        res = self.gp.nextSelect()
        self.assertEqual(res, '01000001')
        self.assertEqual(self.gp.lines_count, 8)

        # placed after the end
        self.gp._lines_count = 15
        res = self.gp.nextSelect()
        self.assertEqual(res, '1' * self.gp.columns)
        self.assertEqual(self.gp.lines_count, 16)

    # unittest.skip
    def test_idem(self):
        # first line
        self.gp = GridPattern(**self.dic1008)
        with self.assertRaises(ValueError):
            self.gp.idem()

        # middle line
        self.gp = GridPattern(**self.dic1008)
        self.gp._lines_count = 7
        res = self.gp.idem()
        self.assertEqual(res, '00010100')
        self.assertEqual(self.gp.lines_count, 7)

    # unittest.skip
    def test_inverse(self):

        # first line
        self.gp = GridPattern(**self.dic1008)
        with self.assertRaises(ValueError):
            self.gp.inverse()

        # middle line
        self.gp = GridPattern(**self.dic1008)
        self.gp._lines_count = 7
        res = self.gp.inverse()
        self.assertEqual(res, '11101011')
        self.assertEqual(self.gp.lines_count, 7)

    # unittest.skip
    def test_all0(self):
        # middle line
        self.gp = GridPattern(**self.dic1008)
        self.gp._lines_count = 2
        res = self.gp.all0()
        self.assertEqual(res, '00000000')
        self.assertEqual(self.gp.lines_count, 2)

    # unittest.skip
    def test_all1(self):
        # middle line
        self.gp = GridPattern(**self.dic1008)
        self.gp._lines_count = 2
        res = self.gp.all1()
        self.assertEqual(res, '11111111')
        self.assertEqual(self.gp.lines_count, 2)

    # unittest.skip
    def test_oneEvo(self):
        # first line
        self.gp = GridPattern(**self.dic1008)
        with self.assertRaises(ValueError):
            self.gp.oneEvo()
        # middle line
        self.gp = GridPattern(**self.dic1008)
        # this is to force access to the "private attribute"
        self.gp._lines_count = 7
        res = self.gp.oneEvo()
        self.assertEqual(res, '10010100')
        self.assertEqual(self.gp.lines_count, 7)

    # unittest.skip
    def test_zeroEvo(self):
        # first line
        self.gp = GridPattern(**self.dic1008)
        with self.assertRaises(ValueError):
            self.gp.zeroEvo()
        # middle line
        self.gp = GridPattern(**self.dic1008)
        self.gp._lines_count = 4
        res = self.gp.zeroEvo()
        self.assertEqual(res, '00000000')
        self.assertEqual(self.gp.lines_count, 4)

    # unittest.skip
    def test_odd0(self):
        # first line
        self.gp = GridPattern(**self.dic1008)
        with self.assertRaises(ValueError):
            self.gp.odd0()
        # no even or odd position given
        self.gp = GridPattern(**self.dic1008)
        self.lines_count = 2
        with self.assertRaises(ValueError):
            self.gp.odd0()
        # even or odd position not true or false
        self.gp = GridPattern(even='c', **self.dic1008)
        self.lines_count = 2
        with self.assertRaises(ValueError):
            self.gp.odd0()

        # middle line even position
        self.gp = GridPattern(even=True, **self.dic1008)
        self.gp._lines_count = 3
        res = self.gp.odd0()
        self.assertEqual(res, '00000000')
        self.assertEqual(self.gp._lines_count, 3)

        # middle line odd position
        self.gp = GridPattern(even=False, **self.dic1008)
        self.gp._lines_count = 6
        res = self.gp.odd0()
        self.assertEqual(res, '00000000')
        self.assertEqual(self.gp.lines_count, 6)

    # unittest.skip
    def test_odd1(self):
        # first line
        self.gp = GridPattern(**self.dic1008)
        with self.assertRaises(ValueError):
            self.gp.odd1()
        # no even or odd position given
        self.gp = GridPattern(**self.dic1008)
        self.lines_count = 2
        with self.assertRaises(ValueError):
            self.gp.odd1()
        # even or odd position not true or false
        self.gp = GridPattern(even='c', **self.dic1008)
        self.lines_count = 2
        with self.assertRaises(ValueError):
            self.gp.odd1()
        # middle line even position
        self.gp = GridPattern(even=True, **self.dic1008)
        self.gp._lines_count = 7
        res = self.gp.odd1()
        self.assertEqual(res, '01010101')
        self.assertEqual(self.gp._lines_count, 7)

        # middle line odd position
        self.gp = GridPattern(even=False, **self.dic1008)
        self.gp._lines_count = 7
        res = self.gp.odd1()
        self.assertEqual(res, '10111110')
        self.assertEqual(self.gp.lines_count, 7)

    # unittest.skip
    def test_shiftLeft_1(self):
        # first line
        self.gp = GridPattern(**self.dic1008)
        with self.assertRaises(ValueError):
            self.gp.shiftLeft1()
        # middle line
        self.gp = GridPattern(**self.dic1008)
        self.gp._lines_count = 7
        res = self.gp.shiftLeft1()
        self.assertEqual(res, '00101001')
        self.assertEqual(self.gp._lines_count, 7)

    # unittest.skip
    def test_shiftLeft0(self):
        # first line
        self.gp = GridPattern(**self.dic1008)
        with self.assertRaises(ValueError):
            self.gp.shiftLeft0()
        # middle line
        self.gp = GridPattern(**self.dic1008)
        self.gp._lines_count = 7
        res = self.gp.shiftLeft0()
        self.assertEqual(res, '00101000')
        self.assertEqual(self.gp.lines_count, 7)

    # unittest.skip
    def test_shiftRight0(self):

        # first line
        self.gp = GridPattern(**self.dic1008)
        with self.assertRaises(ValueError):
            self.gp.shiftRight0()

        # middle line
        self.gp = GridPattern(**self.dic1008)
        self.gp._lines_count = 7
        res = self.gp.shiftRight0()
        self.assertEqual(res, '00001010')
        self.assertEqual(self.gp.lines_count, 7)

    # unittest.skip
    def test_shiftRight1(self):
        # first line
        self.gp = GridPattern(**self.dic1008)
        with self.assertRaises(ValueError):
            self.gp.shiftRight1()
        # middle line
        self.gp = GridPattern(**self.dic1008)
        self.gp._lines_count = 7
        res = self.gp.shiftRight1()
        self.assertEqual(res, '10001010')
        self.assertEqual(self.gp.lines_count, 7)


#@unittest.skip
class Test_patterning_list (unittest.TestCase):

    def test_list(self):
        li = GridPattern.get_SelFuncHEnum()
        res = ['nextSelect', 'idem', 'inverse',
               'all0', 'all1', 'oneEvo', 'zeroEvo',
               'odd0', 'odd1', 'shiftLeft1', 'shiftLeft0',
               'shiftRight0', 'shiftRight1']
        for s in list(li):
            self.assertTrue(s.value in res)


if __name__ == '__main__':

    import unittest

    unittest.main()

