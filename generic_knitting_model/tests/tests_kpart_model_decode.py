#!/usr/bin/env python
# coding:utf-8
"""  SoftKnit21 framework

License
=======

 Copyright (C) <2019>  <Odile Troulet-Lamberttex@orange.fr>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or  any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.



Module purpose
==============

test for kp_knitting_model module

Implements
==========


Documentation
=============


Usage
=====


@author: Odile Troulet-Lambert
@copyright: Odile Troulet-Lambert
@license: GPL
"""

from pathlib import Path
import unittest

import xmlschema as xs

from generic_knitting_model.tests.tests_super_class import Test_super_class
import generic_knitting_model.kpart_model as kpm
import generic_knitting_model.helpers as helpers

import pprint


class Test_kpart_model_decode(Test_super_class, unittest.TestCase):
    """ test that decoding of 2pieces xml file does not produce errors and check nexts and previous"""

    @ classmethod
    def setUpClass(cls):
        cls.ddir = cls.get_tests_datadir(cls, __file__, common=True)
        #cls.schema = helpers.create_schema(kpm.KPartModel)

        cls.files = [Path(cls.ddir, 'manysingulars.xml').resolve(),
                     Path(cls.ddir, 'splits_and_joins.xml').resolve(),
                     Path(cls.ddir, '2pieces.xml').resolve()
                     ]

    def setUp(self):
        test_id = self.id().split('.')[-1]
        self.xml = next((path for path in self.ddir.iterdir() if path.is_file() and test_id[13:] in path.stem))

    def test1_decode_manysingulars(self):
        """check that borders are in the right order in bordersList on right borders of part7"""
        #res = self.schema.decode(self.xml,
                                 #converter=self.converter
                                 #)
        kpmodel = kpm.KPartModel.decode(self.xml)

        segs = kpmodel.kpPiecesList['pieceknit_layer1'].kPartsList[7].bordersList[1]
        for i, s in enumerate(segs):
            with self.subTest(i=i):
                if i < len(segs) - 1:
                    self.assertTrue(s.seg.end == segs[i + 1].seg.start)

    def test2_decode_manysingulars(self):
        """check that bases and borders are coherent"""

        kpmodel = kpm.KPartModel.decode(self.xml)

        kpl = kpmodel.kpPiecesList['pieceknit_layer1'].kPartsList

        def part_cont(pnum1, pnum2, nextp):
            return (kpl[pnum1].bordersList[0][-1].seg.end == kpl[nextp].basePoints[0] and
                    kpl[pnum2].bordersList[1][-1].seg.end, kpl[nextp].basePoints[1])

        self.assertTrue(part_cont(0, 2, 1))
        self.assertTrue(part_cont(1, 5, 3))
        self.assertTrue(part_cont(3, 7, 6))

    def test2_decode_splits_and_joins(self):
        """ check that previous and nexts are correct"""
        previousPartsshouldbe = [
           [],
            [0, 5, 4, 6],
            [],
            [],
            [],
            [],
            [3, 2],
            [1],
            [1],
            [1],
            [1],
            [9],
            [9],
            [12, 17],
            [],
            [],
            [15, 18],
            [14],
            [14],
            [16],
            [13],
            [13],
            [21],
            [20],
            [11],
            [8],
            [7],
        ]
        nextPartsshouldbe = [
            [1],
            [7, 8, 9, 10],
            [6],
            [6],
            [1],
            [1],
            [1],
            [26],
            [25],
            [11, 12],
            [],
            [24],
            [13],
            [20, 21],
            [17, 18],
            [16],
            [19],
            [13],
            [16],
            [],
            [23],
            [22],
            [],
            [],
            [],
            [],
            []]

        kpmodel = kpm.KPartModel.decode(self.xml)

        for i, part in enumerate(kpmodel.kpPiecesList['pieceknit_layer1'].kPartsList):
            with self.subTest(i=i):
                previous = [p for p in part.previous]
                self.assertTrue(all(s.partNumber in previousPartsshouldbe[i] for s in previous))
                nexts = [p for p in part.nexts]
                self.assertTrue(all(s.partNumber in nextPartsshouldbe[i] for s in nexts))

    def test3_decode_2pieces(self):
        #res = self.schema.decode(self.xml,
                                 #converter=self.converter
                                 #)
        kpmodel = kpm.KPartModel.decode(self.xml)


@unittest.skip('temporary')
class Test_kpart_model_errors(Test_super_class, unittest.TestCase):
    """ test assertions and restriction raise error message"""

    @ classmethod
    def setUpClass(cls):
        cls.ddir = cls.get_tests_datadir(cls, __file__, common=True)
        cls.xmlpath = Path(cls.ddir)
        cls.schema = helpers.create_schema(kpm.KPartModel)
        #cls.converter = KpartModelConverter(namespaces=[cls.schema.default_namespace])

    def setUp(self):
        test_id = self.id().split('.')[-1]
        self.xml_errors_dir = Path(self.xmlpath, 'errors')
        self.file = next((path.stem for path in self.xml_errors_dir.iterdir() if path.is_file() and test_id[7:] in path.stem))

        #with open(Path(xml_errors_dir, the_stem + '.xml'), 'r') as f:
            #self.xml = f.read()

    @unittest.expectedFailure
    def test01_duplicate_pieceName(self):
        """test unique-pieceName in kPartModel of  kPartModel.xsd """

        with open(Path(self.xml_errors_dir, self.file + '.xml'), 'r') as f:
            xml = f.read()

        with self.assertRaises(xs.XMLSchemaValidationError) as context:
            kpm.KPartModel.decode(xml)

        ex = context.exception
        self.assertTrue("duplicated value" in ex.reason)
        self.assertTrue("Xsd11Unique" in ex.reason)
        self.assertTrue("unique-pieceName" in ex.reason)
        self.assertTrue('kPartsPiece' in ex.obj.tag)

    def test02_duplicate_partNumber(self):
        """unique-part-number in kpPiece of kPartModel.xsd"""

        with open(Path(self.xml_errors_dir, self.file + '.xml'), 'r') as f:
            xml = f.read()

        with self.assertRaises(xs.XMLSchemaValidationError) as context:
            kpm.KPartModel.decode(xml)

        ex = context.exception
        self.assertTrue("duplicated value (0," in ex.reason)
        self.assertTrue("Xsd11Unique" in ex.reason)
        self.assertTrue('unique-part-number' in ex.reason)

    @unittest.expectedFailure
    def test03_invalid_nexts(self):
        """test test-nexts assertion in kpPiece of kPartModel.xsd"""
        #this test fails with test_nexts
        with open(Path(self.xml_errors_dir, self.file + '.xml'), 'r') as f:
            xml = f.read()

        with self.assertRaises(xs.XMLSchemaValidationError) as context:
            kpm.KPartModel.decode(xml)

        ex = context.exception
        self.assertEqual(ex.validator.id, 'test-nexts')

    @unittest.expectedFailure
    def test04_invalid_previous(self):
        """ test test-previous assertion in kpPiece of kPartModel.xsd"""
        with open(Path(self.xml_errors_dir, self.file + '.xml'), 'r') as f:
            xml = f.read()

        with self.assertRaises(xs.XMLSchemaValidationError) as context:
            kpm.KPartModel.decode(xml)

        ex = context.exception
        self.assertEqual(ex.validator.id, 'test-previous')

    def test05_join_less_than2previous(self):
        """test kpRefsListmin2 in joinPart of kPartModel.xsd"""
        with open(Path(self.xml_errors_dir, self.file + '.xml'), 'r') as f:
            xml = f.read()
        with self.assertRaises(xs.XMLSchemaValidationError) as context:
            kpm.KPartModel.decode(xml)

        ex = context.exception
        self.assertTrue('value length' in ex.reason)

    @unittest.skip('point is encoded as text not as decimal')
    def test06_base_notHorizontal(self):
        """ test test-base-horizontal assertion in baseSeg of kPartModel.xsd"""
        with open(Path(self.xml_errors_dir, self.file + '.xml'), 'r') as f:
            xml = f.read()
        with self.assertRaises(xs.XMLSchemaValidationError) as context:
            kpm.KPartModel.decode(xml)

        ex = context.exception
        self.assertEqual(ex.validator.id, "test-base-horizontal")

    @unittest.skip('point is encoded as text not as decimal')
    def test07_horizontal_line_notHorizontal(self):
        """ test horizontal-line assertion in complexType horizontal-line of kPartModel.xsd"""
        with open(Path(self.xml_errors_dir, self.file + '.xml'), 'r') as f:
            xml = f.read()
        with self.assertRaises(xs.XMLSchemaValidationError) as context:
            kpm.KPartModel.decode(xml)

        ex = context.exception
        pprint.pprint(ex)
        self.assertEqual(ex.validator.id, "test-horizontal-line")

    #@unittest.expectedFailure
    def test08_vertical_line_notVertical(self):
        with open(Path(self.xml_errors_dir, self.file + '.xml'), 'r') as f:
            xml = f.read()
        with self.assertRaises(xs.XMLSchemaValidationError) as context:
            kpm.KPartModel.decode(xml)

        ex = context.exception
        pprint.pprint(ex)
        self.assertEqual(ex.validator.id, "test-vertical-line")

    @unittest.expectedFailure
    def test09_lastnotCastOffPart(self):
        with open(Path(self.xml_errors_dir, self.file + '.xml'), 'r') as f:
            xml = f.read()
        with self.assertRaises(xs.XMLSchemaValidationError) as context:
            kpm.KPartModel.decode(xml)

        ex = context.exception
        self.assertEqual(ex.validator.id, "test-end-castOff")

    @unittest.expectedFailure
    def test10_missing_partNumber(self):
        """test missing partNumber in kPartsList of kPartModel.xsd"""
        with open(Path(self.xml_errors_dir, self.file + '.xml'), 'r') as f:
            xml = f.read()
        with self.assertRaises(xs.XMLSchemaValidationError) as context:
            kpm.KPartModel.decode(xml)

        ex = context.exception
        self.assertEqual(ex.validator.id, "test-missing-part-number")


if __name__ == '__main__':
    import unittest

    unittest.main()
