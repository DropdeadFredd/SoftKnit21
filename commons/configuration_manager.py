# -*- coding: utf-8 -*-
""" This file is part of SoftKnit21 framework

License
=======

 - B{SofKnit21} (U{http://www.softkni21.org}) is Copyright:
  - (C) Odile.Troulet-Lambert@orange.fr

This program is free software; you can redistribute it and/or modify
it under the terms of the Affero GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
Affero GNU General Public License for more details.

You should have received a copy of the Affero GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http://www.gnu.org/licenses/gpl.html}

Module purpose
==============

configuration management

Implements
==========

 - B{configuration handler}

Documentation
=============
contains all the configurations data and functions for :
---SoftKnit21 configuration preferences, (files paths, key shorccuts,....
---knitting machine configuration (hardware accessories, brand, type, ....
---user profile (language, email,......
it also contains a configuration manager to handle the configurations data


Usage
=====

@author: Odile Troulet-Lambert
@copyright: Odile Troulet-Lambert
@license: AGPL
"""

# import pathlib
from pathlib import Path
from SoftKnit21.commons.SoftKnit21Errors import ConfigManagementValueError
from SoftKnit21.commons.tools_for_log import logconf
from gettext import gettext as _
import logging

mlog = logging.getLogger(__name__)


class ConfElement:
    """Abstract class for any configuration element
    to be managed by the configuration manager
    goal :to impose that modification of any configuration element
    leads to a modification of the global configuration """

    def __init__(self, configuration):
        self.globalconf = configuration
        self.default = True

    def update(self):
        self.default = False
        self.globalconf.save()


class Configuration:
    """
    reminder pickle is insecure if it contains external data
    @att : each subclass of ConfElement

    @saved : configuration has been saved
    @type : boolean
    """

    def __init__(self, config_file):
        """ each ConfElement is an attribute
        here self is the global configuration"""
        if config_file:
            self.config_file = config_file
        else:
            parent = Path(__file__).parts[0:-2]
            self.config_file = Path(*parent, 'local_storage', 'local_data', 'config.pickle')
        for element in ConfElement.__subclasses__():
            # create all elementConfs
            v = element(self)
            attributeName = element.__name__[0].lower() + element.__name__[1:]
            setattr(self, attributeName, v)
        self._saved = False

    def save(self):
        """ saves the config file; by defaut, the config file is in the data directory under the main directory"""

        import pickle
        with open(self.config_file, 'wb') as f:
            pickle.dump(self, f, pickle.HIGHEST_PROTOCOL)
        self._saved = True

    @property
    def saved(self):
        return self._saved


# this is a global variable
# the first time the configuration module is loaded this variable refers to the configuration
_configuration = None

# global function


def get_configuration(forceNew=False, config_file=None):
    """creates or reads the configuration at first import of the module
    loads the configuration file if it exists.
    If it does not exist, creates a default configuration and saves it"""

    global _configuration
    if _configuration is None:
        import pickle
        if config_file and config_file.exists() and not forceNew:
            mlog.log(logging.DEBUG, f'reading existing configuration')
            with open(config_file, 'rb') as f:
                conf = pickle.load(f)
        else:
            if not forceNew:
                mlog.log(logging.DEBUG, f'no configuration exists, creating default one')
            conf = Configuration(config_file)
            conf.save()
        _configuration = conf
    else:
        mlog.log(logging.DEBUG, f'configuration already loaded')
    return _configuration


# def _get_baseDir():
    # """ global function
    # returns the base directory"""
    #current_file = Path.cwd()
    #base_dir = current_file.parent
    # return base_dir


# def _get_configFileName():
    # """global function
    # return the absolute path of the config file name"""
    #base_dir = _get_baseDir()
    #config_file = Path(base_dir / 'data' / 'config.pickle')
    # return config_file


# def get_test_configuration(forceNew=False):

    #config_file = _get_testConfigFileName()
    # return get_configuration(forceNew, config_file, test=True)


# class TestConfiguration (Configuration):
    # """ defines a specific configuration for tests so as not to
    # destroy existing real configuration"""

    # def save(self):
        # super().save(_get_testConfigFileName())

    # def delete(self):
        #""" deletes the test config if there is one"""
        #p = _get_testConfigFileName()
        #ex = Path(p).exists()
        # if ex:
            # Path(p).unlink()


# def _get_testConfigFileName():
    #""" returns the path for the test config"""
    #p = _get_testBaseDir()
    # return Path(p / 'data' / 'config.pickle')


# def _get_testBaseDir():
    #""" returns the directory in which to save data for the tests"""
    #p = Path.cwd()
    #l = PurePath(p).parts
    # if 'tests' in l[-1]:
        # the module is from a tests sub directory
        #test_base_dir = p
    # elif 'tests' in l:
        #test_base_dir = p.parent
    # else:
        #test_base_dir = p.parent / 'tests'
    # return test_base_dir


class LockControlerConf(ConfElement):
    """ manages all elements of the LockControlerConfiguration;
    no update by the user;
    some are read from the lock controler in a message received from the LC
    others are given by the manufacturer of the LC    
    """

    def __init__(self, globalconf):
        """creates the default configuration which is empty """
        super().__init__(globalconf)

    def fill(self, *dic, **kwargs):
        """ fills lock configuration with the attributes in the dictionnary"""
        for dictionary in dic:
            for key in dictionary:
                setattr(self, key, dictionary[key])
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def update(self):
        """ saves the global configuration"""
        super().update()

    def get_element(self, name):
        """ returns the value of one element of the LockControlerConfiguration
        @param key : key for the element of the LockControlerConfiguration
        @type : string in the set of keys for the LockControlerConfiguration dictionnary"""
        try:
            return getattr(self, name)
        except:
            return None


class PathsConfig (ConfElement):
    """ manages all paths for accessing permanent data
    @att _base_dir : base directory for SoftKnit21
    @type ; internal parameter

    @att stitchTech : path to the stitchTechDatabase
    @type : pure Path

    @att gridPatterns : path to the standard GridPatterns
    @type : pure Path

    @att _default : states if the PathConfiguration is a default one
    @type : boolean"""

    def __init__(self, globalconf):
        """ creates the default configuration for paths configuration"""
        base_dir = globalconf.config_file.parts[0:-1]
        self.stitchTech = Path(*base_dir, 'stitchtech.sqlite3')
        self.gridPatterns = Path(*base_dir, 'gridpatterns')
        self.models = Path(*base_dir, 'knitting_models')
        self.default = True
        super().__init__(globalconf)

    def getGridPatternsDir(self, origin):
        if origin == 'E6000':
            return self.gridPatterns / 'E6000_gridpatterns'
        if origin == 'E8000':
            return self.gridPatterns / 'E8000_gridpatterns'
        else:
            raise ConfigManagementValueError(
                _(f'{origin} presently only E6000 and E8000 grid Patterns are available in SoftKnit21'))

    def getModelsDir(self):
        return self.models

    def update(self):
        """ modification of the configuration by the user"""
        super().update()


MACHINESUPPORTED = ['passapE6000']
machine_model = 'passapE6000'


def getMachineModel():
    if (machine_model == None or
            machine_model not in MACHINESUPPORTED):
        raise ConfigManagementError(
            _(f' this type of machine is not yet supported'))
    else:
        return machine_model


if __name__ == '__main__':

    # this works global variable is correct
    #gconf = get_configuration(forceDefault=True)
    # print (f'global variable:  {_configuration}')

    import unittest
    import logging
    # do not run tests for this module from here because it does not use test configuration
    #from common.tests.tests_configuration_manager import TestConfigurationClass, TestConfigurationElement
    #from common.tests.tests_configuration_manager import TestLockControlerConf, TestPathConfig
    #from common.tests.tests_configuration_manager import TestGetConfiguration
    unittest.main()





