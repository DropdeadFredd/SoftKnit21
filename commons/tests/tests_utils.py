    #!/usr/bin/env python
    # coding:utf-8
"""
      This file is part of SoftKnit21 framework

    License
    =======

     Copyright (C) <2019>  <Odile Troulet-Lamberttex@orange.fr>

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as
        published by the Free Software Foundation, either version 3 of the
        License, or  any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU Affero General Public License for more details.

        You should have received a copy of the GNU Affero General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.



    Module purpose
    ==============

    Test Generic Machine classes and functions

    Implements
    ==========



    Documentation
    =============


    Usage
    =====


    @author: Odile Troulet-Lambert
    @copyright: Odile Troulet-Lambert
    @license: AGPL
    """

import unittest


#from SoftKnit21.knitting_machines.utils import CamEnum, HEnum
from knitting_machines.utils import *
from knitting_machines.generic_machine import GLock
from SoftKnit21.commons.SoftKnit21Errors import *
from SoftKnit21.generic_knitting_model.grid_pattern import GridPattern

selFuncEnum = GridPattern.get_SelFuncHEnum()


#@unittest.skip
class Test_HEnum (unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        class MyHEnum (HEnum):
            VALUE1 = ('V1', 'label', 'help1')
            VALUE2 = ('V2', 'label2', 'help2')

        cls.MyHEnum = MyHEnum

    def test(self):

        self.assertEqual(self.MyHEnum.VALUE1.value, 'V1')
        self.assertEqual(self.MyHEnum.VALUE1.label, 'label')
        self.assertEqual(self.MyHEnum.VALUE1.help, 'help1')
        self.assertEqual(self.MyHEnum.VALUE2.value, 'V2')
        self.assertEqual(self.MyHEnum.VALUE2.label, 'label2')
        self.assertEqual(self.MyHEnum.VALUE2.help, 'help2')

    def test_check_compatible_no_optional(self):
        class OtherHEnum(HEnum):
            VALUE1 = ('V1', 'newlabel', 'newhelp1')
            VALUE2 = ('V2', 'newlabel2', 'newhelp2')

        res = self.MyHEnum.check_compatible(OtherHEnum)

        self.assertIsNone(res)

    def test_check_compatible_optional(self):
        class OtherHEnum(HEnum):
            VALUE1 = ('V1', 'newlabel', 'newhelp1')

        res = self.MyHEnum.check_compatible(OtherHEnum,
                                            optional=self.MyHEnum.VALUE2)

        self.assertIsNone(res)

    def test_check_compatible_missing_value(self):
        class OtherHEnum(HEnum):
            VALUE1 = ('V1', 'newlabel', 'newhelp1')

        with self.assertRaises(BadImplementationError) as e:
            res = self.MyHEnum.check_compatible(OtherHEnum)

        ex = e.exception.__cause__
        self.assertTrue('cannot be omited' in ex.args[0], msg=f'ex.args[0] is {ex.args[0]}')

    def test_check_compatible_wrong_value(self):
        class OtherHEnum(HEnum):
            VALUE1 = ('V3', 'newlabel', 'newhelp1')
            VALUE2 = ('V2', 'newlabel2', 'newhelp2')

        with self.assertRaises(BadImplementationError) as e:
            res = self.MyHEnum.check_compatible(OtherHEnum
                                                )

        ex = e.exception.__cause__
        self.assertTrue('VALUE1 cannot be overriden' in ex.args[0], msg=f'ex.args[0] is {ex.args[0]}')


class Test_SelCamEnum (unittest.TestCase):

    def test1(self):
        class MyCamEnum (SelCamEnum):
            VALUE1 = ('V1', 'label', 'help1', GLock.Passage.BOTH_DIRECTIONS, selFuncEnum.INVERSE, '_P')
            VALUE2 = ('V2', 'label2', 'help2', GLock.Passage.RIGHT_TO_LEFT, selFuncEnum.IDEM, 'str')
        self.assertEqual(MyCamEnum.VALUE1.value, 'V1')
        self.assertEqual(MyCamEnum.VALUE1.label, 'label')
        self.assertEqual(MyCamEnum.VALUE1.lockPass, GLock.Passage.BOTH_DIRECTIONS)
        self.assertEqual(MyCamEnum.VALUE1.selFunc, selFuncEnum.INVERSE)
        self.assertEqual(MyCamEnum.VALUE2.value, 'V2')
        self.assertEqual(MyCamEnum.VALUE2.label, 'label2')
        self.assertEqual(MyCamEnum.VALUE2.lockPass, GLock.Passage.RIGHT_TO_LEFT)
        self.assertEqual(MyCamEnum.VALUE2.selFunc, selFuncEnum.IDEM)
        self.assertEqual(MyCamEnum.VALUE2.help, 'help2')
        self.assertEqual(MyCamEnum.VALUE1.help, 'help1')

    def test_validate_correct(self):

        class MyCamEnum (SelCamEnum):
            VALUE1 = ('V1', 'label', 'help1', GLock.Passage.BOTH_DIRECTIONS, selFuncEnum.INVERSE, '_P')
            VALUE2 = ('V2', 'label2', 'help2', GLock.Passage.RIGHT_TO_LEFT, selFuncEnum.IDEM, 'wrongkey')

        res = MyCamEnum.VALUE1.validate([('lockPass', 'enum', GLock.Passage),
                                         ('selFunc', 'enum', selFuncEnum),
                                         ('bedCodingKeys', 'dic', {'_P': 'value', 'otherkey': 'othervalue'})
                                         ])
        self.assertTrue(res)

    def test_validate_wrong_Key(self):

        class MyCamEnum (SelCamEnum):
            VALUE1 = ('V1', 'label', 'help1', GLock.Passage.BOTH_DIRECTIONS, selFuncEnum.INVERSE, 'P')
            VALUE2 = ('V2', 'label2', 'help2', GLock.Passage.RIGHT_TO_LEFT, selFuncEnum.IDEM, 'wrongkey')

        with self.assertRaisesRegex(BadImplementationError, 'wrongkey') as e:
            MyCamEnum.VALUE2.validate([('lockPass', 'enum', GLock.Passage),
                                       ('selFunc', 'enum', selFuncEnum),
                                       ('bedCodingKeys', 'list', ['P'])
                                       ])

    def test_validate_wrong_type(self):
        class MyCamEnum (SelCamEnum):
            VALUE1 = ('V1', 'label', 'help1', GLock.Passage.BOTH_DIRECTIONS, selFuncEnum.INVERSE, 'P')
            VALUE2 = ('V2', 'label2', 'help2', GLock.Passage.RIGHT_TO_LEFT, selFuncEnum.IDEM, 'P')
        with self.assertRaisesRegex(BadImplementationError, 'cannot validate') as e:
            MyCamEnum.VALUE2.validate([('lockkPas', 'enum', GLock.Passage),
                                       ('selFuncEnum', 'enum', selFuncEnum),
                                       ('bedCodingKeys', 'dic', {'_P': 'value', 'otherkey': 'othervalue'})
                                       ])


if __name__ == '__main__':
    import unittest

    unittest.main()




