#!/usr/bin/env python
# coding:utf-8
"""
  This file is part of SoftKnit21 framework
  
License
=======

 Copyright (C) <2019>  <Odile Troulet-Lamberttex@orange.fr>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or  any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.



Module purpose
==============

Main

Implements
==========



Documentation
=============

    
Usage
=====


@author: Odile Troulet-Lambert
@copyright: Odile Troulet-Lambert
@license: AGPL

"""

from LC_model.protocol_manager import LCProtocolMgr
from presenter_controler.presenter import Presenter
from common.tools_for_log import logconf
import logging
import queue
import threading

# this is a global variable
LCConnection_is_OK = False


def main():
    """ creates the LCProtocol Manager, the current work and the Knitting machine objects """

    mlog.log(logging.DEBUG, 'creating LC protocol Manager')

    try:
        eventsQueue = queue.SimpleQueue()
        pm = LCProtocolMgr(eventsQueue)
        pres = Presenter()
        pres.configure_LCProtocolMgr(pm)
        pm.configure_presenter(pres)

        # triggers the first transition in the FSM"""
        pm.trigger('on_start')

        while 1 and not pm.LCConnection_is_OK:
            pm.trigger(eventsQueue.get())

            # during this time some usr requests may be transmitted to the user
            # the GUI app is not yet started a progress bar might be useful"""

        # configures the knitting_machine using the LockControlerConfiguration
        # to be written in configuration management module and called here

        # pass the queue reference to the presenter which will create the thread
        pres.configure_queue(self.eventsQueue)
        pres.start()

        # here the PresenterControler takes the lead to guide the user to choose
        # the knitobject or swatch to be knitted (see pencil sketch in start)
        # the Presenter calls model functions from configuration manager to get configuration elements
        # to be displaid
                # call to a presenter function to be added here for this purpose

        # here the swatch to be knitted or the piece  of the swatched knitobject to be knitted
        # is chosen by the user and called chosen here
        running_knit = RunningKnit(chosen)
        pm.configure_running_knit(running_knit)

        while 1:
            pm.trigger(eventsQueue.get())

    except Exception as e:
        mlog.log(logging.CRITICAL, 'fatal error in LC protocol manager creation', exc_info=e)


if __name__ == '__main__':

    mlog = logconf(name=__file__)
    main()

