# -*- coding: utf-8 -*-
""" This file is part of SoftKnit21 framework

License
=======

 - B{SofKnit21} (U{http://www.softkni21.org}) is Copyright:
  - (C) Odile.Troulet-Lambert@orange.fr

This program is free software; you can redistribute it and/or modify
it under the terms of the Affero GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
Affero GNU General Public License for more details.

You should have received a copy of the Affero GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http://www.gnu.org/licenses/gpl.html}

Module purpose
==============

common helpers

Implements
==========

 - B{helpers}

Documentation
=============

    helpers functions common to all modules

Usage
=====

@author: Odile Troulet-Lambert
@copyright: Odile Troulet-Lambert
@license: AGPL
"""

from pathlib import Path, PurePath
from gettext import gettext as _


# def key_for_value(value, dic):
    # for k in dic.keys():
        # if dic[k] == value:
            # return k
    # else:
        # raise SoftKnit21Error(
            # _(f" no key found for {value} in {dic}"))


def _check_string_binary(string):
    import re
    if re.search('[^0^1]', string) == None:
        return True
    else:
        return False


def contains(small, big):
    """ test inclusion of small list in big list
    returns False is list not included
    returns indices of the big list corresponding to the small list
    @param small : small list
    @type : list

    @param small : big list
    @type : list
    """
    for i in range(len(big) - len(small) + 1):
        for j in range(len(small)):
            if big[i + j] != small[j]:
                break
        else:
        # else of the for close not of the if clause
        # runs at the end of the for loop if no break occured
                return i, i + len(small)
    return False


def is_gen_empty(iterable):
    """ returns True if iterable is empty false otherwise"""
    try:
        first = next(iterable)
    except StopIteration:
        return True
    return False


if __name__ == "__main__":

    import unittest
    import time

    unittest.main()
