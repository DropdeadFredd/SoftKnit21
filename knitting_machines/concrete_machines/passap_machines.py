#!/usr/bin/env python
# coding:utf-8
"""
  This file is part of SoftKnit21 framework

License
=======

 Copyright (C) <2019>  <Odile Troulet-Lamberttex@orange.fr>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or  any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.



Module purpose
==============

implementation for PassapE6000 of all the functions and data of ModelSpec abstract class.

Implements
==========

 - B{PassapE6000Spec}

Documentation
=============

    contains PASSAP specific functions called by Machine Class

Usage
=====


@author: Odile Troulet-Lambert
@copyright: Odile Troulet-Lambert
@license: AGPL
"""

import itertools
from gettext import gettext as _

from SoftKnit21.commons.utils import *
from SoftKnit21.commons.SoftKnit21Errors import *
from SoftKnit21.generic_knitting_model.grid_pattern import GridPattern

from SoftKnit21.knitting_machines.generic_machine import *


__all__ = ['E6000', 'Duo80']

selFuncEnum = GridPattern.get_SelFuncHEnum()


class PasNeedlesBed(GNeedlesBed):

    bedCodingKeys = ['N', 'P', 'PUW']

    @ staticmethod
    def validate_coding(bed, tuples):
        """ returns True if the coding for the needle bed is correct
        else raises a ValidationError exception
        must check that all tuples have the same length"""
        bedtup = [(name, value) for (name, value) in tuples if bed in name]
        length = len(bedtup[0][1])
        if not all(len(value) == length for (name, value) in bedtup):
            raise ValidationError(f'all fields of initial {bed} must have the same length')
        #bed_setting = [(name.removeprefix(bed + '_')[-1] * length, value) for (name, value) in bedtup]
        bed_setting = {name.removeprefix(bed + '_'): value for (name, value) in bedtup}
        for i in range(length):
            if (bed_setting['N'][i] == '0' and bed_setting['P'][i] == '1' and bed_setting['PUW'][i] == '1'):
                raise ValidationError(f'{bed} cannot have P=1 and PUW=1 and N=1for pusher {i}')
            if (bed_setting['P'][i] == '1' and bed_setting['PUW'][i] == '0'):
                raise ValidationError(f'{bed} cannot have P=1 and PUW=0 for pusher {i}')
        return True

    @ classmethod
    def number_of_needles(cls):
        return 181

    @ classmethod
    def get_needles_numbering(cls):
        """ needles numbering schema"""

        return list(itertools.chain(range(-90, 1), range(1, 90)))


GNeedlesBed.register(PasNeedlesBed)


class PasLock (GLock):

    doubleFeeder = False

    @classmethod
    def start_position(cls):
        """ lock start position"""
        return GLock.Position.RIGHT_POSITION


GLock.register(PasLock)


class PasStitchCamCmd (GCamCmd):
    bedCodingKey = 'P'

    camValues = [StitchCamValue(value='N', direction=GLock.Direction.RIGHT_DIRECTION, patterning=StitchActions .KNIT, nonPatterning=StitchActions .KNIT),
                 StitchCamValue(value='AX', direction=GLock.Direction.RIGHT_DIRECTION, patterning=StitchActions .KNIT, nonPatterning=StitchActions .TUCK),
                 StitchCamValue(value='BX', direction=GLock.Direction.RIGHT_DIRECTION, patterning=StitchActions .KNIT, nonPatterning=StitchActions .SLIP),
                 StitchCamValue(value='CX', direction=GLock.Direction.RIGHT_DIRECTION, patterning=StitchActions .SLIP, nonPatterning=StitchActions .SLIP),
                 StitchCamValue(value='DX', direction=GLock.Direction.RIGHT_DIRECTION, patterning=StitchActions .SLIP, nonPatterning=StitchActions .SLIP),
                 StitchCamValue(value='EX', direction=GLock.Direction.RIGHT_DIRECTION, patterning=StitchActions .TUCK, nonPatterning=StitchActions .TUCK),
                 StitchCamValue(value='FX', direction=GLock.Direction.RIGHT_DIRECTION, patterning=StitchActions .KNIT, nonPatterning=StitchActions .TUCK),
                 StitchCamValue(value='HX', direction=GLock.Direction.RIGHT_DIRECTION, patterning=StitchActions .SLIP, nonPatterning=StitchActions .SLIP),
                 StitchCamValue(value='GX', direction=GLock.Direction.RIGHT_DIRECTION, patterning=StitchActions .NOTHING, nonPatterning=StitchActions .NOTHING),
                 StitchCamValue(value='N', direction=GLock.Direction.LEFT_DIRECTION, patterning=StitchActions .KNIT, nonPatterning=StitchActions .KNIT),
                 StitchCamValue(value='AX', direction=GLock.Direction.LEFT_DIRECTION, patterning=StitchActions .KNIT, nonPatterning=StitchActions .TUCK),
                 StitchCamValue(value='BX', direction=GLock.Direction.LEFT_DIRECTION, patterning=StitchActions .KNIT, nonPatterning=StitchActions .SLIP),
                 StitchCamValue(value='CX', direction=GLock.Direction.LEFT_DIRECTION, patterning=StitchActions .KNIT, nonPatterning=StitchActions .KNIT),
                 StitchCamValue(value='DX', direction=GLock.Direction.LEFT_DIRECTION, patterning=StitchActions .KNIT, nonPatterning=StitchActions .TUCK),
                 StitchCamValue(value='EX', direction=GLock.Direction.LEFT_DIRECTION, patterning=StitchActions .KNIT, nonPatterning=StitchActions .KNIT),
                 StitchCamValue(value='FX', direction=GLock.Direction.LEFT_DIRECTION, patterning=StitchActions .KNIT, nonPatterning=StitchActions .KNIT),
                 StitchCamValue(value='HX', direction=GLock.Direction.LEFT_DIRECTION, patterning=StitchActions .KNIT, nonPatterning=StitchActions .SLIP),
                 StitchCamValue(value='GX', direction=GLock.Direction.LEFT_DIRECTION, patterning=StitchActions .NOTHING, nonPatterning=StitchActions .NOTHING)
                 ]

    def get_setting(self, name):
        pass

    def set_setting(self, value):
        pass


GCamCmd.register(PasStitchCamCmd)


class BackPushersArrows (GCamCmd):
    bedCodingKey = 'P'
    name = 'BB_PushersArrows'
    SelectionCam = True

    camValues = [SelectionCamValue(name='ARROW_LEFT_TO_RIGHT',
                                   value='L',
                                   label=_('left to right Arrow'),
                                   help=_('inverse back bed pushers position in left to right passage'),
                                   direction=GLock.Direction.LEFT_DIRECTION,
                                   selection=selFuncEnum.INVERSE,
                                   ),
                 SelectionCamValue(name='ARROW_RIGHT_TO_LEFT',
                                   value='R',
                                   label=_('right to left Arrow'),
                                   help=_('inverse back bed pushers position in right to left passage'),
                                   direction=GLock.Direction.RIGHT_DIRECTION,
                                   selection=selFuncEnum.INVERSE,
                                   ),
                 SelectionCamValue(name='NEUTRAL',
                                   value='O',
                                   label=_('No Action'),
                                   help=_('No action on pushers'),
                                   direction=GLock.Direction.LEFT_DIRECTION,
                                   selection=selFuncEnum.IDEM,
                                   ),
                 SelectionCamValue(name='NEUTRAL',
                                   value='O',
                                   label=_('No Action'),
                                   help=_('No action on pushers'),
                                   direction=GLock.Direction.RIGHT_DIRECTION,
                                   selection=selFuncEnum.IDEM,
                                   ),
                 SelectionCamValue(name='BOTH_ARROWS',
                                   value='B',
                                   label=_('both arrows'),
                                   help=_('inverse back pushers in both directions'),
                                   direction=GLock.Direction.RIGHT_DIRECTION,
                                   selection=selFuncEnum.INVERSE,
                                   ),
                 SelectionCamValue(name='BOTH_ARROWS',
                                   value='B',
                                   label=_('both arrows'),
                                   help=_('inverse back pushers in both directions'),
                                   direction=GLock.Direction.LEFT_DIRECTION,
                                   selection=selFuncEnum.INVERSE,
                                   ),
                 ]

    def get_setting(self, name):
        pass

    def set_setting(self, value):
        pass


GCamCmd.register(BackPushersArrows)


class PasStitchSize (GStitchSize):

    @ classmethod
    def get_button_positions(cls):
        """ list of possible stitch sizes"""
        stitchsizes = list(range(1, 9))
        fourth = ['', '1/4', '1/2', '3/4']
        index = itertools.cycle(fourth)
        sizesonbutton = []
        for i in stitchsizes:
            sizesonbutton.append(str(int(i / 4)) + ' ' + next(index))
        return sizesonbutton

    @classmethod
    def get_positions(self):
        return list(range(0, 33))


GStitchSize.register(PasStitchSize)


class PasYarnMast (GYarnMast):
    """ list of tension positions on the mast"""
    Tension = HEnum('Tension',
                    [(str(num), (str(num), '', '')) for num in range(1, 8)],
                    module=__name__,
                    )

    @ classmethod
    def get_tension_positions(cls):
        """ list of thread tension positions on the mast"""
        pass


GYarnMast.register(PasYarnMast)


class PasKThread (GKThread):
    pass


GKThread.register(PasKThread)


class PasRackingLevel (GRackingLevel):

    @ classmethod
    def get_positions(cls):
        """ list of possible racking numbers"""
        return list(range(-3, +4))

    @classmethod
    def get_initial(cls, alignment):
        if alignment == GNeedlesBed.Alignment.FULL_PITCH_ALIGNMENT:
            return 0
        else:
            return 1


GRackingLevel.register(PasRackingLevel)


class PasSinkers(HEnum):

    ORANGE = ('O', _('orange'), _('used for double bed'))
    BLACK = ('B', _('black'), _('used for single bed'))
    BLUE = ('L', _('blue'), _('used for double bed thick knit'))


class FrontLockSetting (PasStitchCamCmd):
    """a set of cam commands which contribute to select the active cam track on the lock """
    bed = GNeedlesBed.BedName.FRONT_BED
    name = 'FB_LockSetting'
    camControler = True
    bedCodingKey = 'P'
    camValues = [
        StitchCamValue(value='N', direction=GLock.Direction.RIGHT_DIRECTION, patterning=StitchActions .KNIT, nonPatterning=StitchActions .KNIT),
        StitchCamValue(value='KX', direction=GLock.Direction.RIGHT_DIRECTION, patterning=StitchActions .KNIT, nonPatterning=StitchActions .TUCK),
        StitchCamValue(value='LX', direction=GLock.Direction.RIGHT_DIRECTION, patterning=StitchActions .SLIP, nonPatterning=StitchActions .KNIT),
        StitchCamValue(value='CX', direction=GLock.Direction.RIGHT_DIRECTION, patterning=StitchActions .KNIT, nonPatterning=StitchActions .KNIT),
        StitchCamValue(value='OX', direction=GLock.Direction.RIGHT_DIRECTION, patterning=StitchActions .KNIT, nonPatterning=StitchActions .TUCK),
        StitchCamValue(value='EX', direction=GLock.Direction.RIGHT_DIRECTION, patterning=StitchActions .KNIT, nonPatterning=StitchActions .KNIT),
        StitchCamValue(value='SX', direction=GLock.Direction.RIGHT_DIRECTION, patterning=StitchActions .SLIP, nonPatterning=StitchActions .SLIP),
        StitchCamValue(value='GX', direction=GLock.Direction.RIGHT_DIRECTION, patterning=StitchActions .NOTHING, nonPatterning=StitchActions .NOTHING),
        StitchCamValue(value='UX', direction=GLock.Direction.RIGHT_DIRECTION, patterning=StitchActions .KNIT, nonPatterning=StitchActions .SLIP),

        StitchCamValue(value='N', direction=GLock.Direction.LEFT_DIRECTION, patterning=StitchActions .KNIT, nonPatterning=StitchActions .KNIT),
        StitchCamValue(value='KX', direction=GLock.Direction.LEFT_DIRECTION, patterning=StitchActions .KNIT, nonPatterning=StitchActions .TUCK),
        StitchCamValue(value='LX', direction=GLock.Direction.LEFT_DIRECTION, patterning=StitchActions .SLIP, nonPatterning=StitchActions .KNIT),
        StitchCamValue(value='CX', direction=GLock.Direction.LEFT_DIRECTION, patterning=StitchActions .SLIP, nonPatterning=StitchActions .SLIP),
        StitchCamValue(value='OX', direction=GLock.Direction.LEFT_DIRECTION, patterning=StitchActions .SLIP, nonPatterning=StitchActions .SLIP),
        StitchCamValue(value='EX', direction=GLock.Direction.LEFT_DIRECTION, patterning=StitchActions .TUCK, nonPatterning=StitchActions .TUCK),
        StitchCamValue(value='SX', direction=GLock.Direction.LEFT_DIRECTION, patterning=StitchActions .SLIP, nonPatterning=StitchActions .SLIP),
        StitchCamValue(value='GX', direction=GLock.Direction.LEFT_DIRECTION, patterning=StitchActions .NOTHING, nonPatterning=StitchActions .NOTHING),
        StitchCamValue(value='UX', direction=GLock.Direction.LEFT_DIRECTION, patterning=StitchActions .KNIT, nonPatterning=StitchActions .TUCK),
     ]


class BackLockSetting (PasStitchCamCmd):
    bed = GNeedlesBed.BedName.BACK_BED
    name = 'BB_LockSetting'


class E6000 (metaclass=GenericMachine,
             numberOfYarnMast=4,
             brandname='Passap',
             mainBed=GNeedlesBed.BedName.FRONT_BED,
             RackingLevel=PasRackingLevel,
             Lock=PasLock,
             StitchSize=PasStitchSize,
             YarnMast=PasYarnMast,
             NeedlesBed={GNeedlesBed.BedName.FRONT_BED: PasNeedlesBed,
                         GNeedlesBed.BedName.BACK_BED: PasNeedlesBed},
             CamCmd={GNeedlesBed.BedName.FRONT_BED: FrontLockSetting,
                     GNeedlesBed.BedName.BACK_BED: [BackLockSetting, BackPushersArrows]}
             ):
    ''' Electronic machine sold under Passap and Pfaff brand name'''
    pass


class Duo80 (metaclass=GenericMachine,
             numberOfYarnMast=2,
             brandname='Passap',
             mainBed=GNeedlesBed.BedName.FRONT_BED,
             RackingLevel=PasRackingLevel,
             Lock=PasLock,
             StitchSize=PasStitchSize,
             YarnMast=PasYarnMast,
             NeedlesBed={GNeedlesBed.BedName.FRONT_BED: PasNeedlesBed,
                         GNeedlesBed.BedName.BACK_BED: PasNeedlesBed},
             CamCmd={GNeedlesBed.BedName.FRONT_BED: PasStitchCamCmd,
                     GNeedlesBed.BedName.BACK_BED: [PasStitchCamCmd, BackPushersArrows]}
             ):
    ''' Non electronic machine sold under Passap and Pfaff brand name'''
    pass


setattr(Duo80, 'sinkersEnum', PasSinkers)
setattr(E6000, 'sinkersEnum', PasSinkers)


if __name__ == '__main__':

    import unittest
    unittest.main()
