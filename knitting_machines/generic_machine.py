    # !/usr/bin/env python
# coding:utf-8
"""
  This file is part of SoftKnit21 framework

License
=======

 Copyright (C) <2019>  <Odile Troulet-Lamberttex@orange.fr>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or  any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.



Module purpose
==============

This module contains the building blocks to create concrete knitting machines

Implements
==========

 different components of a concrete knitting machine. Each concrete machine must define a concrete component which inherits from
 the generic component and register as a subclass of the generic component so as to verify that each abstract method has been redefined.
 Components categories are:
 optional components which may or may not be present in a machine
 multiple components : the generic machine defines one component which may be duplicated in a concrete machine
 each component defines some enums but for the moment these enums do not become

Documentation
=============



Usage
=====


@author: Odile Troulet-Lambert
@copyright: Odile Troulet-Lambert
@license: AGPL
"""
from abc import abstractmethod, ABCMeta
from dataclasses import dataclass
from gettext import gettext as _
import enum
import inspect
import enum
import sys


from SoftKnit21.commons.SoftKnit21Errors import *
from SoftKnit21.commons.utils import HEnum
#import classutilities
from SoftKnit21.generic_knitting_model.grid_pattern import GridPattern

__all__ = ['GenericMachine', 'GCamCmd', 'GLock', 'GStitchSize', 'GNeedlesBed', 'GYarnMast', 'GKThread', 'GRackingLevel',
           'StitchActions', 'StitchCamValue', 'SelectionCamValue']

SelFuncEnum = GridPattern.get_SelFuncHEnum()


def gcomponent(cls):
    setattr(cls, 'gcomp', True)
    return cls


class GenericComponent():
    @classmethod
    def __subclasshook__(meta, cls):
        if not len(cls.__abstractmethods__) == 0:
            raise BadImplementationError(f'{cls.__name__} has abstractmethods which are not overwritten')
        return True


class StitchActions (HEnum):
    KNIT = 'K', _('Knit'), _('a regular stitch')
    TUCK = 'T', _('Tuck'), _('an incompletely formed stitch')
    SLIP = 'L', _('Slip'), _('a missed stitch')
    NOTHING = 'N', _('No action'), _('the needle does not move')


@ gcomponent
class GLock (GenericComponent, metaclass=ABCMeta):
    """ the lock of the machine as a whole
    for machine with single bed the Glock contains one lockPart, the back one"""

    double_feeder = False
    yarns_in_double_feeder = 2

    class Position (HEnum):
        LEFT_POSITION = ('LP', _('Left side'), '')
        RIGHT_POSITION = ('RP', _('Right side'), '')

    class Direction (HEnum):
        LEFT_DIRECTION = ('LD', _('Left to Right'), '')
        RIGHT_DIRECTION = ('RD', _('Right to Left'), '')

        @classmethod
        def opposite(cls, value):
            """returns opposite LockDirectionEnum"""
            if value == cls.LEFT_DIRECTION:
                return cls.RIGHT_DIRECTION
            else:
                return cls.LEFT_DIRECTION

    @ classmethod
    def __subclasshook__(meta, cls):
        """this is to verify that Lock.Direction and Lock.Position are not redefined in the concrete machine"""
        if not cls.Position == meta.Position or not cls.Direction == meta.Direction:
            raise BadImplementationError(f' redefinition of Position or Direction in {cls} is not allowed')
        super().__subclasshook__(cls)
        return True

    def get_lock_start_position():
        """ start position of the lock default is right"""
        return GLock.Position.RIGHT_POSITION


@dataclass(kw_only=True, frozen=True)
class CamValue:
    """a dataclass describing any possible value of CamCmd"""
    value: str
    direction: GLock.Direction
    name: str = ''
    label: str = ''
    help: str = ''

    def __post_init__(self):
        if self.name == '':
            self.__set_name()
        if self.label == '':
            self.__set_label()

        if len(self.value) > 3:
            raise BadImplementationError(f"length of value must be lower or equal to 3")
        for field_name, field_info in self.__dataclass_fields__.items():
            value = getattr(self, field_name)
            if isinstance(field_info.type, enum.EnumMeta):
                if not value in list(field_info.type):
                    raise BadImplementationError(f" {value} of field {field_name} is not a member of {field_info.type}")
            elif type(value) != field_info.type:
                raise BadImplementationError(f"Invalid type of '{type(value).__name__}' on field {field_name}")

    def __set_name(self):
        """necessary to change the name of a frozen dataclass"""
        object.__setattr__(self, 'name', self.value.upper())

    def __set_label(self):
        """necessary to change the label of a frozen dataclass"""
        object.__setattr__(self, 'label', self.name)

    @classmethod
    def check_allValues(cls, listofvalues, cmdclass):
        """returns True if all values are the same class otherwise raises BadImplementationError"""
        try:
            assert all((isinstance(cv, cls) for cv in listofvalues))
        except AssertionError as e:
            raise BadImplementationError(f' in {cmdclass} all camValues must be instances of {cls.__name__}')


@dataclass(frozen=True)
class StitchCamValue (CamValue):
    """a dataclass which describes one possible action of a StitchCamCmd"""
    patterning: StitchActions
    nonPatterning: StitchActions

    def stitch_tuple(self):
        return (self.value, (self.patterning, self.nonPatterning))


@dataclass(frozen=True)
class SelectionCamValue (CamValue):
    """A dataclass which describes one possible action of a SelectionCamCmd
    """
    selection: SelFuncEnum

    def selFunc(self):
        return (self.value, self.selection)


@gcomponent
class GCamCmd (GenericComponent, metaclass=ABCMeta):
    """  A command usually located on the Lock,  which selects a cam track which will produce a given action
    impacting either the tuple of stitch actions on Patterning and nonPatterning needles or the position of the Needles
    it is totally described by a dic attribute containing several fields"""
    #if a camcmd has a cam controler then the camcmd is associated with an electronic lock controler which exchange messages with SoftKnit21
    camControler = None

    @ classmethod
    def get_HEnum(cls):
        """returns a HEnum of possible choices for the camcmd"""
        module = sys.modules[__name__]
        name = cls.__name__ + 'Enum'
        classdict = {}
        for cv in cls.camValues:
            classdict[cv.value] = (cv.value, cv.label, cv.help)
        enumclass = HEnum.__call__(name, names=classdict, module=cls.__module__, qualname=cls.__qualname__ + '.' + name)
        return enumclass

    @classmethod
    def __subclasshook__(meta, clas):
        """"""
        camvalues = getattr(clas, 'camValues', None)
        if not camvalues:
            raise BadImplementationError(f'error in {clas.__name__} a CamCmd class must have a camValues attribute')
        if not isinstance(camvalues, list):
            camvalues = [camvalues]
            setattr(clas, 'camValues', camvalues)
        firstclass = camvalues[0].__class__
        firstclass.check_allValues(camvalues, clas)
        key = getattr(clas, 'bedCodingKey', None)
        if not key:
            raise BadImplementationError(f'error in {cmdclas.__name__} a CamCmd class must have a bedCodingKey attribute')
        super().__subclasshook__(clas)
        enumclass = clas.get_HEnum()
        setattr(clas, 'enum_class', enumclass)
        return True

    @ classmethod
    def is_bed_compatible(cls, itsNeedlesBedClass):
        bedCoding = getattr(cls, 'bedCodingKey')
        return bedCoding in itsNeedlesBedClass.bedCodingKeys

    @ classmethod
    def as_stitch_dict_per_direction(cls):
        """returns a dict of type : {direction: possible camValues}"""
        if cls.is_stitchCam():
            directions = set(v.direction for v in cls.camValues)
            dic = {}
            for d in directions:
                dic[d] = dict(v.stitch_tuple() for v in cls.camValues if v.direction == d)
            return dic
        else:
            return {}

    @classmethod
    def get_patterningNonPatterning(cls, value, direction):
        """ returns the tuple of patterning, NonPatterning in cls for a given bed and a given direction"""
        possibles = [v.stitch_tuple() for v in cls.camValues if cls.is_stitchCam() and v.value == value and v.direction == direction]
        if len(possibles) == 0 or len(possibles) > 1:
            raise ValueError(_(f'{cls.__name__} has an ambiguous list of camValues as there are several possible values for {bed} in {direction}'))
        return possibles[0][1]

    @ classmethod
    def is_stitchCam(cls):
        """return True is cls is a StitchCam"""
        if isinstance(cls.camValues, list):
            return isinstance(cls.camValues[0], StitchCamValue)
        else:
            return isinstance(cls.camValues, StitchCamValue)

    @ classmethod
    def is_selectionCam(cls):
        """return True is cls is a selectionCam"""
        if isinstance(cls.camValues, list):
            return isinstance(cls.camValues[0], SelectionCamValue)
        else:
            return isinstance(cls.camValues, SelectionCamValue)

    @ abstractmethod
    def get_setting(self, name):
        """ read the current setting of CamCmd
        @param name : name of the lock part
        @type: string = 'front' or 'back'
        """
        raise NotImplementedError

    @ abstractmethod
    def set_setting(self, value):
        """ changes the lock setting, returns nothing
        @param value : new setting for the lock
        @type : type of attribute
        """
        raise NotImplementedError

    @classmethod
    def valueslist(cls):
        """returns the attribute value for all camvalues"""
        return [v.value for v in cls.camValues]


@ gcomponent
class GNeedlesBed(GenericComponent, metaclass=ABCMeta):
    """a set of needles and eventuall a set of pushers acting on needle position """

    class Alignment(HEnum):

        HALF_PITCH_ALIGNMENT = 'HPA', _('half pitch position'), _('Needles of both beds are in offset position; that is the needles of one bed are exactly opposite of the sinker posts of the other needle bed; the racking handle is down '),

        FULL_PITCH_ALIGNMENT = 'FPA', _('full pitch position'), _('Needles of both beds are exactly opposite; the racking handle is up'),

        QUARTER_PITCH_ALIGNMENT = 'QPA', _('quarter pitch position'), _('Needles of the back_bed are slightly to the left of corresponding needles on the front bed; used for transfer'),

        @classmethod
        def extra_help(cls, member_name, brandname):
            """for the moment this function is not called in SoftKnit21"""
            if member_name != 'HALF_PITCH8ALIGNMENT':
                return ''
            if brandname == 'Passap':
                return 'the racking level is down'
            elif brandname == 'Brother' or brandname == 'SilverReed':
                return 'the pitch lever is set to H'
            elif brandname == 'superba':
                return 'the intermediary level is to the right'
            else:
                raise BadImplementationError(f'cannot configure Helpdict for Alignment for brandname : {brandname}')

    class BedName(HEnum):
        BACK_BED = 'BB', _('Back Bed'), ''
        FRONT_BED = 'FB', _('Front Bed'), ''

    @ classmethod
    def __subclasshook__(meta, cls):
        """this is to verify required attributes of NeedleBeds are present and NeedleBeds.BedName and Alignment are correct
        verify also that bedcoding keys do not include an _
        if BedName and Alignment are redefined, check that they are compatible with generic"""
        if not hasattr(cls, 'bedCodingKeys'):
            raise BadImplementationError(f'NeedlesBed class requires an attribute bedCodingKeys describing how to encode Needle Positions')
        keyslist = getattr(cls, 'bedCodingKeys')
        try:
            assert isinstance(keyslist, list), f'NeedlesBed bedCodingKeys attribute must be a list'
            assert all(isinstance(k, str) and k[0] != '_' for k in keyslist)
        except (ValueError, AssertionError) as e:
            raise BadImplementationError(f'NeedlesBed bedCodingList attribute must be string and not start with _') from e
        #if attributes are redefined check they are compatible with generic enums
        bedName = getattr(cls, 'BedName', None)
        if bedName:
            meta.BedName.check_compatible(bedName)
        alignment = getattr(cls, 'Alignment', None)
        if alignment:
            meta.Alignment.check_compatible(alignment,
                                            optional=meta.Alignment.QUARTER_PITCH_ALIGNMENT)

        super().__subclasshook__(cls)
        return True

    @ classmethod
    def has_stitchCam(cls, camlist):
        """returns True if the NeedleBed has a stitchCam associated with it"""
        return any(cam.is_stitchCam() for cam in camlist if cam.bed == cls.name)

    #@ classmethod
    #def get_cam_classes(cls, camclasses):
        #"""returns the list of camcmds associated with this needlesBed"""
        #return [cam for cam in camclasses if cam.bed == cls.name]

    """ here we consider no SoftKnit21 machine will have more than 200 needles"""
    @ classmethod
    @ abstractmethod
    def number_of_needles(cls):
        return 200

    @ staticmethod
    @ abstractmethod
    def get_needles_numbering():
        """needles numbering schema"""
        raise NotImplementedError

    @ staticmethod
    @ abstractmethod
    def validate_coding(bed, tuples):
        """ returns True is coding of the bed is coherent otherwise raises BadImplementationError
        this fucntion must check that all tuples have the same length"""
        raise NotImplementedError


@ gcomponent
class GStitchSize (GenericComponent, metaclass=ABCMeta):
    """ a device usually located on the lock which allows the user to choose the stitch size"""

    @ classmethod
    def __subclasshook__(meta, cls):
        return super().__subclasshook__(cls)

    @ classmethod
    @ abstractmethod
    def get_positions(cls):
        """ list of possible stitch sizes"""
        raise NotImplementedError


@ gcomponent
class GYarnMast (GenericComponent, metaclass=ABCMeta):
    """a device which guides a thread all the way to the Lock and on which the user may select a tension"""

    class Tension (HEnum):
        """ this enum is to be overwritten for each machine"""
        MIN = ('N', _('minimum tension'), '')
        MAX = ('X', _('maximum tension'), '')
        UNKNOWN = ('U', _('unknown'), '')

    @ classmethod
    def get_tension_positions(cls):
        """ list of thread tension positions on the mast"""
        return list(cls.Tension)

    @ classmethod
    def __subclasshook__(meta, cls):
        return super().__subclasshook__(cls)


@ gcomponent
class GKThread (GenericComponent, metaclass=ABCMeta):
    """an abstract representation of a Knitting Thread used on  the knitting machine
    it may be handled in a YarnMast or outside of a YarnMast(additional thread)"""

    pass


@ gcomponent
class GRackingLevel (GenericComponent, metaclass=ABCMeta):
    """ a mechanical device or a set of mechanical devices which allows the user to translate the front needle bed to the right
    or to the left"""

    class Direction (HEnum):
        LEFT = ('L', _('needle bed moving towards Left'), '')
        RIGHT = ('R', _('needle bed moving towards Right'), '')

    @ classmethod
    @ abstractmethod
    def get_positions(cls):
        """ list of possible racking numbers"""
        raise NotImplementedError

    @ classmethod
    def __subclasshook__(meta, cls):
        return super().__subclasshook__(cls)

    @ classmethod
    @ abstractmethod
    def get_initial(cls, alignment):
        """initial position of racking level depending of bed alignement"""
        pass


#create a list of generic components of this module which are generic components of a KnittingMachine
import sys
module = sys.modules[__name__]
generics = {name: c for name, c in inspect.getmembers(module, inspect.isclass) if hasattr(c, 'gcomp')}


def get_generic(genericname):
    """return the generic class for class"""
    return generics[genericname]


# to be done make this the GenericMachineMetaclass
#and have a separate class GenericMachine
#hopefully this will be compatible with classproperty from class utilities
class GenericMachine (ABCMeta):
    """ Abstract Class to define a knitting machine
    each component of the knitting machine may be overwritten by specific module
    """
    numberOfYarnMast = 1
    #this is a list of attributes which are added to the machine
    constant_attributes = {'numberOfYarnMast': int, 'brandname': str}

    bed_components = [
        'GStitchSize',
        'GNeedlesBed',
        'GCamCmd'
    ]
    other_components = ['GRackingLevel',
                        'GLock',
                        ]
    multiple_components = ['GYarnMast']

    optional_components = ['GKThread']
    DEFAULT_NUMB_YARNMASTS = 2
    MAX_CAMCMDS_PER_BED = 4
    NAME_MAX_LENGTH = 20

    #@ classmethod
    #def name(cls):
        #return cls.__name__

    @ classmethod
    def check_bed_components(cls, name, **kwargs):
        """check that beds, ssizes and camcmds are coherent with each other
        return a dict containing 3 keys : beds, camcmds, ssizes
        value for each key is a dic containing : {bedname : list of componentclasses}
        kwargs contains the components and attributes for the machine"""
        camcls = kwargs.get('CamCmd', None)
        beds = kwargs.get('NeedlesBed', None)
        ssizes = kwargs.get('StitchSize', None)
        mainbed = kwargs.get('mainBed', None)
        if not mainbed:
            mainbed = GNeedlesBed.BedName.BACK_BED
        elif mainbed not in list(GNeedlesBed.BedName):
            raise BadImplementationError(f'in {cls} error on MainBed : {mainbed}')
        if not camcls or not beds or not ssizes:
            raise BadImplementationError(f'{cls} must have at least one of each component in {GenericMachine["bed_components"]}')
        if not isinstance(beds, dict):
            if not isinstance(beds, list):
                beds = {GNeedlesBed.BedName.BACK_BED: beds}
            elif beds[0] == beds[1] and issubclass(beds[0], GNeedlesBed):
                beds = {GNeedlesBed.BedName.BACK_BED: beds[0],
                        GNeedlesBed.BedName.FRONT_BED: beds[1]}
            else:
                raise BadImplementationError(f'in {cls} cannot identify NeedlesBed {beds}')
        doublebed = len(beds.keys()) > 1
        if not isinstance(camcls, dict):
            if not isinstance(camcls, list):
                v = camcls
                camcls = {GNeedlesBed.BedName.BACK_BED: [v]}
                if doublebed:
                    camcls[GNeedlesBed.BedName.FRONT_BED] = [v]
            elif len(camcls) == 2 and camcls[0] == camcls[1] and issubclass(camcls[0], GCamCmd):
                camcls = {GNeedlesBed.BedName.BACK_BED: [camcls[0]],
                          GNeedlesBed.BedName.FRONT_BED: [camcls[1]]}
            elif doublebed:
                camcls = {GNeedlesBed.BedName.BACK_BED: camcls,
                          GNeedlesBed.BedName.FRONT_BED: camcls}
        else:
            for key in camcls.keys():
                value = camcls[key]
                if not isinstance(value, list):
                    camcls[key] = [value]

        if not isinstance(ssizes, dict):
            if not isinstance(ssizes, list):
                value = ssizes
                ssizes = {GNeedlesBed.BedName.BACK_BED: value}
                v = ssizes
                if doublebed:
                    ssizes[GNeedlesBed.BedName.FRONT_BED] = value
            elif ssizes[0] == ssizes[1] and issubclass(ssizes[0], GStitchSize):
                ssizes = {GNeedlesBed.BedName.BACK_BED: ssizes[0],
                          GNeedlesBed.BedName.FRONT_BED: ssizes[1]}
            else:
                raise BadImplementationError(f'in {cls} cannot assign StitchSize to beds {ssizes}')
        #here all dict are setup; now check their content is OK
        #single bed has only one entry in dicts
        bednames = list(GNeedlesBed.BedName)
        for name in bednames:
            l = list(beds.keys())
            if (name not in l) and (len(l) == 1) and (l[0] in bednames):
                continue
            try:
                #check classes are instances of GNeedlesBed
                value = ('NeedlesBed', beds)
                assert GNeedlesBed in beds[name].__mro__, f' in {cls} {beds[name].__name__} must be a subclass of GNeedlesBed'
                value = ('StitchSize', ssizes)
                assert GStitchSize in ssizes[name].__mro__, f' in {cls} {ssizes[name].__name__} must be a subclass of GStitchSizes'
                value = ('CamCmd', camcls)
                for cam in camcls[name]:
                    try:
                        assert GCamCmd in cam.__mro__, f' in {cls} {cam} must be a subclass of GCamCmd'
                        assert cam.is_bed_compatible(beds[name]), f'in {cls} {cam} has a bedCodingKey which is not compatible with its {name}'
                    except AssertionError as e:
                        raise BadImplementationError(e.args) from e
                assert any(cam.is_stitchCam() for cam in camcls[name]), f'{cls} must have a StitchCamCmd for bed {name}'
                assert len(camcls[name]) <= GenericMachine.MAX_CAMCMDS_PER_BED, f'in {cls} SoftKnit21 does not support more than {GenericMachine.MAX_CAMCMDS_PER_BED}'
            except AssertionError as e:
                raise BadImplementationError(e.args) from e
            except KeyError as e:
                raise BadImplementationError(f'in {cls} cannot assign {value[0]} value is {value[1]}')

        return {'NeedlesBed': beds, 'CamCmd': camcls, 'StitchSize': ssizes, 'mainBed': mainbed}

    @ classmethod
    def create_optional_component(cls, gname, **kwargs):
        """returns a dic of type name : class of the component """
        component = kwargs.get(gname[1:], None)
        if component:
            return{gname[1:]: component}
        return {gname[1:]: get_generic(gname)}

    @ classmethod
    def duplicate_component(cls, gname, **kwargs):
        """return a dic of type 'name' : a list of the class of the component"""
        number = kwargs.get('numberOf' + gname[1:], None)
        if not number:
            number = getattr(GenericMachine, 'DEFAULT_NUMB_YARNMASTS')
        comp_class = kwargs.get(gname[1:], None)
        if not comp_class:
            raise BadImplementationError(f'in machine {cls} cannot find component {gname[1:]}')
        return {gname[1:]: [comp_class] * number}

    @ staticmethod
    def create_component(cls, gname, **kwargs):
        """return a dic of type 'name' : a list of the class of the component"""
        component = kwargs.get(gname[1:], None)
        if component:
            return{gname[1:]: component}
        else:
            raise BadImplementationError(f'in machine {cls} cannot find component {gname[1:]}')

    def __new__(meta, cls, bases, clsdict, **kwargs):
        """check that all required components are present  and subclass of generic classes
        set number of beds to 1 and mainbed to back if numberofbeds is not defined
        associate CamCmds to beds
        It is assumed that number of CamdCmd instance classes is equivalent to the number of beds because in most cases
        the frontLock and the backLock do not have the same cam commands
        call method to create components"""

        #required components are classes for which members must be defined in kwargs;
        #each instance must be in subclasses of corresponding generic component which means
        #it was checked by subclass_hook of the generic component"
        if len(cls) > GenericMachine.NAME_MAX_LENGTH:
            raise BadImplementationError(f'machine name length must be lower than {MAX_LENGTH} characters')
        names_list = GenericMachine.bed_components + GenericMachine.other_components + GenericMachine.multiple_components + GenericMachine.optional_components
        beds_handled = False
        for name in names_list:
            #get generic name of comp_class without the G
            comp_class = kwargs.get(name[1:], None)
            if not comp_class and name not in GenericMachine.optional_components:
                raise BadImplementationError(f'the knitting machine {cls} is missing component {name[1:]}')
            generic_class = get_generic(name)
            if isinstance(comp_class, dict):
                comp_classes = list(comp_class.values())
            elif not isinstance(comp_class, list):
                comp_classes = [comp_class]
            else:
                comp_classes = comp_class
            for ccls in comp_classes:
                try:
                    if ccls and isinstance(ccls, list):
                        assert all(issubclass(c, generic_class) for c in ccls), f' {ccls.__name__} was not registered as subclass of {generic_class.__name__}'
                    elif ccls:
                        assert issubclass(ccls, generic_class), f' {ccls.__name__} was not registered as subclass of {generic_class.__name__}'
                #if (ccls
                    #and not ccls in generic_class.__subclasses__()
                    #and not any(c in generic_class.__subclasses__() for c in ccls.__bases__ )
                    #and name not in GenericMachine.optional_components
                    #):
                    #if ccls is not subclasses it was not registered thus
                except AssertionError as e:
                    raise BadImplementationError(f' {ccls.__name__} was not registered as subclass of {generic_class.__name__}') from e
            if name in GenericMachine.bed_components and not beds_handled:
                add_dict = GenericMachine.check_bed_components(name, **kwargs)
                beds_handled = True
            elif name in GenericMachine.multiple_components:
                add_dict = GenericMachine.duplicate_component(name, **kwargs)
            elif name in GenericMachine.optional_components:
                add_dict = GenericMachine.create_optional_component(name, **kwargs)
            elif name in GenericMachine.other_components:
                add_dict = GenericMachine.create_component(cls, name, **kwargs)

            clsdict.update(add_dict)
        #add attributes; if not present in kwargs take default value if it exists
        for attname, attType in GenericMachine.constant_attributes.items():
            try:
                att = kwargs[attname]
            except KeyError:
                try:
                    att = getattr(GenericMachine, attname)
                except AttributeError:
                    raise BadImplementationError(f'in {cls} attribute {attname} is missing')
            if not isinstance(att, attType):
                raise BadImplementationError(f'in {cls} attribute {attname},   {att}  is not a {attType}')
            clsdict[attname] = att

        #each component has been added in clsdict so no need to pass kwargs
        theclas = super().__new__(meta, cls, bases, clsdict)
        #GenericMachine class has an attribute softKnit21Machines which is the list of all machines declared in SoftKnit21 software
        #add the new machine to this list
        ms = getattr(meta, 'softKnit21Machines', None)
        if not ms:
            ms = [theclas]
        elif not theclas in ms:
            ms.append(theclas)
        setattr(meta, 'softKnit21Machines', ms)

        return theclas

    @classmethod
    def number_of_needles(cls):
        """returns the number of needles of the NeedlesBed"""
        if cls.__name__ == 'GenericMachine':
            return GNeedlesBed.number_of_needles()
        return cls.NeedlesBed[0].number_of_needles()

    def __call__(self):
        """create an instance of a knitting machine
        call __init__ for each instance of component
        this code is not yet tested
        will be used in SoftKnit21 to create instance of a machine model
        """

        for bedname, bedclass in self.beds.items():
            b = bedclass.__init__(bedclass)
            for compname in self.bed_components:
                instance = self.create_compInstance(compname, bed=bedname)
                setattr(bedclass, compname[1:], instance)

        for compname, natt in self.multiple_components:
            number = getattr(self, natt)
            instances = []
            for i in range(0, number):
                instance = self.create_compInstance(compname)
                instances.append(instance)
            setattr(self, compname[1:], instances)
        for compname in self.single_components:
            instance = self.create_compInstance(compname)
            setattr(self, compname[1:], instance)

    #@classutilities.classproperty
    #def singlebed(cls):
        #pass

    #@classutilities.classproperty
    #def doublefeeder(cls):
        #pass

    #@classutilities.classproperty
    #def doublefeeder(cls):
        #pass

    #@classutilities.classproperty
    #def numberOfYarnmasts(cls):
        #pass


if __name__ == '__main__':

    import unittest
    unittest.main()
