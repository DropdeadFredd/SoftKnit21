    #!/usr/bin/env python
    # coding:utf-8
"""
      This file is part of SoftKnit21 framework

    License
    =======

     Copyright (C) <2019>  <Odile Troulet-Lamberttex@orange.fr>

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as
        published by the Free Software Foundation, either version 3 of the
        License, or  any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU Affero General Public License for more details.

        You should have received a copy of the GNU Affero General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.



    Module purpose
    ==============

    Test Generic Machine classes and functions

    Implements
    ==========



    Documentation
    =============


    Usage
    =====


    @author: Odile Troulet-Lambert
    @copyright: Odile Troulet-Lambert
    @license: AGPL
    """
import sys


import unittest
from unittest.mock import patch

from SoftKnit21.commons.SoftKnit21Errors import *
import SoftKnit21.knitting_machines.generic_machine as gm
#from SoftKnit21.generic_knitting_model.grid_pattern import GridPattern
from SoftKnit21.commons.utils import *
from SoftKnit21.knitting_machines.tests.FakeMachine_class_for_tests import FakeMachine_setUpClass


class ModuleStitchSize (gm.GStitchSize):
    @ classmethod
    def get_positions():
        pass


class ModuleYarnMast (gm.GYarnMast):
    @ classmethod
    def get_tension_positions():
        pass


class ModuleRackingLevel(gm.GRackingLevel):
    @ classmethod
    def get_positions():
        pass


class ModuleLock(gm.GLock):
    pass


class ModuleNeedlesBed(gm.GNeedlesBed):
    bedCodingKeys = ['P', 'N', 'PUX']

    @ classmethod
    def number_of_needles(cls):
        return 200

    @ staticmethod
    def get_needles_numbering():
        pass

    @ staticmethod
    def validate_coding(bed, tuples):
        pass


class AbstractMethodsNeedlesBed (gm.GNeedlesBed):
    """this class is never registered in the tests"""
    bedCodingKeys = ['P', 'N', 'PUX']


class Wrong:
    """this class is not an instance of GNeedlesBed"""
    bedCodingKeys = ['P', 'N', 'PUX']

    @ classmethod
    def number_of_needles(cls):
        return 200

    @ staticmethod
    def get_needles_numbering():
        pass

    @ staticmethod
    def validate_coding(bed, tuples):
        pass


setattr(Wrong, '__abstractmethods__', [])


class Test_CamValue_classes (FakeMachine_setUpClass, unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()

    def test_defaultvalue_and_no_name(self):
        self.assertEqual(self.StCamValues1[0].name, 'V1')
        self.assertEqual(self.StCamValues1[0].label, 'labelV1')

    def test_error_in_type(self):
        with self.assertRaisesRegex(BadImplementationError, 'length of value'):
            value = gm.CamValue(name='V1', value='TTTTT', direction=gm.GLock.Direction.LEFT_DIRECTION)

    def test_correct_SelectionCamValue(self):
        value = self.SelCamValue1
        self.assertIsInstance(value, gm.SelectionCamValue)


class Test_Generic_Component(unittest.TestCase):

    def test_abstract_method_not_present(self):
        with self.assertRaisesRegex(BadImplementationError, 'abstractmethods') as e:
            class NeedlesBed(gm.GNeedlesBed):
                bedCodingKeys = ['P', 'N', 'PUX']
            gm.GNeedlesBed.register(NeedlesBed)
            cn = NeedlesBed()


class Test_Needled_Beds_subclassHook_and_functions(FakeMachine_setUpClass, unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()

    def test_wrong_bedcodingName(self):
        with self.assertRaisesRegex(BadImplementationError, 'bedCodingKeys') as e:
            class WrongNeedleBed(gm.GNeedlesBed):
                notconding = ['P', 'N', 'PUX']
            gm.GNeedlesBed.register(WrongNeedleBed)

    def test_no_bedCodingKeys(self):
        with self.assertRaisesRegex(BadImplementationError, 'bedCodingKeys') as e:
            class NeedlesBed(gm.GNeedlesBed):
                pass
            gm.GNeedlesBed.register(NeedlesBed)

    def test_correct_class(self):

        gm.GNeedlesBed.register(ModuleNeedlesBed)

        self.assertTrue('P' in ModuleNeedlesBed.bedCodingKeys, f'{ModuleNeedlesBed.bedCodingKeys}')
        self.assertTrue('N' in ModuleNeedlesBed.bedCodingKeys, f'{ModuleNeedlesBed.bedCodingKeys}')
        self.assertTrue('PUX' in ModuleNeedlesBed.bedCodingKeys, f'{ModuleNeedlesBed.bedCodingKeys}')

    def test_key_error(self):
        MyNeedlesBed = type('MyNeedlesBed', ModuleNeedlesBed.__bases__, dict(ModuleNeedlesBed.__dict__))
        setattr(MyNeedlesBed, 'bedCodingKeys', ['P', '_N', 'PUX'])
        with self.assertRaisesRegex(BadImplementationError, 'not start with _') as e:
            gm.GNeedlesBed.register(MyNeedlesBed)

    def test_wrong_BedName(self):
        MyNeedlesBed = type('MyNeedlesBed', ModuleNeedlesBed.__bases__, dict(ModuleNeedlesBed.__dict__))

        class ThisBedName(HEnum):
            FRONT_BED = 'FB', 'Front Bed', ''
            BACK_BED = 'OB', 'Back Bed', ''
        setattr(MyNeedlesBed, 'BedName', ThisBedName)
        with self.assertRaisesRegex(BadImplementationError, '') as e:
            gm.GNeedlesBed.register(MyNeedlesBed)

    def test_has_stitchCam(self):
        MyNeedlesBed = type('MyNeedlesBed', ModuleNeedlesBed.__bases__, dict(ModuleNeedlesBed.__dict__))
        setattr(MyNeedlesBed, 'name', gm.GNeedlesBed.BedName.FRONT_BED)
        MyFirstCam = self.SelCamCmd
        MySecondCam = self.StCamCmd
        setattr(MyFirstCam, 'bed', MyNeedlesBed.name)
        setattr(MySecondCam, 'bed', MyNeedlesBed.name)
        camlist = [MyFirstCam, MySecondCam]

        self.assertTrue(MyNeedlesBed.has_stitchCam(camlist))
        self.assertFalse(MyNeedlesBed.has_stitchCam([MyFirstCam]))


class Test_Lock_subclassHook(unittest.TestCase):

    def test_correct(self):
        gm.GLock.register(ModuleLock)

    def test_incorrect_with_redefinition(self):
        MyLock = type('MyLock', ModuleLock.__bases__, dict(ModuleLock.__dict__))

        class MyPosition (HEnum):
            LEFT = ('LP', 'Left side', 'new help')
            RIGHT = ('RP', 'right side', '')
        setattr(MyLock, 'Position', MyPosition)
        with self.assertRaisesRegex(BadImplementationError, 'redefinition of Position '):
            gm.GLock.register(MyLock)


class TestCamCmd (FakeMachine_setUpClass, unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()

    def test_correct_class(self):

        gm.GCamCmd.register(self.StCamCmd)
        self.assertEqual(self.StCamCmd.enum_class.__module__, FakeMachine_setUpClass.__module__)
        self.assertEqual(self.StCamCmd.enum_class.__qualname__, self.StCamCmd.__qualname__ + '.' + 'StCamCmdEnum')
        gm.GCamCmd.register(self.SelCamCmd)
        self.assertEqual(self.SelCamCmd.enum_class.__module__, FakeMachine_setUpClass.__module__)
        self.assertEqual(self.SelCamCmd.enum_class.__qualname__, self.SelCamCmd.__qualname__ + '.' + 'SelCamCmdEnum')

    def test_subclasshook_different_camValues(self):
        MyCamCmd = type('MyCamCmd', self.StCamCmd.__bases__, dict(self.StCamCmd.__dict__))
        camValues = [gm.StitchCamValue(value='V1', direction=gm.GLock.Direction.LEFT_DIRECTION,
                                       patterning=gm.StitchActions.KNIT,
                                       nonPatterning=gm.StitchActions.TUCK,
                                       ),
                     gm.SelectionCamValue(value='V2', direction=gm.GLock.Direction.RIGHT_DIRECTION,
                                          selection=self.SelFuncEnum.IDEM,
                                          )
                     ]
        setattr(MyCamCmd, 'camValues', camValues)
        with self.assertRaisesRegex(BadImplementationError, 'instances of StitchCamValue') as e:
            gm.GCamCmd.register(MyCamCmd)

    def test_is_bed_compatible(self):
        MyCamCmd = type('MyCamCmd', self.StCamCmd.__bases__, dict(self.StCamCmd.__dict__))
        MyCamCmd.name = gm.GNeedlesBed.BedName.FRONT_BED
        MyNeedlesBed = type('MyNeedleBed', ModuleNeedlesBed.__bases__, dict(ModuleNeedlesBed.__dict__))
        MyNeedlesBed.name = gm.GNeedlesBed.BedName.FRONT_BED
        MyCamCmd.bed = gm.GNeedlesBed.BedName.FRONT_BED
        #need to register camCmd with unique value to make it a list
        gm.GCamCmd.register(MyCamCmd)
        setattr(MyCamCmd, 'bedCodingKey', 'P')

        res = self.SelCamCmd.is_bed_compatible(MyNeedlesBed)

        self.assertTrue(res)

    def test_get_HEnum(self):
        res = self.StCamCmd.get_HEnum()

        self.assertTrue(issubclass(res, HEnum))
        for index, e in enumerate(list(res)):
            self.assertEqual(e.label, self.StCamCmd.camValues[index].label)
            self.assertEqual(e.help, self.StCamCmd.camValues[index].help)
            self.assertEqual(e.value, self.StCamCmd.camValues[index].value)

    def test_is_stitchCam(self):
        gm.GCamCmd.register(self.SelCamCmd)

        self.assertTrue(self.StCamCmd.is_stitchCam())
        self.assertFalse(self.SelCamCmd.is_stitchCam())

    def test_as_stitch_dict_per_direction(self):
        from copy import deepcopy
        othervalue = gm.StitchCamValue(value='V5', direction=gm.GLock.Direction.LEFT_DIRECTION,
                                       patterning=gm.StitchActions.SLIP,
                                       nonPatterning=gm.StitchActions.NOTHING,
                                       )
        othervalue1 = gm.StitchCamValue(value='V6', direction=gm.GLock.Direction.RIGHT_DIRECTION,
                                        patterning=gm.StitchActions.KNIT,
                                        nonPatterning=gm.StitchActions.SLIP,
                                        )
        MyCamCmd = self.StCamCmd

        MyCamCmd.camValues = self.StCamCmd.camValues + [othervalue] + [othervalue1]
        shouldbe = {gm.GLock.Direction.LEFT_DIRECTION: {'V5': (gm.StitchActions.SLIP, gm.StitchActions.NOTHING),
                                                        'v1': (gm.StitchActions.KNIT, gm.StitchActions.TUCK)},

                    gm.GLock.Direction.RIGHT_DIRECTION: {'V6': (gm.StitchActions.KNIT, gm.StitchActions.SLIP),
                                                         'v2': (gm.StitchActions.KNIT, gm.StitchActions.SLIP)}
                    }

        res = self.StCamCmd.as_stitch_dict_per_direction()

        self.assertEqual(res[gm.GLock.Direction.LEFT_DIRECTION]['V5'], shouldbe[gm.GLock.Direction.LEFT_DIRECTION]['V5'])
        self.assertEqual(res[gm.GLock.Direction.LEFT_DIRECTION]['v1'], shouldbe[gm.GLock.Direction.LEFT_DIRECTION]['v1'])
        self.assertEqual(res[gm.GLock.Direction.RIGHT_DIRECTION]['V6'], shouldbe[gm.GLock.Direction.RIGHT_DIRECTION]['V6'])
        self.assertEqual(res[gm.GLock.Direction.RIGHT_DIRECTION]['v2'], shouldbe[gm.GLock.Direction.RIGHT_DIRECTION]['v2'])

    def test_get_patterningNonPatterning(self):

        direction = gm.GLock.Direction.LEFT_DIRECTION
        bed = gm.GNeedlesBed.BedName.FRONT_BED

        res_patterning, res_nonPatterning = self.FakeMachine.CamCmd[bed][0].get_patterningNonPatterning('v1', direction)

        self.assertEqual(res_patterning, gm.StitchActions.KNIT)
        self.assertEqual(res_nonPatterning, gm.StitchActions.TUCK)


class Test_class_Generic_Machine_new (FakeMachine_setUpClass, unittest.TestCase):
    """this test covers only new of generic machine, does not test __init__"""

    @ classmethod
    def setUp(cls):
        super().setUpClass()
        with patch.object(gm.GenericComponent, '__subclasshook__', return_value=True):
            gm.GRackingLevel.register(ModuleRackingLevel)
            gm.GLock.register(ModuleLock)
            gm.GNeedlesBed.register(ModuleNeedlesBed)
            gm.GCamCmd.register(cls.SelCamCmd)
            gm.GStitchSize.register(ModuleStitchSize)
        cls.front = gm.GNeedlesBed.BedName.FRONT_BED
        cls.back = gm.GNeedlesBed.BedName.BACK_BED
        #cls.Single = Single
        #cls.Double = Double

    def test_singlebed_correct(self):
        class Single(metaclass=gm.GenericMachine,
                     NeedlesBed=ModuleNeedlesBed,
                     CamCmd=self.StCamCmd,
                     StitchSize=ModuleStitchSize,
                     RackingLevel=ModuleRackingLevel,
                     YarnMast=ModuleYarnMast,
                     Lock=ModuleLock,
                     brandname='myBrand'):
            pass

        self.assertEqual(Single.__dict__['mainBed'], gm.GNeedlesBed.BedName.BACK_BED)
        self.assertEqual(Single.__dict__['NeedlesBed'], {gm.GNeedlesBed.BedName.BACK_BED: ModuleNeedlesBed})
        self.assertEqual(Single.__dict__['CamCmd'], {gm.GNeedlesBed.BedName.BACK_BED: [self.StCamCmd]})
        self.assertEqual(Single.__dict__['StitchSize'], {gm.GNeedlesBed.BedName.BACK_BED: ModuleStitchSize})
        self.assertEqual(Single.__dict__['YarnMast'], [ModuleYarnMast, ModuleYarnMast])
        self.assertEqual(Single.__dict__['RackingLevel'], ModuleRackingLevel)
        self.assertEqual(Single.__dict__['Lock'], ModuleLock)
        self.assertEqual(Single.__dict__['numberOfYarnMast'], 1)

    def test_Double_correct(self):
        class Double (metaclass=gm.GenericMachine,
                      mainBed=gm.GNeedlesBed.BedName.FRONT_BED,
                      NeedlesBed={self.front: ModuleNeedlesBed, self.back: ModuleNeedlesBed},
                      CamCmd={self.front: self.StCamCmd, self.back: [self.StCamCmd, self.SelCamCmd]},
                      StitchSize={self.front: ModuleStitchSize, self.back: ModuleStitchSize},
                      RackingLevel=ModuleRackingLevel,
                      YarnMast=ModuleYarnMast,
                      Lock=ModuleLock,
                      numberOfYarnMast=2,
                      brandname='mybrand'
                      ):
            pass

        self.assertEqual(Double.__dict__['mainBed'], gm.GNeedlesBed.BedName.FRONT_BED)
        self.assertEqual(Double.__dict__['NeedlesBed'], {gm.GNeedlesBed.BedName.BACK_BED.value: ModuleNeedlesBed,
                                                         gm.GNeedlesBed.BedName.FRONT_BED.value: ModuleNeedlesBed})
        self.assertEqual(Double.__dict__['CamCmd'], {gm.GNeedlesBed.BedName.BACK_BED.value: [self.StCamCmd, self.SelCamCmd],
                                                     gm.GNeedlesBed.BedName.FRONT_BED.value: [self.StCamCmd]})
        self.assertEqual(Double.__dict__['StitchSize'], {gm.GNeedlesBed.BedName.BACK_BED.value: ModuleStitchSize,
                                                         gm.GNeedlesBed.BedName.FRONT_BED.value: ModuleStitchSize})
        self.assertEqual(Double.__dict__['YarnMast'], [ModuleYarnMast, ModuleYarnMast])
        self.assertEqual(Double.__dict__['RackingLevel'], ModuleRackingLevel)
        self.assertEqual(Double.__dict__['Lock'], ModuleLock)
        self.assertEqual(Double.__dict__['numberOfYarnMast'], 2)

    def test_abstractmethods(self):
        with self.assertRaisesRegex(BadImplementationError, 'abstractmethods'):
            class SingleInError(metaclass=gm.GenericMachine,
                                NeedlesBed=AbstractMethodsNeedlesBed,
                                CamCmd=self.StCamCmd,
                                StitchSize=ModuleStitchSize,
                                RackingLevel=ModuleRackingLevel,
                                YarnMast=ModuleYarnMast,
                                Lock=ModuleLock):
                pass


class Test_check_beds_components(FakeMachine_setUpClass, unittest.TestCase):

    @ classmethod
    def setUpClass(cls):
        super().setUpClass()

        class NotBedCompatible (cls.SelCamCmd):
            bedCodingKey = 'A'

        setattr(cls, 'NotBedCompatible', NotBedCompatible)

    def setUp(self):

        self.double = {'mainBed': gm.GNeedlesBed.BedName.FRONT_BED,
                       'NeedlesBed': {gm.GNeedlesBed.BedName.BACK_BED: ModuleNeedlesBed,
                                      gm.GNeedlesBed.BedName.FRONT_BED: ModuleNeedlesBed},
                       'CamCmd': {gm.GNeedlesBed.BedName.BACK_BED: [self.StCamCmd, self.SelCamCmd],
                                  gm.GNeedlesBed.BedName.FRONT_BED: [self.StCamCmd]},
                       'StitchSize': {gm.GNeedlesBed.BedName.BACK_BED: ModuleStitchSize,
                                      gm.GNeedlesBed.BedName.FRONT_BED: ModuleStitchSize},
                       'RackingLevel': ModuleRackingLevel,
                       'YarnMast': [ModuleYarnMast, ModuleYarnMast],
                       'Lock': ModuleLock,
                       }
        self.single = {'mainBed': gm.GNeedlesBed.BedName.BACK_BED,
                       'NeedlesBed': {gm.GNeedlesBed.BedName.BACK_BED: ModuleNeedlesBed},
                       'CamCmd': {gm.GNeedlesBed.BedName.BACK_BED: [self.StCamCmd]},
                       'StitchSize': {gm.GNeedlesBed.BedName.BACK_BED: ModuleStitchSize},
                       'YarnMast': [ModuleYarnMast, ModuleYarnMast],
                       'RackingLevel': ModuleRackingLevel,
                       'Lock': ModuleLock
                       }
        self.front = gm.GNeedlesBed.BedName.FRONT_BED
        self.back = gm.GNeedlesBed.BedName.BACK_BED

    def test_missing_mainbed(self):
        self.single.pop('mainBed')
        self.double.pop('mainBed')
        res1 = gm.GenericMachine.check_bed_components('Aclass', **self.single)
        res2 = gm.GenericMachine.check_bed_components('Aclass', **self.double)

        self.assertEqual(res1['mainBed'], self.back)
        self.assertEqual(res2['mainBed'], self.back)

    def test_wrong_mainbed(self):

        self.single['mainBed'] = 'toto'
        with self.assertRaisesRegex(BadImplementationError, 'error on MainBed') as e:
            gm.GenericMachine.check_bed_components('Aclass', **self.single)

    def missing_component(self):
        self.double.pop('StitchSizes')
        with self.assertRaisesRegex(BadImplementationError, 'at least one of each component') as e:
            gm.GenericMachine.check_bed_components('Aclass', **self.double)

    def test_keyerror_in_dict_NeedlesBed(self):
        self.double['NeedlesBed'] = {'toto': ModuleNeedlesBed,
                                     gm.GNeedlesBed.BedName.FRONT_BED.value: ModuleNeedlesBed}
        self.single['NeedlesBed'] = {'toto': ModuleNeedlesBed}

        with self.assertRaisesRegex(BadImplementationError, 'cannot assign NeedlesBed') as e:
            gm.GenericMachine.check_bed_components('Aclass', **self.single)

        with self.assertRaisesRegex(BadImplementationError, 'cannot assign NeedlesBed') as e:
            gm.GenericMachine.check_bed_components('Aclass', **self.double)

    def test_keyerror_in_dict_StitchSize(self):
        self.double['StitchSize'] = {'toto': ModuleStitchSize,
                                     gm.GNeedlesBed.BedName.FRONT_BED.value: ModuleStitchSize}
        self.single['StitchSize'] = {'toto': ModuleStitchSize}

        with self.assertRaisesRegex(BadImplementationError, 'cannot assign StitchSize') as e:
            gm.GenericMachine.check_bed_components('Aclass', **self.single)

        with self.assertRaisesRegex(BadImplementationError, 'cannot assign StitchSize') as e:
            gm.GenericMachine.check_bed_components('Aclass', **self.double)

    def test_keyerror_in_dict_CamCmd(self):

        self.double['CamCmd'] = {'toto': self.StCamCmd,
                                 gm.GNeedlesBed.BedName.FRONT_BED.value: self.StCamCmd}
        self.single['CamCmd'] = {'toto': self.StCamCmd}

        with self.assertRaisesRegex(BadImplementationError, 'cannot assign CamCmd') as e:
            gm.GenericMachine.check_bed_components('Aclass', **self.single)

        with self.assertRaisesRegex(BadImplementationError, 'cannot assign CamCmd') as e:
            gm.GenericMachine.check_bed_components('Aclass', **self.double)

    def test_wrong_class(self):

        self.double['NeedlesBed'] = {gm.GNeedlesBed.BedName.BACK_BED: Wrong,
                                     gm.GNeedlesBed.BedName.FRONT_BED.value: ModuleNeedlesBed}
        self.single['NeedlesBed'] = {gm.GNeedlesBed.BedName.BACK_BED: Wrong}

        with self.assertRaisesRegex(BadImplementationError, 'must be a subclass of GNeedlesBed') as e:
            gm.GenericMachine.check_bed_components('Aclass', **self.single)

        with self.assertRaisesRegex(BadImplementationError, 'must be a subclass of GNeedlesBed') as e:
            gm.GenericMachine.check_bed_components('Aclass', **self.double)

    def test_wrong_class2(self):

        self.double['StitchSize'] = {gm.GNeedlesBed.BedName.BACK_BED: Wrong,
                                     gm.GNeedlesBed.BedName.FRONT_BED.value: ModuleStitchSize}
        self.single['StitchSize'] = {gm.GNeedlesBed.BedName.BACK_BED: Wrong}

        with self.assertRaisesRegex(BadImplementationError, 'must be a subclass of GStitchSize') as e:
            gm.GenericMachine.check_bed_components('Aclass', **self.single)

        with self.assertRaisesRegex(BadImplementationError, 'must be a subclass of GStitchSize') as e:
            gm.GenericMachine.check_bed_components('Aclass', **self.double)

    def test_wrong_class3(self):
        self.double['CamCmd'] = {gm.GNeedlesBed.BedName.BACK_BED: Wrong,
                                 gm.GNeedlesBed.BedName.FRONT_BED: self.StCamCmd}
        self.single['CamCmd'] = {gm.GNeedlesBed.BedName.BACK_BED: Wrong}

        with self.assertRaisesRegex(BadImplementationError, 'must be a subclass of GCamCmd') as e:
            gm.GenericMachine.check_bed_components('Aclass', **self.single)

        with self.assertRaisesRegex(BadImplementationError, 'must be a subclass of GCamCmd') as e:
            gm.GenericMachine.check_bed_components('Aclass', **self.double)

    def test_not_bed_compatible(self):
        self.single['CamCmd'] = {gm.GNeedlesBed.BedName.BACK_BED: self.NotBedCompatible}

        with self.assertRaisesRegex(BadImplementationError, 'bedCodingKey which is not compatible') as e:
            gm.GenericMachine.check_bed_components('Aclass', **self.single)

    def test_not_StitchCamCmd(self):
        self.double['CamCmd'] = self.SelCamCmd
        with self.assertRaisesRegex(BadImplementationError, 'must have a StitchCamCmd') as e:
            gm.GenericMachine.check_bed_components('Aclass', **self.double)

    def test_toomany_CamCmd(self):
        self.single['CamCmd'] = {self.back: [self.StCamCmd, self.SelCamCmd, self.SelCamCmd, self.SelCamCmd, self.SelCamCmd]}
        with self.assertRaisesRegex(BadImplementationError, 'does not support more than ') as e:
            gm.GenericMachine.check_bed_components('Aclass', **self.single)


class Test_class_properties (FakeMachine_setUpClass, unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()

    def test_singleBed(self):
        self.assertEqual(self.FakeMachine.singleBed, False)


if __name__ == '__main__':
    import unittest

    unittest.main()




