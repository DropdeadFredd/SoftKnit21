    #!/usr/bin/env python
    # coding:utf-8
"""
      This file is part of SoftKnit21 framework

    License
    =======

     Copyright (C) <2019>  <Odile Troulet-Lamberttex@orange.fr>

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as
        published by the Free Software Foundation, either version 3 of the
        License, or  any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU Affero General Public License for more details.

        You should have received a copy of the GNU Affero General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.



    Module purpose
    ==============

    Test Generic Machine classes and functions

    Implements
    ==========



    Documentation
    =============


    Usage
    =====


    @author: Odile Troulet-Lambert
    @copyright: Odile Troulet-Lambert
    @license: AGPL
    """
import sys
#import pathlib
#kni = pathlib.Path(__file__).parent.parent
#sys.path.append(str(kni))
#sys.path.append(str(kni.parent))
#sys.path.append(str(kni.parent.parent))
#print(sys.path)

#import unittest
#import unittest.mock as mock
#import dataclasses as dt

from SoftKnit21.commons.SoftKnit21Errors import *
import SoftKnit21.knitting_machines.generic_machine as gm
from SoftKnit21.generic_knitting_model.grid_pattern import GridPattern
from SoftKnit21.commons.utils import *

#from ...commons.SoftKnit21Errors import *
#from ..knitting_machines.utils import HEnum, CamEnum
#from ...generic_knitting_model.grid_pattern import GridPattern
#from ..knitting_machines.generic_machine import *


class FakeMachine_setUpClass:

    @classmethod
    def setUpClass(cls):
        cls.SelFuncEnum = GridPattern.get_SelFuncHEnum()
        cls.StCamValues1 = [gm.StitchCamValue(value='v1',
                                              label='labelV1',
                                              help='helpV1',
                                              direction=gm.GLock.Direction.LEFT_DIRECTION,
                                              patterning=gm.StitchActions.KNIT,
                                              nonPatterning=gm.StitchActions.TUCK,
                                              ),
                            gm.StitchCamValue(value='v2',
                                              label='labelV2',
                                              help='helpV2',
                                              direction=gm.GLock.Direction.RIGHT_DIRECTION,
                                              patterning=gm.StitchActions.KNIT,
                                              nonPatterning=gm.StitchActions.SLIP,
                                              )
                            ]

        cls.SelCamValue1 = gm.SelectionCamValue(value='sv1',
                                                label='labelV1',
                                                help='helpV1',
                                                direction=gm.GLock.Direction.RIGHT_DIRECTION,
                                                selection=cls.SelFuncEnum.IDEM,
                                                )

        class StCamCmd (gm.GCamCmd):
            camValues = cls.StCamValues1
            bedCodingKey = 'P'

            def get_setting(self, name):
                pass

            def set_setting(self, name):
                pass

        class SelCamCmd (gm.GCamCmd):
            camValues = [cls.SelCamValue1]
            bedCodingKey = 'P'

            def get_setting(self, name):
                pass

            def set_setting(self, name):
                pass

        setattr(cls, 'StCamCmd', StCamCmd)
        setattr(cls, 'SelCamCmd', SelCamCmd)
        gm.GCamCmd.register(SelCamCmd)
        gm.GCamCmd.register(StCamCmd)

        #this attribute is used in Ludd21 tests
        cls.machine_camcmds = {'CamCmd': {gm.GNeedlesBed.BedName.FRONT_BED: [cls.StCamCmd, cls.SelCamCmd],
                                          gm.GNeedlesBed.BedName.BACK_BED: [cls.StCamCmd]
                                          }
                               }

        class YarnMast (gm.GYarnMast):
            class Tension (HEnum):
                FIRST = ('F', 'firstlabel', 'firsthelp')
                SECOND = ('S', 'secondlabel', 'secondhelp')

        gm.GYarnMast.register(YarnMast)
        setattr(cls, 'YarnMast', YarnMast)

        class NeedlesBed(gm.GNeedlesBed):
            bedCodingKeys = ['P']

            @ classmethod
            def number_of_needles(cls):
                return 200

            @ staticmethod
            def get_needles_numbering():
                """needles numbering schema"""
                raise NotImplementedError

            @ staticmethod
            def validate_coding(bed, tuples):
                """ returns True is coding of the bed is coherent otherwise raises BadImplementationError
                this fucntion must check that all tuples have the same length"""
                raise NotImplementedError
        gm.GYarnMast.register(NeedlesBed)
        setattr(cls, 'NeedlesBed', NeedlesBed)

        class RackingLevel(gm.GRackingLevel):

            @ classmethod
            def get_positions(cls):
                """ list of possible racking numbers"""
                raise NotImplementedError

            @classmethod
            def get_initial(cls):
                raise NotImplementedError

        gm.GYarnMast.register(RackingLevel)
        setattr(cls, 'RackingLevel', RackingLevel)

        class StitchSize(gm.GStitchSize):

            @ classmethod
            def get_positions(cls):
                """ list of possible stitch sizes"""
                raise NotImplementedError

            @ classmethod
            def get_initial(cls):
                """ list of possible stitch sizes"""
                raise NotImplementedError

        gm.GYarnMast.register(StitchSize)
        setattr(cls, 'StitchSize', StitchSize)

        class FakeMachine (metaclass=gm.GenericMachine,
                           numberOfYarnMast=2,
                           brandname='brandname',
                           mainBed=gm.GNeedlesBed.BedName.FRONT_BED,
                           RackingLevel=RackingLevel,
                           Lock=gm.GLock,
                           StitchSize=StitchSize,
                           YarnMast=YarnMast,
                           NeedlesBed={gm.GNeedlesBed.BedName.FRONT_BED: NeedlesBed,
                                       gm.GNeedlesBed.BedName.BACK_BED: NeedlesBed},
                           CamCmd=cls.machine_camcmds['CamCmd']
                           ):
            machine_model = 'FakeMachine'
        setattr(cls, 'FakeMachine', FakeMachine)













