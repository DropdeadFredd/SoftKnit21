
#!/usr/bin/env python
# coding:utf-8

""" this file is part of SoftKnit21 framework
  
License
=======

 Copyright (C) <2019>  <Odile Troulet-Lamberttex@orange.fr>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or  any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.



Module purpose
==============

Grid pattern storage

Implements
==========

 - B{grid_pattern_storage}

Documentation
=============

    contains functions to read  a gridPattern in local storage 
    to decode gridpattern from different coding schemas

Usage
=====


@author: Odile Troulet-Lambert
@copyright: Odile Troulet-Lambert
@license: AGPL
"""


from SoftKnit21.commons.configuration_manager import get_configuration
#from commons.SoftKnit21Errors import GridPatternError
from gettext import gettext as _
from pathlib import Path
from inspect import getmodulename


def decodeGridPatternCut(file):
    """ decodes gridpatterns in cut format with no palette preceding the description of the grid.
    information were taken from http://www.textfiles.com/programming/FORMATS/pix_fmt.txt
    in complement reverse engineering was done from simple files
    returns the grid or an error message

    format is headers of 6 bytes (2 long integers (2 bytes)  for width and Height and a thide dummy long integer
    then line length in a long integer
    then each line is coded as described in the document with a 0 at the end of each line
    simple files reverse engineering lead to ambiguity :
    either the line length i in a long integer and the 0 for the end of line is included in the line
    or the line length is on a short integer and the 0 end of line is outside of the line
    the first hypothsis seemed more coherent and is implemented in the function
    decoding a gridpattern with line length over 256 would allow to definitely leave ambiguïty

    this decoding algorithm does not seem to work for E8000 gridpatterns : test failed on grid 2195 of E8000

    @ param : file path
    @ type : Path
    
    """

    def cut_code(num, index):
        if num == 255:
            return '0'
        if num == 0:
            return '1'
        else:
            raise ValueError(
                _(f'in module {getmodulename(__file__)} error in decoding gridPattern { num} on  line {index}'))

    def genforHeader(file):
        for i in range(0, 3):
            b = file.read(2)
            hinteger = int.from_bytes(b, byteorder='little')
            # width and height cannot be zero
            if hinteger <= 0 and i != 2:
                raise ValueError(
                    _(f' in module {getmodulename(__file__)} error in decoding of file {file} Header'))
            yield hinteger

    def genforLine(file):
        b = file.read(2)
        while b:
            line_length = int.from_bytes(b, byteorder='little')
            line = file.read(line_length)
            b = file.read(2)
            yield line

    def genforLinecontent(line_codes, index):
        i = 0
        while i < len(line_codes):
            count = line_codes[i]
            # end of line is 0
            if i == len(line_codes) - 1:
                if count != 0:
                    raise ValueError(
                        _(f'in module {getmodulename(__file__)} error in decoding file {file} line {index} does not terminate with 0 ')
                    )
                else:
                    break

            if count > 127:
                # most significant bit is 1; it is a repeat factor
                count = count - 128
                code = line_codes[i + 1]
                i += 2
                y = ''.join([cut_code(code, index)] * count)
                yield y

            else:
                # most significant bit is 0 , it is a count of bytes
                codes = line_codes[i + 1: i + count + 1]
                i += count + 1
                y = ''.join([cut_code(code, index) for code in codes])
                yield y

    with open(file, 'rb') as file:
        try:
            width, height, dummy = list(genforHeader(file))
            c = iter(genforLine(file))
            line_list = []
            for index, l in enumerate(c):
                a = iter(genforLinecontent(l, index))
                b = ''.join(list(a))
                line_list.append(b)

        except IOError:
            raise GridPatternError(
                _(f' in module {getmodulename(__file__)} I/O Error in reading gridPattern  file {file}')
            )

    if len(line_list) != height:
        raise ValueError(
            _(f'in module {getmodulename(__file__)} error in decoding {file}:  height is {height} but file does contain such number of lines')
        )

    for index, line in enumerate(line_list):
        if len(line) != width:
            raise ValueError(
                _(f'in module {getmodulename(__file__)} error in decoding {file} width is {width} line {index} has wrong length')
            )

    return width, height, line_list


def getGridPatternPath(**kwargs):
    """ returns the directory to access the gridpattern files

    @keywordparam : number
    @type : presently only number of gridpattern
    
    @keywordparam : origin 
    @ type : string; presently only E6000
    """

    config = get_configuration()
    number = kwargs.get('number', None)
    origin = kwargs.get('origin', 'E6000')

    if number == None:
        raise FileNotFoundError(
            _(' error in getting cutfile : this version of SoftKnit21 allows access to GridPattern only with its number'))
    dirpath = config.pathsConfig.getGridPatternsDir(origin)
    if dirpath == None:
        raise ValueError(
            _(f'error in getting cutfile; this version of SoftKnit21 only allows to access E6000 gridPatterns with their number')
        )
    else:
        filepath = dirpath / (str(number) + '.CUT')

    return number, filepath


def getGridPattern(**kwargs):
    number, file = getGridPatternPath(**kwargs)
    return number, *decodeGridPatternCut(file)


if __name__ == '__main__':
    import unittest

    from common.tools_for_log import logconf

    mlog = logconf('__name__', test=True)

    from pathlib import Path
    current = Path(__file__)
    tests_dir = Path(current.parent, 'tests')
    loader = unittest.TestLoader()
    tests_suite = loader.discover(tests_dir, pattern='tests_' + current.stem + '*.py')
    testRunner = unittest.runner.TextTestRunner()
    testRunner.run(tests_suite)
