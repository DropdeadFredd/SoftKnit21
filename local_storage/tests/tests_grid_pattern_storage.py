# -*- coding: utf-8 -*-
""" This file is part of SoftKnit21 framework

License
=======

 - B{SofKnit21} (U{http://www.softkni21.org}) is Copyright:
  - (C) Odile.Troulet-Lambert@orange.fr

This program is free software; you can redistribute it and/or modify
it under the terms of the affero GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
Affero GNU General Public License for more details.

You should have received a copy of the Affero GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http://www.gnu.org/licenses/gpl.html}

Module purpose
==============

gridPattern Tests

Implements
==========

 - B{test_grid_pattern_stroage}:


Documentation
=============

    

Usage
=====

@author: Odile Troulet-Lambert
@copyright: Odile Troulet-Lambert
@license: AGPL
"""
import unittest
from pathlib import Path

from .tests_super_class import Test_super_class
from local_storage.grid_pattern_storage import decodeGridPatternCut


class TestdecodeGridPatternTest (Test_super_class, unittest.TestCase):
    """ tests for grid_pattern_storage_module"""
    @classmethod
    def setUpClass(cls):
        cls.path = super().get_tests_datadir(cls, __file__)

    def setUp(self):
        self.content1008_0 = b'\x08\x00\x08\x00\x00\x00\x0b\x00\x83\xff\x01\x00\x01\xff\x01\x00\x82\xff'
        self.content1008_1 = b'\x00\x0b\x00\x82\xff\x01\x00\x83\xff\x01\x00\x01\xff\x00\t\x00\x01\xff\x01'
        self.content1008_2 = b'\x00\x85\xff\x01\x00\x00\x05\x00\x01\x00\x87\xff\x00\t\x00\x01\xff\x01\x00'
        self.content1008_3 = b'\x85\xff\x01\x00\x00\x0b\x00\x82\xff\x01\x00\x83\xff\x01\x00\x01\xff\x00\x0b'
        self.content1008_4 = b'\x00\x83\xff\x01\x00\x01\xff\x01\x00\x82\xff\x00\x07\x00\x84\xff\x01\x00\x83\xff\x00'
        self.content1008 = (self.content1008_0 +
                            self.content1008_1 +
                            self.content1008_2 +
                            self.content1008_3 +
                            self.content1008_4)

        self.content1018_0 = b'\x14\x00\x14\x00\x00\x00\x15\x00\x82\xff\x03\x00\xff\x00\x83\xff\x03\x00\xff\x00\x83'
        self.content1018_1 = b'\xff\x03\x00\xff\x00\x83\xff\x00\x16\x00\x04\xff\x00\xff\x00\x83\xff\x05\x00\xff\x00'
        self.content1018_2 = b'\xff\x00\x83\xff\x05\x00\xff\x00\xff\xff\x00\x15\x00\x03\x00\xff\x00\x83\xff\x07\x00\xff'
        self.content1018_3 = b'\x00\xff\x00\xff\x00\x83\xff\x01\x00\x83\xff\x00\x14\x00\x02\xff\x00\x83\xff\x03\x00\xff'
        self.content1018_4 = b'\x00\x83\xff\x03\x00\xff\x00\x85\xff\x01\x00\x00\x16\x00\x01\x00\x83\xff\x05\x00\xff\x00'
        self.content1018_5 = b'\xff\x00\x83\xff\x03\x00\xff\x00\x83\xff\x02\x00\xff\x00\x15\x00\x83\xff\x07\x00\xff\x00'
        self.content1018_6 = b'\xff\x00\xff\x00\x83\xff\x07\x00\xff\x00\xff\x00\xff\x00\x00\x16\x00\x82\xff\x03\x00\xff'
        self.content1018_7 = b'\x00\x83\xff\x03\x00\xff\x00\x83\xff\x06\x00\xff\x00\xff\x00\xff\x00\x14\x00\x04\xff\x00'
        self.content1018_8 = b'\xff\x00\x85\xff\x03\x00\xff\x00\x83\xff\x05\x00\xff\x00\xff\xff\x00\x16\x00\x03\x00\xff'
        self.content1018_9 = b'\x00\x83\xff\x01\x00\x83\xff\x03\x00\xff\x00\x83\xff\x04\x00\xff\x00\xff\x00\x16\x00\x02'
        self.content1018_10 = b'\xff\x00\x83\xff\x03\x00\xff\x00\x83\xff\x03\x00\xff\x00\x83\xff\x03\x00\xff\x00\x00\x14'
        self.content1018_11 = b'\x00\x01\x00\x83\xff\x03\x00\xff\x00\x85\xff\x03\x00\xff\x00\x83\xff\x02\x00\xff\x00\x16\x00'
        self.content1018_12 = b'\x02\xff\x00\x83\xff\x03\x00\xff\x00\x83\xff\x03\x00\xff\x00\x83\xff\x03\x00\xff\x00\x00\x16'
        self.content1018_13 = b'\x00\x03\x00\xff\x00\x83\xff\x07\x00\xff\x00\xff\x00\xff\x00\x83\xff\x04\x00\xff\x00\xff\x00'
        self.content1018_14 = b'\x16\x00\x04\xff\x00\xff\x00\x83\xff\x05\x00\xff\x00\xff\x00\x83\xff\x05\x00\xff\x00\xff\xff'
        self.content1018_15 = b'\x00\x15\x00\x82\xff\x03\x00\xff\x00\x83\xff\x03\x00\xff\x00\x83\xff\x03\x00\xff\x00\x83\xff'
        self.content1018_16 = b'\x00\x15\x00\x83\xff\x03\x00\xff\x00\x83\xff\x01\x00\x83\xff\x03\x00\xff\x00\x83\xff\x01\x00'
        self.content1018_17 = b'\x00\x14\x00\x01\x00\x83\xff\x03\x00\xff\x00\x85\xff\x03\x00\xff\x00\x83\xff\x02\x00\xff\x00'
        self.content1018_18 = b'\x16\x00\x02\xff\x00\x83\xff\x03\x00\xff\x00\x83\xff\x03\x00\xff\x00\x83\xff\x03\x00\xff\x00'
        self.content1018_19 = b'\x00\x16\x00\x03\x00\xff\x00\x83\xff\x07\x00\xff\x00\xff\x00\xff\x00\x83\xff\x04\x00\xff\x00'
        self.content1018_20 = b'\xff\x00\x16\x00\x04\xff\x00\xff\x00\x83\xff\x05\x00\xff\x00\xff\x00\x83\xff\x05\x00\xff\x00'
        self.content1018_21 = b'\xff\xff\x00'
        self.content1018 = (self.content1018_0 +
                            self.content1018_1
                            + self.content1018_2
                            + self.content1018_3
                            + self.content1018_4
                            + self.content1018_5
                            + self.content1018_6
                            + self.content1018_7
                            + self.content1018_8
                            + self.content1018_9
                            + self.content1018_10
                            + self.content1018_11
                            + self.content1018_12
                            + self.content1018_13
                            + self.content1018_14
                            + self.content1018_15
                            + self.content1018_16
                            + self.content1018_17
                            + self.content1018_18
                            + self.content1018_19
                            + self.content1018_20
                            + self.content1018_21)
        # the list of rows starts with the bottom row as seen from the manual
        self.rows_list_1008 = ['00010100', '00100010', '01000001', '10000000',
                               '01000001', '00100010', '00010100', '00001000']

        self.decode_1008 = [8, 8, self.rows_list_1008]

        self.decode_1018 = [20, 20, ['00101000101000101000', '01010001010100010100',
                                     '10100010101010001000', '01000101000101000001',
                                     '10001010100010100010', '00010101010001010101',
                                     '00101000101000101010', '01010000010100010100',
                                     '10100010001010001010', '01000101000101000101',
                                     '10001010000010100010', '01000101000101000101',
                                     '10100010101010001010', '01010001010100010100',
                                     '00101000101000101000', '00010100010001010001',
                                     '10001010000010100010', '01000101000101000101',
                                     '10100010101010001010', '01010001010100010100']]

    def testdecodeGridPatternCut_files(self):
        """ this test decodes from data files in the test directory"""
        result = decodeGridPatternCut(Path(self.path, 'data_1008.CUT'))
        self.assertEqual(list(result), self.decode_1008)
        result = decodeGridPatternCut(Path(self.path, 'data_1018.CUT'))
        self.assertEqual(list(result), self.decode_1018)

    def testdecodeGridPatternCut_files_header_error(self):
        """ error in the header"""
        with self.assertRaises(ValueError):
            decodeGridPatternCut(Path(self.path, 'data_1008_0 _in_header.CUT'))

    def testdecodeGridPatternCut_files_coding_error(self):
        """ error in coding"""
        with self.assertRaises(ValueError):
            decodeGridPatternCut(Path(self.path, 'data_1008_error_in_coding.CUT'))

    def testdecodeGridPatternCut_files_EOL_error(self):
        """ error in EOL"""
        with self.assertRaises(ValueError):
            decodeGridPatternCut(Path(self.path, 'data_1008_error_in_EOL.CUT'))

    def testdecodeGridPatternCut_files_linelength_error(self):
        """ error in line length"""
        with self.assertRaises(ValueError):
            decodeGridPatternCut(Path(self.path, 'data_1008_error_in line_length.CUT'))

    def testdecodeGridPatternCut_files_width_error(self):
        """ error in the header"""
        with self.assertRaises(ValueError):
            decodeGridPatternCut(Path(self.path, 'data_1008_error_in_width.CUT'))

    def testdecodeGridPatternCut_files_linemissing_error(self):
        """ error in the header"""
        with self.assertRaises(ValueError):
            decodeGridPatternCut(Path(self.path, 'data_1008_one_line_missing.CUT'))

    def tearDowm(self):
        pass


if __name__ == '__main__':
    import unittest

    from common.tools_for_log import logconf

    mlog = logconf('__name__', test=True)

    unittest.main()


