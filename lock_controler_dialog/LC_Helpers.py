# -*- coding: utf-8 -*-
""" This file is part of SoftKnit21 framework

License
=======

 - B{SofKnit21} (U{http://www.softkni21.org}) is Copyright:
  - (C) Odile.Troulet-Lambert@orange.fr

This program is free software; you can redistribute it and/or modify
it under the terms of the Affero GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
Affero GNU General Public License for more details.

You should have received a copy of the Affero GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http://www.gnu.org/licenses/gpl.html}

Module purpose
==============

Helpers for LCModel package

Implements
==========



Documentation
=============


Usage
=====


@author: Odile Troulet-Lambert
@copyright: Odile Troulet-Lambert
@license: AGPL
"""

# from gettext import gettext as _
from common.SoftKnit21Errors import LCMessageError
# import serial

import logging

mlog = logging.getLogger(__name__)


class LCProtocolHelpers:
    """ Constants for Lock Controler Protocol Message"""
    LTR = 0
    RTL = 1
    # error codes for which messages contain the message number
    eCodesWithMesNum = [41, 42, 43]
    # list of all error codes
    alleCodes = [10, 11, 12, 13, 20, 31, 32, 33, 41, 42, 43, 50, ]
    # code for events
    eventCodes = ['1*', '1*', '1*', '1*', '20', '3*', '3*', '3*', '4*', '4*', '4*', '50']
    # list of needles position used by the Lock Controler software
    lockPosLC = list(range(-90, 89))
    numberNeedles = 179
    NB_MAX_BUFFERS = 3
    TIMEOUT_MESSAGE = 1
    TIMEOUT_USER = 100
    MAXREPEAT = 3

    @staticmethod
    def get_SKlockpos(lcPos):
        """ returns Lockposition in the softknit21 range

        @param lcPos :position in the LCrange
        @type : int"""

        if (lcPos - 8192) not in LCProtocolHelpers.lockPosLC:

            return lcPos - 8192
        else:
            return LCProtocolHelpers.lockPosLC.index(lcPos - 8192)

    @staticmethod
    def get_LClockpos(sk21pos):
        """ returns Lockposition in the LCrange
        @param sk21pos :position in the numbering scheme of softknit21
        @type : int"""
        if sk21pos in range(0, LCProtocolHelpers.numberNeedles):
            return LCProtocolHelpers.lockPosLC[sk21pos] + 8192
        else:
            raise LCMessageError(f'needle position {sk21pos} is not in the range of needles')

    @staticmethod
    def check_brand(brand):
        """ check  brand of the machine is one that can be handled and log
        returns true or error message"""
        if brand not in (dic.get('brand', None) for dic in LCConfigurations.KNOWN_LCs):
            return f'{brand} is not yet supported by Softknit21'
        else:
            return True

    rMessagesDef = {
        'A': {'class': 'normal', },
        'N': {'class': 'normal', },
        'L': {'class': 'normal', },
        'R': {'class': 'normal', },
        'a': {'class': 'normal', 'values': ['mesNumber']},
        'n': {'class': 'normal', 'values': ['mesNumber']},
        # all values of hardware parameters to be checkes start with a V
        'd': {'class': 'normal', 'values': ['Vcc', 'V15']},
        'e': {'class': 'normal', 'values': ['errorcode', 'mesNumber']},
        'w': {'class': 'normal', 'values': ['brand', 'hard', 'soft', ]},
        'p': {'class': 'test', 'values': ['lockpos']},
        's': {'class': 'test', 'values': ['PrevDir']},
        't': {'class': 'test', 'values': ['buffindex', 'mesNumber', 'bufftype', 'leftmost', 'rightmost', 'LTRBuffer', ]},
        'u': {'class': 'test', 'values': ['buffindex', 'mesNumber', 'bufftype', 'leftmost', 'rightmost', 'RTLBuffer', ]},
        # 'k': {'class': 'test', 'values': ['currentBuff',
        #                                  'previousBuff',
        #                                  'currentExtreme',
        #                                  'nextExtreme',
        #                                  'inKnittingZone',
        #                                  'newleftmost',
        #                                  'expectedMessageNumber'],

    }

    sMessagesDef = {
        'D': {'class': 'normal', },
        'Y': {'class': 'normal', },
        'Z': {'class': 'normal', },
        'W': {'class': 'normal', },
        'M': {'class': 'test', },
        'Q': {'class': 'test', },
        'P': {'class': 'test', },
        'S': {'class': 'test', },
        'U': {'class': 'test', },
        'K': {'class': 'test', },
        'x': {'class': 'normal', 'values': ['bufftype', 'mesNumber']},
        'l': {'class': 'normal', 'values': ['mesNumber', 'leftmost', 'rightmost', 'LTRBuffer']},
        'r': {'class': 'normal', 'values': ['mesNumber', 'leftmost', 'rightmost', 'RTLBuffer']},
        'm': {'class': 'test', 'values': ['testnum', ]}, }

    mesTypesDef = {'mesNumber': {'vtype': 'byte', 'length': None, 'extra': None},
                   'Vcc': {'vtype': '2bytes', 'length': None,
                           'extra': {'send': {'factor': lambda x: int(x * 10)},
                                     'receive': {'factor': lambda x: x / 10}}},
                   'V15': {'vtype': '2bytes', 'length': None,
                           'extra': {'send': {'factor': lambda x: int(x * 10)},
                                     'receive': {'factor': lambda x: x / 10}}},
                   'errorcode': {'vtype': 'byte', 'length': None, 'extra': {'checklist': alleCodes}},
                   'brand': {'vtype': 'strtype', 'length': 12, 'extra': {'check': check_brand}},
                   'hard': {'vtype': 'strtype', 'length': 4, 'extra': None, },
                   'soft': {'vtype': 'strtype', 'length': 3, 'extra': None, },
                   'lockpos': {'vtype': '2bytes', 'length': None,
                               'extra': {'send': {'pos': get_LClockpos},
                                         'receive': {'pos': get_SKlockpos}}},
                   'PrevDir': {'vtype': 'bits', 'length': 6,
                               'extra': {'islist': ['PrevDir', 'PrevCsense', 'PrevCref', 'Dir', 'CSense', 'Cref']}},
                   'leftmost': {'vtype': '2bytes', 'length': None,
                                'extra': {'send': {'pos': get_LClockpos},
                                          'receive': {'pos': get_SKlockpos}}},
                   'rightmost': {'vtype': '2bytes', 'length': None,
                                 'extra': {'send': {'pos': get_LClockpos},
                                           'receive': {'pos': get_SKlockpos}}},
                   'LTRBuffer': {'vtype': 'LTRBuff', 'length': None,
                                 'extra': {'send': {'checkregexp': '^[0-1]+'},
                                           'receive': {'sliceO': 'checklr'}}},
                   'RTLBuffer': {'vtype': 'RTLBuff', 'length': None,
                                 'extra': {'send': {'checkregexp': '^[0-1]+'},
                                           'receive': {'sliceO': 'checklr'}}},
                   # this type only used for received messages
                   'buffindex': {'vtype': 'byte', 'length': None,
                                 'extra': {'checklist': range(0, NB_MAX_BUFFERS)}},
                   # 'inkzone': {'vtype': 'bool', 'length': None, 'extra': None},
                   # 'newleftmost': {'vtype': '2bytes', 'length': None, 'extra': {'lockpos': LConstants.lockpos}},
                   # 'newrightmost': {'vtype': '2bytes', 'length': None, 'extra': {'lockpos': LConstants.lockpos}},
                   # 'current': {'vtype': 'buffer', 'length': None, 'extra': None},
                   # 'previous': {'vtype': 'buffer', 'length': None, 'extra': None},
                   # 'expected': {'vtype': 'buffer', 'length': None, 'extra': None},
                   # 'next': {'vtype': 'buffer', 'length': None, 'extra': None},
                   # 'next1': {'vtype': 'buffer', 'length': None, 'extra': None},
                   # 'dircar': {'vtype': 'strtype', 'length': 1, 'extra': {'checklist': ['l', 'r']}},
                   'bufftype': {'vtype': 'strtype', 'length': 1, 'extra': {'checklist': ['l', 'r']}},
                   'testnum': {'vtype': 'byte', 'length': None, 'extra': {'checklist': [97, 99]}},
                   }


class LCConfigurations:
    """ all information related to Lock Controler Hardware"""

    def checkVoltages(**event):
        """"check min and max values for Owen's LC"""
        checks = {'Vcc': (4.90, 5.5),
                  'V15': (13.5, 15),
                  }
        res = True
        for key, value in event.items():
            if key in checks.keys():

                mini, maxi = checks.get(key)
                res = res and mini <= value <= maxi
        return res

    # list of all LCs types handled by SoftKnit21
    KNOWN_LCs = [{  # this is Owen Mace's hardware parameters
        # brand should be OwenMace and Not PASSAPE6000
        # description should be something different from USBserial which is what the LC
                    # hardware returns presently
        'description': 'Arduino',
        'brand': 'PassapE6000 ',
        'hard': 'LC15',
        'soft': '201',
        # estimated correct voltages for the voltages of the lock controler
        'checkHard': checkVoltages,
        'knitting_machine_model': 'E6000',
        'brand manufacturer mail': 'owen.mace@internode.on.net'
    }
    ]

    def is_LC(description):
        """ returns True if description is in dictionnary key 'description' for a Known LC
        needed to recognize the LC in case scanning of serial ports returns several hardware"""

        known_descriptions = [LCdic.get('description') for LCdic in LCConfigurations.KNOWN_LCs]
        return any(description.find(ex) >= 0 for ex in known_descriptions)

    def is_hardware_OK(brand, **event):
        """ returns true if values are OK

        @values : values to be checked
        @type : dic"""
        LCparam = [param for param in LCConfigurations.KNOWN_LCs
                   if param.get('brand') == brand]
        function = LCparam[0].get('checkHard')
        res = function(**event)
        return res


if __name__ == '__main__':

    import unittest

    from common.tools_for_log import logconf

    mlog = logconf('__name__', test=True)

    from pathlib import Path
    current = Path(__file__)
    tests_dir = Path(current.parent, 'tests')
    loader = unittest.TestLoader()
    tests_suite = loader.discover(tests_dir, pattern='tests_' + current.stem + '*.py')
    testRunner = unittest.runner.TextTestRunner()
    testRunner.run(tests_suite)



















