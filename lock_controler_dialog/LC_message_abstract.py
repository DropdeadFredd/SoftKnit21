#!/usr/bin/env python
# coding:utf-8
"""
  This file is part of SoftKnit21 framework

License
=======

 Copyright (C) <2019>  <Odile Troulet-Lamberttex@orange.fr>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or  any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.



Module purpose
==============

LC Message is an abstract class ; LCMessageReceived and LCMessagetoSend inherit from LCMessage
Implements
==========

 - B{LCMessage}

Documentation
=============

    LCMessages and Payload classes

Usage
=====
exceptions raised only for errors from programming environment
errors which might come from transmission on serial port are not raised but logged at critical level

@author: Odile Troulet-Lambert
@copyright: Odile Troulet-Lambert
@license: AGPL
"""


from common. SoftKnit21Errors import LCMessageError, LCMessageConfError
from common.tools_for_log import StrucLogMessage
from abc import ABC, abstractmethod
import sys
import logging

mlog = logging.getLogger(__name__)


class Payload (list):
    """ Payload list containing only integer representing the bytes of the payload
    subclass of list to prevent from adding incorrect type values
    """

    def __init__(self):
        super().__init__()

    def pvalue(self, oneval):
        """returns the value of one byte of the payload"""
        if isinstance(oneval, bytes):
            return int.from_bytes(oneval, sys.byteorder)
        elif isinstance(oneval, int):
            return oneval
        else:
            raise (LCMessageError(f'{oneval} is not an integer nor a byte'))

    def add(self, value):
        if isinstance(value, list):
            for e in value:
                super().append(self.pvalue(e))
        else:
            super().append(self.pvalue(value))

    def append(self, value):

        raise LCMessageError(f'cannot append to the payload, use addpayload')

    def insert(self, value):
        raise LCMessageError(f'cannot append to the payload, us addpayload')

    def extend(self, value):
        raise LCMessageError(f'cannot append to the payload, us addpayload')

    def __str__(self):
        s = []
        for e in self:
            s.append(str(hex(e)))
        return str(s)


class LCMessage (ABC):
    """ contains a LCMessage

    @att mtype : type of the message
    @type : ascii character

    @att lenpayload : length of the payload of the message
    @type : int (-1 for a single byte message)

    @att payload : payload of the message
    @type : list of integers

    @att checksum : checked checksum
    @type : True or False

    """
    @abstractmethod
    def __init__(self):
        """ constructor"""
        pass

    @property
    def checksum(self):
        if self.mtype.islower():
            return self._checksum
        else:
            return None

    @property
    def payload(self):
        return self._payload

    @payload.setter
    def payload(self, value):
        # used only to set a new list as payload
        # in the form : self.payload = value
        if (isinstance(value, list)
                    and len(value) == self.lenpayload
                    and self.mtype.islower()
                ):
            self._payload = Payload()
            self._payload.add(value)
        # for a single byte only empty list for payload
        elif (self.mtype.isupper() and
              self.lenpayload == -1 and
              isinstance(value, list) and
              len(value) == 0
              ):

            self._payload = Payload()
        else:
            raise LCMessageError(f'payload must be a list')

    def _extra(self, dic, value):
        """ applies a modification to value
        @param dic: describes the modification to apply
        @type : dic

        @param value : the value to be modified"""
        import re
        if not isinstance(dic, dict):
            raise LCMessageError(f'extra is not a dict : {dic}')
        li = dic.get('checklist')
        if li is not None:
            if not value in li:
                raise LCMessageError(f'extra function {dic}, value {value} is not in dic : {li}')
            return value
        pos = dic.get('pos')
        if pos is not None:
            if (callable(pos)):
                value = pos._call__(value)
                return value
            elif isinstance(pos, staticmethod):
                value = pos.__func__(value)
                return value
        factor = dic.get('factor')
        if factor is not None:
            value = factor(value)
            return value
        islist = dic.get('islist')
        if islist is not None:
            if isinstance(value, list):
                return dict(list(zip(islist, value)))
            else:
                # in self.datadict get the list of values in the right order
                orderedvalues = [self.datadict.get(key) for key in islist]
                if len(orderedvalues) != len(islist) or (
                    any(e for e in orderedvalues if (e != 0 and e != 1))
                ):
                    raise LCMessageError(f'error in list of bits values {orderedvalues}')
                return orderedvalues
        valid = dic.get('check')
        if valid is not None:
            if (callable(valid)):
                res = valid.__call__(value)
            elif isinstance(valid, staticmethod):
                res = valid.__func__(value)
            if res != True:
                raise LCMessageConfError(f'wrong lock controler supported {res}')
            else:
                return value
        regexp = dic.get('checkregexp')
        if regexp is not None:
            res = re.search(regexp, value)
            if res is not None:
                s = res.span()
                if not(s[0] == 0 and s[1] == len(value)):
                    raise LCMessageError(f'value is not a O1 string {value}')
            return value
        sliceO = dic.get('sliceO')
        if sliceO is not None:
            value = value[sliceO]
            self.checklr(value)
            return value

    def checklr(self, value=None):
        """ checks the coherence of extreme needles with length of the buffer
        no return raises exception in case of error
        @param value : the value of the buffer
        @type : string
        
        """
        from lock_controler_dialog.LC_Helpers import LCProtocolHelpers

        if self.mtype == 'l' or self.mtype == 'r' or self.mtype == 't' or self.mtype == 'u':
            left = self.datadict.get('leftmost')
            right = self.datadict.get('rightmost')
            if left not in range(0, LCProtocolHelpers.numberNeedles):
                raise LCMessageError(f'{left} is not a correct value for leftmostneedle')
            if right not in range(0, LCProtocolHelpers.numberNeedles):
                raise LCMessageError(f'{right} is not a correct value for rightmostneedle')
            if self.mtype == 'l'or self.mtype == 't':
                buff = value if value is not None else self.datadict.get('LTRBuffer')
            if self.mtype == 'r' or self.mtype == 'u':
                buff = value if value is not None else self.datadict.get('RTLBuffer')
            if len(buff) != right - left + 1:
                raise LCMessageError(f'buffer has a length of {len(buff)} not coherent with extreme needles: {left} {right}')
        else:
            raise LCMessageError(f'checklr called for other message than l, r, t, u')


if __name__ == '__main__':

    import unittest
    import time
    from unittest.mock import patch, Mock
    # no constructor for abstract messages tested throught LC messagereceived

    unittest.main()


