#!/usr/bin/env python
# coding:utf-8
"""
  This file is part of SoftKnit21 framework

License
=======

 Copyright (C) <2019>  <Odile Troulet-Lamberttex@orange.fr>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or  any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.



Module purpose
==============

tools to generate random l and r messages

Implements
==========

 - B{}

Documentation
=============


Usage
=====


@author: Odile Troulet-Lambert
@copyright: Odile Troulet-Lambert
@license: AGPL
"""
import sys
import builtins
from Lock_Controler.LC_Helpers import LCProtocolHelpers


def gen_data_buff(it, modulo):
    """ generator for pattern"
    based on a moulo and shifting the change of pushers of one position each turn"""

    b = 0
    c = 0
    for i in it:
        if b == c:
            yield '1'
        else:
            yield '0'
        if (b + 1) % modulo == 0:
            c = (c + 1) % modulo
        b = (b + 1) % modulo


def buffer_car(leftmost, rightmost, modulo):
    """generates a pattern for the buffer"""
    return gen_data_buff(range(leftmost, rightmost + 1), modulo)


def createmessages(mtype, left, right, mesnumber, modulo):
    """ creates a dict for the constructor of the message
    and a dict for the results to check against"""
    from LC_model.LC_message_tosend import LCMessagetoSend

    buffercar = ''.join(buffer_car(left, right, modulo))
    if mtype == 'l':
        buffkey = 'LTRBuffer'
        buffpayload = LCMessagetoSend._encodeLTRbuff(buffercar)
    else:
        buffkey = 'RTLBuffer'
        buffpayload = LCMessagetoSend._encodeRTLbuff(buffercar)
    mdict = {
        'mtype': mtype,
        'class': 'normal',
        'mesNumber': mesnumber,
        'leftmost': left,
        'rightmost': right,
        buffkey: buffercar,
        'checksum': True
    }

    payload = [mesnumber]
    lefttranslated = LCProtocolHelpers.get_LClockpos(left)
    righttranslated = LCProtocolHelpers.get_LClockpos(right)
    payload.extend([(lefttranslated // 128), (lefttranslated % 128)])
    payload.extend([(righttranslated // 128), (righttranslated % 128)])
    payload.extend(buffpayload)
    payloadbytes = [e.to_bytes(1, sys.byteorder) for e in payload]
    checksum = ord(mdict['mtype']) + len(payload)
    for b in payload:
            checksum = checksum + b
    checksum = checksum % 128
    resultdict = {
        'mtype': mtype,
        'lenpayload': int(len(buffercar) / 7) + (1 * len(buffercar) % 7 != 0) + 5,
        'mesNumber': mesnumber,
        'payload': payloadbytes,
        'checksum': checksum.to_bytes(1, sys.byteorder),
        'checksumdec': checksum,
        'payloadindec': payload,
    }

    return mdict, resultdict


if __name__ == '__main__':


# tests will be ran with the following values
# mtype, leftmost, rightmost, mesnumber, modulo for the buffergenerator
    buffersvalueslist = [
        ('l', 30, 50, 1, 3),
        ('r', 0, 22, 2, 4),
        ('r', 0, 178, 3, 5),
        ('l', 110, 150, 4, 8),
        ('l', 0, 77, 5, 2),
        ('r', 1, 22, 6, 7)
    ]

    messagesdict = {}
    resultsdict = {}

    for mtype, left, right, mesnumber, modulo in buffersvalueslist:
        messagedict, resultdict = createmessages(mtype, left, right, mesnumber, modulo)
        key = 'm' + mtype + str(mesnumber)
        messagesdict.update({key: messagedict})
        resultsdict.update({key: resultdict})

    for key, value in messagesdict.items():

        print(key, f' : {value} ,')

    for key, value in resultsdict.items():
        stringdec = ','.join([f'{e}' for e in value.get('payloadindec')])
        stringhex = ','.join([f'x\{e.hex()}' for e in value.get('payload')])
        checksum = value.get('checksum')
        chechsumdec = value.get('checksumdec')
        del value['payloadindec']
        del value['checksumdec']
        print(f'{key} : {value}')
        print(f'#payload in decimal : {stringdec}, checksum in hexa: x\{checksum.hex()}')
        print(f'#payload in hexadecimal : {stringhex} ')
        print()
        print()



