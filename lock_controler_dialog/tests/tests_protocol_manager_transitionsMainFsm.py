#!/usr/bin/env python
# coding:utf-8
"""
  SoftKnit21 framework

License
=======

 Copyright (C) <2019>  <Odile Troulet-Lamberttex@orange.fr>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or  any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.



Module purpose
==============

Test for protocol manager : tests for the main FSM

Implements
==========

 - B{TestProtocolManager}

Documentation
=============



Usage
=====


@author: Odile Troulet-Lambert
@copyright: Odile Troulet-Lambert
@license: GPL
"""
import unittest

import logging
from lock_controler_dialog.tests.tests_super_class import Test_super_class
from common.tools_for_log import search_in_log


class TestMainFsmTransitions (Test_super_class, unittest.TestCase):
    """ tests for init individual functions
    test objective only go through each transition and check new state
    other tests suites are targeted towards testing the results of the functions
    check no log CRITICAL"""

    def setUp(self):
        """create the protocol manager
        mock LockControler writer and its methods with autospec so as to verify the arguments passed
        """

        super().commonsetUp_for_pm()

    def test_transition_on_start(self):

        event = {'evName': 'on_start'}

        # with patch('common.fsm.mlog', new=self.mockwritelog):
        self.pm.trigger(**event)

        self.assertTrue(self.pm.currentFsm, self.pm.InitFsm)
        self.assertEqual(self.pm.currentFsm.state, 'waitingLC')
        self.assertTrue(search_in_log(self.mocklog.log.call_args_list, '',
                                      logging.CRITICAL))

    def test_transition_on_user_click_knit(self):

        event = {'evName': 'on_user_click_knit'}
        self.pm._state = 'readyKnitTest'
        attrs = {'nextCP.return_value': (85, 105, '1' * 7 + '0' * 7 + '1' * 7, 'r')}
        self.pm.running_knit.configure_mock(**attrs)

        self.pm.trigger(**event)

        self.assertTrue(self.pm.currentFsm, self.pm.KnitFsm)
        self.assertEqual(self.pm.currentFsm.state, 'knit')
        self.assertTrue(search_in_log(self.mocklog.log.call_args_list, '',
                                      logging.CRITICAL))

    def test_transition_on_user_click_test(self):

        event = {'evName': 'on_usr_click_test'}
        self.pm._state = 'readyKnitTest'

        self.pm.trigger(**event)

        self.assertTrue(self.pm.currentFsm, self.pm.TestsFsm)
        self.assertTrue(search_in_log(self.mocklog.log.call_args_list, '',
                                      logging.CRITICAL))

    def tearDown(self):
        pass


if __name__ == '__main__':
    import unittest

    from common.tools_for_log import logconf
    mlog = logconf('__file__', test=True)

    unittest.main()
