#!/usr/bin/env python
# coding:utf-8
"""
  This file is part of SoftKnit21 framework

License
=======

 Copyright (C) <2019>  <Odile Troulet-Lamberttex@orange.fr>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or  any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.



Module purpose
==============

Tests for LockReader Class in isolation from the protocol manager: messages are read and protocol manager is moked,
logs are analysed

Implements
==========
first TestCase uses a file containing messages to send and  checks the results in the log;
reads in the file a random number of bytes
random errors are generated.
second TestCase reads a sample of messages sent by the test program to be loaded into the LC



Documentation
=============


Usage
=====


@author: Odile Troulet-Lambert
@copyright: Odile Troulet-Lambert
@license: AGPL
"""


import unittest
from unittest.mock import patch, Mock, MagicMock, DEFAULT
from pathlib import Path
from lock_controler_dialog.LC_Helpers import LCProtocolHelpers
import json
from common.tools_for_log import StrucLogMessage
import random
from lock_controler_dialog.LC_messages_reader import LCMesReader
from lock_controler_dialog.fsm import FSM
import logging
from lock_controler_dialog.tests.tests_super_class import Test_super_class
from lock_controler_dialog.LC_message_received import LCMessageReceived

LCConnected = False
debug_test = False


class TestLockReaderWithFile (Test_super_class, unittest.TestCase):
    """ test for the LockReader reading function with a file.
    messages are in LOG"""

    def setUp(self):
        """ create the data bytes and the event decodes"""

        super().commonsetUp_for_pm()

        test_datadir = super().get_tests_datadir(__file__)
        file = Path(test_datadir, 'filteredMessages')
        if file.exists():
            with open(file, 'rb') as f:
                self.databytes = f.read()
        else:
            raise Exception('no datafile available')

        # dictionnary comprehension
        resultmessages = [message for key, message in self.dictmessages.items()
                          if message['mtype'] in LCProtocolHelpers.rMessagesDef.keys()]

        events = []
        for message in resultmessages:
            name = LCMessageReceived.eventName(message.get('mtype'),
                                               message.get('class'),
                                               message.get('errorcode'))
            message.update({'evName': name})
            events.append(message)
        self.events = events
        mockserial = Mock()
        mockfsm = MagicMock(autospec=FSM.trigger)

        with unittest.mock.patch.multiple(LCMesReader,
                                          run=DEFAULT,
                                          stop_thread=DEFAULT):
            with unittest.mock.patch('lock_controler_dialog.LC_messages_reader.mlog',
                                     spec=logging.Logger, wraps=logging.Logger) as self.mocklog:
                self.lr = LCMesReader(mockserial, mockfsm)

    def test_handle_bytes_file(self):
        """reads the file using a random number of bytes
        self.databytes contains the messages
        reads n bytes at a time, n being randomly generated for each read
        decoded messages are logged in the lock reader
        logging is mocked to check decoded messages"""

        index = 0
        while True:
            n = random.randrange(1, 5)
            if index + n >= len(self.databytes):
                sliceobj = slice(index, len(self.databytes))
            else:
                sliceobj = slice(index, index + n)
            index += n
            self.lr.handle_bytes(self.databytes[sliceobj])
            if index >= len(self.databytes):
                break

        # now check the messages in the log are correct
        called = [json.loads(str(arg)) for (level, arg), empty_dic in self.mocklog.log.call_args_list if isinstance(arg, StrucLogMessage)]

        for c in called:
            self.assertTrue(c in self.events)

    def test_handle_bytes_file_with_error_missed_bytes(self):
        """ checks that if an error occur in one message, the handle byte does not crash and
        following message are decoded OK
        reads the bytes using a random number of bytes
         #   error generated which misses one or several bytes at random
         all decoded messages are logged including errors
         checks that program does not crash and some messages are decodes OK"""
        index = 0

        # triple the length of self.databytes
        doublebytes = self.databytes + self.databytes + self.databytes
        missedbytes = []

        for p in range(1, 3):
            while True:
                n = random.randrange(1, 5)
                m = random.randrange(0, 10)
                p = 2
                error = abs(m - n) % 5 == 0
                if error:
                    # misses one byte
                    missedbytes.append(index)
                if index + n >= len(doublebytes):
                    sliceobj = slice(index + p * error, len(doublebytes))
                    if debug_test:
                        print(f'n : {n}, error : {error}, index : {index} , bytemissed:, bytes read {doublebytes[sliceobj]}')
                else:
                    sliceobj = slice(index + p * error, index + n)
                    if debug_test:
                        print(f'n : {n}, index : {index} , bytemissed: { hex(doublebytes[index]) * (error)}, bytes read {doublebytes[sliceobj]}')
                index += n
                self.lr.handle_bytes(doublebytes[sliceobj])
                if index >= len(doublebytes):
                    break

        if debug_test:
            print(self.mocklog.log.call_args_list)

    def test_handle_bytes_file_with_error_bytes_modified(self):
        """ checks that if an error occur in one message, the handle byte does not crash and
        following message are decoded OK
        reads the bytes using a random number of bytes
         #   error generated which misses one or several bytes at random
         all decoded messages are logged including errors
         checks that program does not crash and some messages are decodes OK"""
        index = 0
        # triple the length of self.databytes and changes it to integer
        doublebytes = [e for e in self.databytes + self.databytes + self.databytes]

        while True:
            n = random.randrange(1, 5)
            m = random.randrange(0, 10)
            error = abs(m - n) % 5 == 0
            if index + n >= len(doublebytes):
                sliceobj = slice(index, len(doublebytes))
            else:
                sliceobj = slice(index, index + n)
            slicebytes = doublebytes[sliceobj]
            if error:
                slicebytes[0] = doublebytes[index] + 1
                if debug_test:
                    print(f'index {index} modified slicebytes : {slicebytes}')
            else:
                if debug_test:
                    print(f'index {index} bytes read {slicebytes}')
            self.lr.handle_bytes(slicebytes)
            index += n
            if index >= len(doublebytes):
                break

        if debug_test:
            print(self.mocklog.log.call_args_list)


@unittest.skipUnless(LCConnected, 'need LC connected')
class TestLockReaderwithSerialPort(Test_super_class, unittest.TestCase):
    """ tests for reading test messages on the serial port
    fsm.trigger is mocked and calls to mock are analyzed to check that all events
    are correclty created
    """

    def setUp(self):
        from lock_controler_dialog.LC_messages_manager import LCMesManager

        print('this test requires the Lock Controler to send test messages')
        print('download the softknit21_messages program through visual code into the '
              'STM lock controler board connected on USB port')
        print('make sure the serial terminal is stopped in visual code or close visual code')
        input('press enter to continue and wait for a few seconds')

        super().commonsetUp_for_pm()

        # mock mlog used in handlebytes
        patcher2 = patch('lock_controler_dialog.LC_messages_reader.mlog', spec=logging.Logger, wraps=logging.Logger)
        self.mocklog = patcher2.start()
        self.addCleanup(patcher2.stop)

        # need a LockControler with the serial port
        protocolmanager = Mock()
        LC = LCMesManager(protocolmanager)
        self.lr = LC.lockReader
        self.serialport = self.lr.serialport

        # do not want to call the FSM for tests of this module
        self.lr.fsm.trigger = Mock(autospec=FSM.trigger, side_effect=self._result)

        self.counting = 0
        # dictionnary comprehension
        resultmessages = [message for key, message in self.dictmessages.items()
                          if message['mtype'] in LCProtocolHelpers.rMessagesDef.keys()]

        events = []
        for message in resultmessages:
            name = LCMessageReceived.eventName(message.get('mtype'),
                                               message.get('class'),
                                               message.get('errorcode'))
            message.update({'evName': name})
            events.append(message)
        self.events = events

    def _result(self, **event):
        self.counting += 1

    def test(self):
        """ reads all the input on the serial port
        then test events correctly created"""
        while True:
            if self.counting == len(self.events):
                break
        self.lr.stop_thread()
        self.serialport.close()
        gen = (event for args, event in self.lr.fsm.trigger.call_args_list)
        for event in gen:
            self.assertTrue(event in self.events)
        loglevels = [level for (level, arg), empty_dic
                     in self.mocklog.log.call_args_list]
        # there are several incorrect bytes at the beginning because the serial needs to synchronize

        self.assertFalse(logging.CRITICAL in loglevels[-24:])

    def tearDown(self):

        pass


if __name__ == '__main__':
    import unittest

    from common.tools_for_log import logconf

    mlog = logconf('__file__', test=True)

    unittest.main()
