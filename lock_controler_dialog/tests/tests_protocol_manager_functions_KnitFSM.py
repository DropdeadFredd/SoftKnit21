#!/usr/bin/env python
# coding:utf-8
"""
  SoftKnit21 framework

License
=======

 Copyright (C) <2019>  <Odile Troulet-Lamberttex@orange.fr>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or  any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.



Module purpose
==============

Test for class LCProtocolMgr : second part of the tests for the methods

Implements
==========

Documentation
=============

Usage
=====


@author: Odile Troulet-Lambert
@copyright: Odile Troulet-Lambert
@license: GPL
"""
import unittest
from unittest.mock import Mock, patch

from kpart_knitting_model.running_knit import RunningKnit
from lock_controler_dialog.protocol_manager import LCProtocolMgr
from lock_controler_dialog.LC_Helpers import LCProtocolHelpers
from common.SoftKnit21Errors import LCManagerError
from common.tools_for_log import logconf
from lock_controler_dialog.tests.tests_super_class import Test_super_class
import logging
import json


class Test_functions_KnitFsm (Test_super_class, unittest.TestCase):

    def setUp(self):

        super().commonsetUp_for_pm()

    def test1_write_reset(self):
        """ test messageNumber incremented, LCBuffers ==0, timer_cancel called
        send message type x, log critical"""
        event = {}
        currentmess = self.pm.messageNumber
        self.pm.running_knit.knit_reset.side_effect = 'l'

        self.pm.write_Reset(**event)

        self.assertEqual(self.pm.LCBuffers, 0)
        self.assertEqual(self.pm.messageNumber, (currentmess + 1) % 128)
        (message, timout), emptydic = self.pm.LC.write_message.call_args_list[0]
        self.assertEqual(message, self.pm.currentMess)
        self.assertEqual(self.pm.currentMess.mtype, 'x')
        self.assertEqual(self.pm.currentMess.datadict['bufftype'], 'l')
        self.assertEqual(timout, LCProtocolHelpers.TIMEOUT_MESSAGE)
        (level, messdic), emptydic = self.mocklog.log.call_args_list[0]
        self.assertEqual(level, logging.CRITICAL)
        logdic = json.loads(str(messdic))
        re = {key: value for key, value in logdic.items() if (key, value) in self.pm.currentMess.toDict().items()}
        self.assertEqual(re, self.pm.currentMess.toDict())

    def test2_end_CP_not_error(self):
        """ test current_work endCP called , LCBUffers decreased
        nextBuffer called, log DEBUG"""
        event = {'evName': 'on_msg_in_R', 'mtype': 'R'}
        self.pm.LCBuffers = 1
        self.pm.running_knit.endCP = Mock(autospec=RunningKnit.endCP, side_effect='R')
        self.pm.nextBuffer = Mock(spec=LCProtocolMgr.nextBuffer)

        self.pm.end_CP(**event)

        self.pm.nextBuffer.assert_called_once()
        self.pm.running_knit.endCP.assert_called_once_with('R')
        self.assertEqual(self.pm.LCBuffers, 0)
        (level, message), emptydic = self.mocklog.log.call_args_list[0]
        self.assertEqual(level, logging.DEBUG)
        self.assertTrue('R' in message)

    def test3_end_CP_with_error(self):
        """ test LCManagerError raised """
        event = {'mtype': 'W'}
        with self.assertRaises(LCManagerError):
            self.pm.end_CP(**event)

    def test4_LC_reset(self):
        """test terminate work is called, log critical and changeFsm to main start
        one test from main FSM , one test from Knit Fsm
        test message W sent"""
        event = {'evName': 'on_msg_in_e_4*'}
        self.pm.running_knit.terminate_current_work = Mock(autospec=RunningKnit.terminate_current_work)
        self.pm.create_write_message = Mock(autospec=self.pm.create_write_message)

        self.pm.LC_reset(**event)

        self.pm.running_knit.terminate_current_work.assert_called_once()
        self.pm.create_write_message.assert_called_once()
        (level, message), emptydic = self.mocklog.log.call_args_list[0]
        self.assertEqual(level, logging.CRITICAL)
        self.assertEqual(self.pm.currentFsm, self.pm.InitFsm)
        self.assertEqual(self.pm.currentFsm.state, 'waitingLC')

        self.pm.changeFsm('KnitFsm', **event)
        self.pm.create_write_message.reset_mock()

        self.pm.LC_reset(**event)

        self.assertEqual(self.pm.currentFsm, self.pm.InitFsm)
        self.assertEqual(self.pm.currentFsm.state, 'waitingLC')
        self.pm.create_write_message.assert_called_once()

    def test5_stop(self):
        """test stop log critical and changeFsm to MainFsm stop"""

        event = {'evName': 'on_msg_in_e_4*'}

        self.pm.stop(**event)

        self.pm.running_knit.terminate_current_work.assert_called_once()
        (level, message), emptydic = self.mocklog.log.call_args_list[0]
        self.assertEqual(level, logging.CRITICAL)
        self.assertEqual(self.pm.currentFsm, self.pm.MainFsm)
        self.assertEqual(self.pm.currentFsm.state, 'stop')

    def test6_call_presenter(self):
        """ test call for presenter"""

        event = {'evName': 'toto'}

        self.pm.call_presenter('request', 'this is a message', 'event for call back', **event)

        self.pm.pres.usr_request.called_once()

        self.pm.call_presenter('message', 'this is a message', None, **event)

        self.pm.pres.usr_msg_out.called_once()

    def tearDown(self):

        pass


if __name__ == '__main__':

    import unittest

    from common.tools_for_log import logconf

    mlog = logconf('__file__', test=True)
    unittest.main()

