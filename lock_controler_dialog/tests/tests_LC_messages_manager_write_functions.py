

# coding:utf-8

"""
  SoftKnit21 framework

License
=======

 Copyright (C) <2019>  <Odile Troulet-Lamberttex@orange.fr>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or  any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.



Module purpose
==============
Tests for write functions of module Lock_controler_writer
tests are in isolation of serialport and protocol Manager


Implements
==========

Documentation
=============

Usage
=====


@author: Odile Troulet-Lambert
@copyright: Odile Troulet-Lambert
@license: GPL
"""


import unittest
from unittest.mock import Mock, patch, call, MagicMock, DEFAULT
from lock_controler_dialog.LC_message_tosend import LCMessagetoSend
from lock_controler_dialog.LC_messages_manager import LCMesManager
from lock_controler_dialog.LC_Helpers import LCProtocolHelpers


class Test_write_bytes(unittest.TestCase):
    """ tests for LockControler functions with no LC connected
    serial write are patched and calls to serial write are checked for correct parameters"""

    def setUp(self):
        """ creates the instance lockcontroler and the message to write"""
        protocolmanager = Mock()
        queue = Mock()
        with unittest.mock.patch.multiple(LCMesManager,
                                          _find_device=DEFAULT,
                                          start_serial=DEFAULT,
                                          ) as LCmock:

                self.LC = LCMesManager(protocolmanager, queue)

        from lock_controler_dialog.tests.data.data_for_LCMessages_Tests import datadictmessages
        self.wmes = LCMessagetoSend(**datadictmessages.get('ml1'))
        self.wmesbytes = [(ord(self.wmes.mtype) + 128),
                          self.wmes.lenpayload,
                          * self.wmes.payload,
                          self.wmes.checksum]
        self.smes = LCMessagetoSend(**datadictmessages.get('mA'))
        self.smesbytes = [ord(self.smes.mtype) + 128]

    def test_write_bytes_all_atonce_no_timeout(self):
        """ test _write_bytes with all bytes sent at once, and no time out
        one test for l message 1 test for single byte messages
        checks that timer is None"""
        self.LC.serialport.write.configure_mock(**{'side_effect': [self.wmes.lenpayload + 3]})
        self.wmes.bytestosend = self.smesbytes
        self.wmes.timer = 'something'

        self.LC._write_bytes(self.wmes, timeout=None)

        self.LC.serialport.write.assert_called_once_with(self.wmes.bytestosend)
        self.assertEqual(self.wmes.repeat, 1)
        self.assertTrue(self.wmes.timer == None)

        self.LC.serialport.write.reset_mock()
        self.LC.serialport.write.configure_mock(**{'side_effect': [1]})
        self.smes.bytestosend = self.smesbytes

        self.LC._write_bytes(self.smes, timeout=None)

        self.LC.serialport.write.assert_called_once_with(self.smesbytes)
        self.assertEqual(self.smes.repeat, 1)
        self.assertTrue(self.smes.timer == None)

    def test_write_bytes_not_all_at_once(self):
        """ test _write_bytes with  bytes sent in several times, and no time out"""
        self.LC.serialport.write.configure_mock(**{'side_effect': [3, 2, 3 + len(self.wmesbytes) - 5]})
        self.wmes.bytestosend = self.wmesbytes

        self.LC._write_bytes(self.wmes, timeout=None)

        expected = [call(self.wmesbytes[0:],),
                    call(self.wmesbytes[3:],),
                    call(self.wmesbytes[5:],), ]
        self.assertTrue(self.LC.serialport.write.call_args_list == expected)

    def test_write_bytes_with_timeout_timer_exists_and_cancelled(self):
        """test _write_bytes with timeout bytes sent all at once
        test timer exists and can be cancelled"""
        import threading
        test_timeout = 2
        self.LC.serialport.write.configure_mock(**{'side_effect': [len(self.wmesbytes)] * 5})
        self.wmes.bytestosend = self.wmesbytes

        self.LC._write_bytes(self.wmes, timeout=test_timeout)

        self.wmes.timer.cancel()
        self.assertTrue(self.wmes.timer != None)
        self.assertTrue(isinstance(self.wmes.timer, threading.Timer))

    def test_write_bytes_with_timeout_timer_expires(self):
        """test _write_bytes with timeout;  bytes sent all at once
        test timer calls function at expiration"""
        import time
        test_timeout = 2
        self.LC.serialport.write.configure_mock(**{'side_effect': [len(self.wmesbytes)] * 5})
        self.LC.repeat_write = MagicMock(autospec=self.LC.repeat_write)
        self.wmes.bytestosend = self.wmesbytes

        self.LC._write_bytes(self.wmes, timeout=test_timeout)
        time.sleep(5)

        (message, time), args = self.LC.repeat_write.call_args_list[0]
        self.assertEqual(message, self.wmes)

    def test_write_message(self):
        """tests calculation of bytestosend and call to _write_bytes
        with multibyte message and singlebyte message"""
        test_timeout = 2
        self.LC._write_bytes = MagicMock(autospec=LCMesManager._write_bytes)

        self.LC.write_message(self.wmes, timeout=None)
        self.assertEqual(self.wmes.bytestosend, self.wmesbytes)
        self.LC._write_bytes.assert_called_once_with(self.wmes, None)

        self.LC._write_bytes.reset_mock()
        self.LC.write_message(self.smes, timeout=test_timeout)
        self.assertEqual(self.smes.bytestosend, self.smesbytes)
        self.LC._write_bytes.assert_called_once_with(self.smes, test_timeout)


class Test_repeat_write (unittest.TestCase):

    def setUp(self):
        """ creates the instance lockcontroler and the message to write"""
        protocolmanager = Mock()
        queue = Mock()
        with unittest.mock.patch.multiple(LCMesManager,
                                          _find_device=DEFAULT,
                                          start_serial=DEFAULT,
                                          ) as LCmock:

                self.LC = LCMesManager(protocolmanager, queue)
        from lock_controler_dialog.fsm import FSM
        self.LC.pm.trigger = MagicMock(autospec=FSM.trigger)
        self.LC._write_bytes = MagicMock(autospec=LCMesManager._write_bytes)

        from lock_controler_dialog.tests.data.data_for_LCMessages_Tests import datadictmessages
        self.wmes = LCMessagetoSend(**datadictmessages.get('ml1'))
        self.wmesbytes = [(ord(self.wmes.mtype) + 128),
                          self.wmes.lenpayload,
                          * self.wmes.payload,
                          self.wmes.checksum]
        self.smes = LCMessagetoSend(**datadictmessages.get('mA'))
        self.smesbytes = [ord(self.smes.mtype) + 128]

    def test_repeat_write_not_max_repeat(self):
        """ tests message is repeated """
        import json
        import logging

        test_timeout = 2
        with patch('lock_controler_dialog.LC_messages_manager.mlog', autospec=logging.Logger, wraps=logging.Logger) as mocklog:

            self.LC.repeat_write(self.wmes, test_timeout)

            self.assertEqual(self.wmes.repeat, 1)
            (mess, timout), args = self.LC._write_bytes.call_args_list[0]
            self.assertEqual(timout, test_timeout)
            self.assertEqual(mess, self.wmes)
            (level, message), emptydic = mocklog.log.call_args_list[1]
            self.assertEqual(level, logging.CRITICAL)
            self.assertEqual(json.loads(str(message)), self.wmes.toDict())

    @patch.object(LCProtocolHelpers, 'MAXREPEAT', 1)
    def test_repeat_write_max_repeat(self):
        self.wmes.repeat = 1

        self.LC.repeat_write(self.wmes)

        args, event = self.LC.pm.trigger.call_args
        self.assertEqual(event, {'evName': 'on_max_repeat', **self.wmes.toDict()})

    def test_repeat_write_with_repeat_gives_event(self):
        self.wmes.repeat = 'name_of_event'

        self.LC.repeat_write(self.wmes)

        args, event = self.LC.pm.trigger.call_args
        self.assertEqual(event, {'evName': 'name_of_event', **self.wmes.toDict()})

    def tearDown(self):
        pass


if __name__ == '__main__':
    import logging
    import unittest

    from common.tools_for_log import logconf

    mlog = logconf('__file__', test=True)
    unittest.main()
