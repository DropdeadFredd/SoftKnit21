#!/usr/bin/env python
# coding:utf-8
"""
  SoftKnit21 framework

License
=======

 Copyright (C) <2019>  <Odile Troulet-Lamberttex@orange.fr>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or  any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.



Module purpose
==============

Tests for protocol manager : tests for knitfsm transitions for waiting and knit-reset

Implements
==========

 - B{TestProtocolManager}

Documentation
=============



Usage
=====


@author: Odile Troulet-Lambert
@copyright: Odile Troulet-Lambert
@license: GPL
"""
import unittest

from lock_controler_dialog.LC_message_tosend import LCMessagetoSend
from lock_controler_dialog.LC_message_received import LCMessageReceived, create_from_dic
from common.tools_for_log import search_in_log
from lock_controler_dialog.tests.tests_super_class import Test_super_class
import logging


class TestKnitFsmTransitions_knitreset (Test_super_class, unittest.TestCase):
    """ tests for KnitFsm transitions with src = WatingUserConf
    test objective only go through each transition and check new state
    other tests suites are targeted towards testing the results of the functions """

    def setUp(self):
        """ TestMainFsmTransitions creates the protocol manager instance and do the appropriate mocks
        changes current Fsm to KnitFsm
        triggers 3 transitions to put the KnitFsm in the knit_reset SoftKnit21Errors"""

        super().commonsetUp_for_pm()

        event = {'evName': 'evènement'}
        self.pm.changeFsm('KnitFsm', initial='knitreset', **event)

    def test_on_msg_in_a(self):

        dicmessage = self.dataformessages.get('ma')
        message = create_from_dic(dicmessage)
        event = message.createEvent()
        messnum = message.datadict.get('mesNumber')
        self.pm.currentMess = LCMessagetoSend(**self.dictmessages.get('ml1'))
        self.pm.currentMess.datadict.update({'mesNumber': messnum})
        attrs = {'nextCP.return_value': (85, 105, '1' * 7 + '0' * 7 + '1' * 7, 'r')}
        self.pm.running_knit.configure_mock(**attrs)

        self.pm.currentFsm.trigger(**event)

        self.assertEqual(self.pm.currentFsm.state, 'knit')
        self.assertTrue(search_in_log(self.mocklog.log.call_args_list, '',
                                      logging.CRITICAL))

    def test_on_msg_in_e_50(self):

        event = {'errorcode': 50, 'evName': 'on_msg_in_e_50'}

        self.pm.trigger(**event)

        self.assertEqual(self.pm.currentFsm.state, 'waitingUserConf')
        self.pm.pres.usr_request.assert_called_once()
        self.assertTrue(search_in_log(self.mocklog.log.call_args_list, '',
                                      logging.CRITICAL))

    def test_on_msg_in_star(self):
        """tests for on_mes_in_R and on msg_in_e_4*"""
        message = LCMessageReceived('R')
        event = {'evName': 'on_msg_in_R', **message.toDict(both=True)}

        self.pm.trigger(**event)

        self.assertEqual(self.pm.currentFsm.state, 'knitreset')
        self.assertTrue(search_in_log(self.mocklog.log.call_args_list, '',
                                      logging.CRITICAL))

    def tearDown(self):
        pass


if __name__ == '__main__':

    import unittest

    from common.tools_for_log import logconf

    mlog = logconf('__file__', test=True)

    unittest.main()
