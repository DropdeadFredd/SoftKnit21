

# coding:utf-8

"""
  SoftKnit21 framework

License
=======

 Copyright (C) <2019>  <Odile Troulet-Lamberttex@orange.fr>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or  any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.



Module purpose
==============

Tests for lclass LockControler : test for init and its functions

Implements
==========


Documentation
=============
some tests require connecting and disconnecting different USB devices
They are skipped by default/
To do write a module with a sepcific loader to launch all these tests together


Usage
=====


@author: Odile Troulet-Lambert
@copyright: Odile Troulet-Lambert
@license: GPL
"""


import unittest
from unittest.mock import Mock
from lock_controler_dialog.LC_messages_manager import LCMesManager
from common.SoftKnit21Errors import LockControlerError
from lock_controler_dialog.tests.tests_super_class import Test_super_class
#from common.configuration_manager import get_test_configuration

# This module is very touchy do not modify unless you think you know what you are doing
# in Test_find_device_and_connection_retry
# if test1 and test2 are passed in sequence, test2 fails for device busy
# if test1 is skipped then test2 passes.
# run these tests with preferences debug I/O external console
# and external console waits on exit

LCConnected = False


class Test_choose_previous_device (Test_super_class, unittest.TestCase):

    def setUp(self):
        """ create a test configuration with ACM0"""
        self.devicesOK = ['/dev/ttyACM1', '/dev/ttyACM0']
        self.devicesNOK = ['/dev/ttyACM2', '/dev/ttyACM3']
        conf = super().get_test_configuration(__file__, forceNew=True)
        confelement = conf.lockControlerConf
        confelement.fill({'port': '/dev/ttyACM0'})
        confelement.update()

    def test_choose_previous_device(self):
            mock = Mock()
            res = LCMesManager._choose_previous_device(mock, self.devicesOK)
            self.assertEqual(res, '/dev/ttyACM0')
            res = LCMesManager._choose_previous_device(mock, self.devicesNOK)
            self.assertEqual(res, '/dev/ttyACM2')
            res = LCMesManager._choose_previous_device(mock, 'toto')
            self.assertEqual(res, 'toto')


class Test_find_device_and_connection_retry (Test_super_class, unittest.TestCase):

    def mock_usreq(self, message):
        print(message)

    def setUp(self):
        self.pm = Mock(autospec=True)
        attrs = {'pm.pres.usr_request': {'side_effect': self.mock_usreq}}
        self.pm.configure_mock(**attrs)

    @unittest.skipUnless(LCConnected, 'need LC connected')
    def test1_find_device(self):
        # test several LC connected
        print(self._testMethodName)
        print("to run this test make sure 2 USB controlers are connected, ")
        input("press enter to continue")

        with unittest.mock.patch('lock_controler_dialog.LCHelpers.LCConfigurations.is_LC',
                                 autospec=True,
                                 return_value=True):
            # do not start serial so as not to have a second thread
            with unittest.mock.patch('lock_controler_dialog.LC_messages_manager.LCMesManager.start_serial'
                                     ) as self.mstartserial:
                self.LC = LCMesManager(self.pm)
                self.suspect = self.LC.serialport
                res = self.LC._find_device()
                self.assertTrue(len(res) >= 1)
                self.listdevices = res
                self.pm.assert_not_called()
                # test no LC connected 2 retries
                print("now disconnect all LCs ")

                input('press enter to continue')
                with self.assertRaises(LockControlerError):
                    res = self.LC._find_device()
                    self.assertEqual(pm.mockusreq.call_count, 2)

                # test connect on second request
                print("")
                print("now CONNECT ONE  LC")
                input('press enter to continue')
                res = self.LC._find_device()
                self.assertTrue(len(res) == 1)
        self.suspect.close()

    @unittest.skipUnless(LCConnected, 'need LC connected')
    def test2_startSerial_and_retry_LC_connnection(self):
        import threading
        import copy

        print(self._testMethodName)
        print("this test needs 2 lock controlers connected")
        input("type enter to continue")
        conf = super().get_test_configuration(__file__, forceNew=True)
        with unittest.mock.patch('threading.Thread.start') as mockLR:
            with unittest.mock.patch('lock_controler_dialog.LC_messages_manager.mlog',
                                     spec=logging.Logger) as mocklog:
                with unittest.mock.patch('lock_controler_dialog.LCHelpers.LCConfigurations.is_LC',
                                         autospec=True,
                                         return_value=True):

                    self.LCwithReader = LCMesManager(self.pm)
                    self.suspect = self.LCwithReader.serialport
                    self.listdevices = copy.deepcopy(self.LCwithReader.devices)

                    self.assertTrue(isinstance(self.LCwithReader.lockReader, threading.Thread))
                    self.assertEqual(self.LCwithReader.lockReader.name, 'LCReader')
                    self.assertTrue(self.LCwithReader.portparams.get('port') in self.listdevices)
                    mocklog.log.assert_called_once()
                    mockLR.assert_called_once()
                    self.port = self.LCwithReader.portparams.get('port')

        # first retry
        with unittest.mock.patch(
            'lock_controler_dialog.lock_controler_writer.LCMesManager.start_serial',
                autospec=True) as mstartserial:
            self.LCwithReader.devices = copy.deepcopy(self.listdevices)

            self.LCwithReader.retry_LC_connection()
            self.assertEqual(len(self.LCwithReader.devices), len(self.listdevices) - 1)
            self.assertTrue(self.LCwithReader.portparams.get('port') == self.listdevices[0])
            mstartserial.assert_called_once()
            # thread created in previous instance has been killed in retry_LCconnection
            self.assertIsNone(self.LCwithReader.lockReader)

            # second retry, on more port in the list
            # no new thread created so mock the reader
            with unittest.mock.patch.object(self.LCwithReader, 'lockReader') as mockLR:
                with self.assertRaises(LockControlerError):
                    self.LCwithReader.retry_LC_connection()
                    self.mstartserial.not_called()
        self.suspect.close()

    @unittest.skipUnless(LCConnected, 'need LC connected')
    def test3_2retriesLC_connections_with_start_serial(self):
        print(self._testMethodName)
        print("this test needs 2 lock controlers connected")
        input("type enter to continue")
        conf = super().get_test_configuration(__file__, forceNew=True)
        with unittest.mock.patch('threading.Thread.start'):
            with unittest.mock.patch('lock_controler_dialog.LCHelpers.LCConfigurations.is_LC',
                                     autospec=True,
                                     return_value=True):
                self.LC1 = LCMesManager(self.pm)
                self.assertTrue(self.LC1.serialport.is_open)
                reader1 = self.LC1.lockReader
                port1 = self.LC1.portparams.get('port')
                serialport1 = self.LC1.serialport
                self.LC1.retry_LC_connection()
                self.assertNotEqual(self.LC1.lockReader, reader1)
                self.assertNotEqual(port1, self.LC1.portparams.get('port'))
                self.assertFalse(serialport1.is_open)
                self.assertTrue(self.LC1.serialport.is_open)

                with self.assertRaises(LockControlerError):
                    self.LC1.retry_LC_connection()
                self.assertFalse(self.LC1.serialport.is_open)
                self.assertIsNone(self.LC1.lockReader)


if __name__ == '__main__':
    import logging
    import unittest

    from common.tools_for_log import logconf

    mlog = logconf('__file__', test=True)
    unittest.main()

