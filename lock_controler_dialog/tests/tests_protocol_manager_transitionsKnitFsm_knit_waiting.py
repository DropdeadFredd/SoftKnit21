#!/usr/bin/env python
# coding:utf-8
"""
  SoftKnit21 framework

License
=======

 Copyright (C) <2019>  <Odile Troulet-Lamberttex@orange.fr>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or  any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.



Module purpose
==============

Tests for protocol manager : test for KnitFsm transitions for knit state
see documentation for graphical representation of the FSM

Implements
==========



Documentation
=============



Usage
=====


@author: Odile Troulet-Lambert
@copyright: Odile Troulet-Lambert
@license: GPL
"""
import unittest


from lock_controler_dialog.LC_message_tosend import LCMessagetoSend
from lock_controler_dialog.LC_message_received import LCMessageReceived, create_from_dic

import logging
from common.tools_for_log import search_in_log
from lock_controler_dialog.tests.tests_super_class import Test_super_class


class TestKnitFsmTransitions_knitstate (Test_super_class, unittest.TestCase):
    """ tests for KnitFsm transitions with src = knit
    test objective only go through each transition and check new state
    other tests suites are targeted towards testing the results of the functions 
    """

    def setUp(self):
        """ 
        TestMainFsmTransitions creates the protocol manager instance and do the appropriate mocks
        most functions of protocol manager are mocked as tested in other tests
        changes current Fsm to KnitFsm
        """
        super().commonsetUp_for_pm()
        event = {'evName': 'evènement'}

        self.pm.changeFsm('KnitFsm', initial='knit', **event)

    def test1_on_msg_in_a(self):
        message = self.dictmessages.get('ma')
        event = {'evName': 'on_msg_in_a', **message}
        messnum = message.get('mesNumber')
        self.pm.currentMess = LCMessagetoSend(**self.dictmessages.get('ml1'))
        self.pm.currentMess.datadict.update({'mesNumber': messnum})
        attrs = {'nextCP.return_value': (85, 105, '1' * 7 + '0' * 7 + '1' * 7, 'r')}
        self.pm.running_knit.configure_mock(**attrs)

        self.pm.trigger(**event)

        self.assertEqual(self.pm.currentFsm.state, 'knit')
        self.assertTrue(search_in_log(self.mocklog.log.call_args_list, '',
                                      logging.CRITICAL))

    def test2_on_msg_in_n(self):
        message = self.dictmessages.get('ma')
        message['mtype'] = 'n'
        event = {'evName': 'on_msg_in_n', **message}
        messnum = message.get('mesNumber')
        self.pm.currentMess = LCMessagetoSend(**self.dictmessages.get('ml1'))
        self.pm.currentMess.datadict.update({'mesNumber': messnum})

        self.pm.trigger(**event)

        self.assertEqual(self.pm.currentFsm.state, 'knit')
        self.assertTrue(search_in_log(self.mocklog.log.call_args_list, '',
                                      logging.CRITICAL))

    def test3_on_msg_in_R_condition_False(self):

        message = LCMessageReceived('R')
        event = {'evName': 'on_msg_in_R', **message.toDict(both=True)}
        attrs = {'check_end_pass.return_value': False}
        self.pm.running_knit.configure_mock(**attrs)

        self.pm.trigger(**event)

        self.pm.pres.usr_request.assert_called_once()
        self.assertEqual(self.pm.currentFsm.state, 'waitingUserConf')
        self.assertTrue(search_in_log(self.mocklog.log.call_args_list, '',
                                      logging.CRITICAL))

    def test4_on_msg_in_R_condition_True(self):

        message = LCMessageReceived('R')
        event = {'evName': 'on_msg_in_R', **message.toDict(both=True)}
        attrs = {'check_end_pass.return_value': True,
                 'nextCP.return_value': (85, 105, '1' * 7 + '0' * 7 + '1' * 7, 'r')}
        self.pm.running_knit.configure_mock(**attrs)

        self.pm.trigger(**event)

        self.pm.pres.usr_request.assert_not_called()
        self.pm.running_knit.check_end_pass.assert_called_once()
        self.pm.running_knit.endCP.assert_called_once()
        self.assertEqual(self.pm.currentFsm.state, 'knit')
        self.assertTrue(search_in_log(self.mocklog.log.call_args_list, '',
                                      logging.CRITICAL))

    def test5_on_msg_in_L_condition_False(self):

        message = LCMessageReceived('L')
        event = {'evName': 'on_msg_in_L', **message.toDict(both=True)}
        attrs = {'check_end_pass.return_value': False}
        self.pm.running_knit.configure_mock(**attrs)

        self.pm.trigger(**event)

        self.pm.pres.usr_request.assert_called_once()
        self.assertEqual(self.pm.currentFsm.state, 'waitingUserConf')
        self.assertTrue(search_in_log(self.mocklog.log.call_args_list, '',
                                      logging.CRITICAL))

    def test6_on_msg_in_L_condition_True(self):

        message = LCMessageReceived('L')
        event = {'evName': 'on_msg_in_L', **message.toDict(both=True)}
        attrs = {'check_end_pass.return_value': True,
                 'nextCP.return_value': (85, 105, '1' * 7 + '0' * 7 + '1' * 7, 'r')}
        self.pm.running_knit.configure_mock(**attrs)

        self.pm.trigger(**event)

        self.pm.pres.usr_request.assert_not_called()
        self.pm.running_knit.check_end_pass.assert_called_once()
        self.pm.running_knit.endCP.assert_called_once()
        self.assertEqual(self.pm.currentFsm.state, 'knit')
        self.assertTrue(search_in_log(self.mocklog.log.call_args_list, '',
                                      logging.CRITICAL))

    def test7_on_msg_in_e_50(self):

        dicmessage = self.dataformessages.get('me')
        message = create_from_dic(dicmessage)
        event = message.createEvent()
        event.update({'errorcode': 50, 'evName': 'on_msg_in_e_50'})

        self.pm.trigger(**event)

        self.pm.pres.usr_request.assert_called_once()
        self.assertEqual(self.pm.currentFsm.state, 'waitingUserConf')
        self.assertTrue(search_in_log(self.mocklog.log.call_args_list, '',
                                      logging.CRITICAL))


class TestKnitFsmTransitions_watinguserconf (Test_super_class, unittest.TestCase):
    """ tests for KnitFsm transitions with src = watinguserconf
    test objective only go through each transition and check new state
    other tests suites are targeted towards testing the results of the functions """

    def setUp(self):
        """ 
        TestMainFsmTransitions creates the protocol manager instance and do the appropriate mocks
        changes current Fsm to KnitFsm
        """
        super().commonsetUp_for_pm()
        event = {'evName': 'evènement'}

        self.pm.changeFsm('KnitFsm', initial='knit', **event)

    def test1_on_usr_click_lock_thread_push_condition_False(self):

        dicmessage = self.dataformessages.get('me')
        message = create_from_dic(dicmessage)
        event = message.createEvent()
        event.update({'errorcode': 50, 'evName': 'on_msg_in_e_50'})
        self.pm.currentFsm.trigger(**event)
        event = {'evName': 'on_usr_click_lock_thread_push', 'reset': False}

        self.pm.trigger(**event)

        self.assertEqual(self.pm.currentFsm.state, 'knit')
        self.assertTrue(search_in_log(self.mocklog.log.call_args_list, '',
                                      logging.CRITICAL))

    def test2_on_usr_click_lock_thread_push_condition_True(self):

        dicmessage = self.dataformessages.get('me')
        message = create_from_dic(dicmessage)
        event = message.createEvent()
        event.update({'errorcode': 50, 'evName': 'on_msg_in_e_50'})
        self.pm.currentFsm.trigger(**event)
        event = {'evName': 'on_usr_click_lock_thread_push', 'reset': True}
        attrs = {'knit_reset.return_value': 'l'}
        self.pm.running_knit.configure_mock(**attrs)

        self.pm.trigger(**event)

        self.assertEqual(self.pm.currentFsm.state, 'knitreset')
        self.assertTrue(search_in_log(self.mocklog.log.call_args_list, 'write_reset',
                                      logging.CRITICAL))
        self.assertFalse(search_in_log(self.mocklog.log.call_args_list, 'undefined event',
                                       logging.CRITICAL))

    def tearDown(self):
        pass


if __name__ == '__main__':
    import unittest

    from common.tools_for_log import logconf

    mlog = logconf('__file__', test=True)

    unittest.main()
