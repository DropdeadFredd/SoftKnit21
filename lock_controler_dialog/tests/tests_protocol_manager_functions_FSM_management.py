#!/usr/bin/env python
# coding:utf-8
"""
  SoftKnit21 framework

License
=======

 Copyright (C) <2019>  <Odile Troulet-Lamberttex@orange.fr>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or  any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.



Module purpose
==============

Tests for the class LCProtocolMgr : 1rst part of the tests for the Methods of this class

Implements
==========

Documentation
=============


Usage
=====


@author: Odile Troulet-Lambert
@copyright: Odile Troulet-Lambert
@license: GPL
"""
import unittest
from unittest.mock import Mock, patch

from lock_controler_dialog.LC_message_tosend import LCMessagetoSend
from common.SoftKnit21Errors import LCManagerError


from lock_controler_dialog.tests.tests_super_class import Test_super_class


class TestInitandFSMchanges (Test_super_class, unittest.TestCase):
    """ tests for init and individual functions"""

    def setUp(self):
        """
        creates the protocol manager
        mock the logger.mlog with wraps so as to be able to read the log
        mock LockControler writer and its methodswith autospec so as to verify the arguments passed
        mock the timer_cancel so as to avoid the start of a new thread
        """

        super().commonsetUp_for_pm()

        self.wmes = LCMessagetoSend(**self.dictmessages.get('ml1'))
        self.wmesbytes = [(ord(self.wmes.mtype) + 128),
                          self.wmes.lenpayload,
                          * self.wmes.payload,
                          self.wmes.checksum]

        self.smes = LCMessagetoSend(**self.dictmessages.get('mA'))
        self.smesbytes = [ord(self.smes.mtype) + 128]

    def test1_init(self):
        from lock_controler_dialog.fsm import FSM
        self.assertTrue(isinstance(self.pm, FSM))
        self.assertTrue(isinstance(self.pm.InitFsm, FSM))
        self.assertTrue(isinstance(self.pm.KnitFsm, FSM))
        self.assertTrue(isinstance(self.pm.TestsFsm, FSM))
        self.assertEqual(self.pm.state, 'start')
        self.assertEqual(self.pm.InitFsm.state, 'waitingLC')
        self.assertEqual(self.pm.KnitFsm.state, 'knit')
        self.assertEqual(self.pm.TestsFsm.state, 'idle')
        self.assertEqual(self.pm.name, 'MainFsm')
        self.assertEqual(self.pm.MainFsm, self.pm)
        self.assertEqual(self.pm.InitFsm.name, 'InitFsm')
        self.assertEqual(self.pm.KnitFsm.name, 'KnitFsm')
        self.assertEqual(self.pm.TestsFsm.name, 'TestsFsm')
        self.assertEqual(self.pm.parent, None)
        self.assertEqual(self.pm.InitFsm.parent, self.pm)
        self.assertEqual(self.pm.KnitFsm.parent, self.pm)
        self.assertEqual(self.pm.TestsFsm.parent, self.pm)

    def test2_currentFsm_property(self):
        """ checks that currentFsm cannot be changed"""
        self.assertEqual(self.pm.currentFsm, self.pm)
        with self.assertRaises(LCManagerError):
            self.pm.currentFsm = 'Init'

    def test3_check_mtype(self):
        event = {'evName': 'on_msg_in_d', 'mtype': 'd', 'class': 'normal', 'Vcc': 5.5, 'V15': 13.5, 'checksum': True}
        self.assertTrue(self.pm.check_mtype('d', **event))
        self.assertFalse(self.pm.check_mtype('e', **event))

    def test4_startSubFsm(self):
        """ test new current Fsm wrong state and no initial state """

        with self.assertRaises(LCManagerError):
            self.pm._startSubFsm('InitFsm', 'wrongstate')

        self.pm._startSubFsm('KnitFsm', None, **{})

        self.assertEqual(self.pm.currentFsm, self.pm.KnitFsm)
        self.assertEqual(self.pm.currentFsm.state, 'knit')

    # @unittest.mock.patch('LC_model.protocol_manager.mlog',
                         # autospec=logging.Logger, wraps=logging.Logger)
    def test5_startSubFsm_log(self):
        """test new current Fsm and initial state"""

        self.pm._startSubFsm('InitFsm', 'hardwareCheck')

        self.assertEqual(self.pm.currentFsm, self.pm.InitFsm)
        self.assertEqual(self.pm.currentFsm.state, 'hardwareCheck')
        (level, arg), emptydic = self.mocklog.log.call_args_list[0]
        self.assertTrue(f'{self.pm.currentFsm} started' in arg)
        self.assertTrue(f'state: {self.pm.currentFsm.state}' in arg)

    # @unittest.mock.patch('LC_model.protocol_manager.mlog',
                         # autospec=logging.Logger, wraps=logging.Logger)
    def test6_startSubFsm_Main(self):
        """test new current Fsm is Main and initial state"""

        self.pm._startSubFsm('MainFsm', 'start')

        self.assertEqual(self.pm.currentFsm, self.pm.MainFsm)
        self.assertEqual(self.pm.currentFsm.state, 'start')
        (level, arg), emptydic = self.mocklog.log.call_args_list[0]
        self.assertTrue(f'{self.pm.currentFsm} started' in arg)
        self.assertTrue(f'state: {self.pm.currentFsm.state}' in arg)

    # @unittest.mock.patch('LC_model.protocol_manager.mlog',
                         # autospec=logging.Logger, wraps=logging.Logger)
    def test7_changeFsm(self):
        """test new fsm become current and
        test log written with correct parameters
        test call of start_subFsm"""

        self.pm.changeFsm('InitFsm', 'hardwareCheck', **{'evName': 'event_name'})

        self.assertEqual(self.pm.currentFsm, self.pm.InitFsm)
        self.assertEqual(self.pm.currentFsm.state, 'hardwareCheck')
        (level, arg), emptydic = self.mocklog.log.call_args_list[0]
        self.assertTrue('change of Fsm' in arg)

    def tearDown(self):
        pass


from lock_controler_dialog.protocol_manager import LCProtocolMgr
from threading import Timer


class Test_timer_cancel (unittest.TestCase):

    def setUp(self):
        queue = Mock()
        with patch('lock_controler_dialog.protocol_manager.LCMesManager'):
                self.pm = LCProtocolMgr(queue)
        self.mockTimer = Mock(Timer,
                              autospec=Timer)

    def test1_timer_cancel_timer_exists(self):
        """tests references to timer are deleted
        """
        mess = LCMessagetoSend(**{'mtype': 'W'})
        self.pm.currentMess = mess
        self.pm.currentMess.timer = self.mockTimer

        self.pm.timer_cancel()

        self.assertTrue(self.pm.currentMess.timer is None)
        self.assertTrue(self.mockTimer.cancel.called)

    def test2_timer_cancel_timer_not__exists(self):
        """ test no problems"""
        mess = LCMessagetoSend(**{'mtype': 'W'})
        self.pm.currentMess = mess
        self.pm.currentMess.timer = None

        self.pm.timer_cancel()

        self.assertTrue(self.pm.currentMess.timer is None)
        self.assertTrue(self.mockTimer.cancel.not_called)


if __name__ == '__main__':
    import unittest

    from common.tools_for_log import logconf

    mlog = logconf('__file__', test=True)
    unittest.main()


