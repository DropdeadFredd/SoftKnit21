#!/usr/bin/env python
# coding:utf-8
"""  SoftKnit21 framework

License
=======

 Copyright (C) <2019>  <Odile Troulet-Lamberttex@orange.fr>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or  any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.



Module purpose
==============

tests for configuration Management

Implements
==========

 - B{test for configuration manager}

Documentation
=============
 tests for the module configuration manager
Usage
=====


@author: Odile Troulet-Lambert
@copyright: Odile Troulet-Lambert
@license: GPL
"""


import unittest
import logging
from pathlib import Path

__all__ = [
]

from common.configuration_manager import get_configuration
from lock_controler_dialog.protocol_manager import LCProtocolMgr
from lock_controler_dialog.LC_messages_manager import LCMesManager
from kpart_knitting_model.running_knit import RunningKnit
from unittest.mock import patch, create_autospec, Mock


class Test_super_class (unittest.TestCase):

    def get_tests_datadir(self, callingfile):
        """ returns the PosixPath for the file of data for the tests_script"""

        script = Path(callingfile).stem
        callingfile = Path(callingfile)
        if 'tests' in callingfile.parts:
            filepath = Path(*callingfile.parts[0:-1], 'data')
        else:
            filepath = Path(*callingfile.parts[0:-1], 'tests', 'data')
        return filepath

    def get_test_configuration(self, calling_file, forceNew=False):
        filepath = self.get_tests_datadir(calling_file)
        config_file = Path(filepath, 'config.pickle')
        conf = get_configuration(forceNew, config_file)
        return conf

    def commonsetUp_for_pm(self):
        """
        create the protocol manager
        mock LockControler writer and its method swith autospec so as to verify the arguments passed
        mock current_work with autospec so as to check the arguments
        """

        queue = Mock()
        with patch('lock_controler_dialog.protocol_manager.LCMesManager'):
                self.pm = LCProtocolMgr(queue)

        pres = Mock()
        self.pm.configure_presenter(pres)
        self.pm.pres.usr_request = Mock()

        self.pm.LC.configure_mock(autospec=LCMesManager.write_message, spec_set=True)
        self.pm.LC.configure_mock(autospec=LCMesManager.repeat_write,
                                  spec_set=True)
        # patchmt = patch.object(pm, 'timer_cancel',
        #                       autospec=LCProtocolMgr)
        #mocktimer_cancel = patchmt.start()
        # testCase.addCleanup(patchmt.stop)
        self.pm.running_knit = create_autospec(spec=RunningKnit,
                                               spec_set=True,
                                               instance=True)

        patchlog = patch('lock_controler_dialog.protocol_manager.mlog',
                         autospec=logging.Logger,
                         wraps=logging.Logger)
        self.mocklog = patchlog.start()
        self.addCleanup(patchlog.stop)

        patchel1 = patch('lock_controler_dialog.fsm.mlog', new=self.mocklog)
        mocklogp = patchel1.start()
        self.addCleanup(patchel1.stop)

        patches = patch('common.tools_for_log.mlog', new=self.mocklog)
        mocklogs = patches.start()
        self.addCleanup(patches.stop)

        from lock_controler_dialog.tests.data.data_for_LCMessages_Tests import datadictmessages, dataformessages
        self.dictmessages = datadictmessages
        self.dataformessages = dataformessages


if __name__ == '__main__':

    import unittest
    import logging
    from common.tools_for_log import logconf, search_in_log
    from lock_controler_dialog.tests.tests_super_class import Test_super_class

    mlog = logconf('__file__', test=True)

    class TestPatches (Test_super_class, unittest.TestCase):

        def setUp(self):
            super().commonsetUp_for_pm()

        def test1(self):
            """ tests for mocks"""

            self.assertTrue(isinstance(self.pm.LC, Mock))
            self.pm.create_write_message('W', {})

        def test2(self):
            """ tests methods of current_work mock"""
            attrs = {'knit_reset.return_value': 'l', 'check_end_pass.return_value': False}
            self.pm.running_knit.configure_mock(**attrs)

            res = self.pm.running_knit.knit_reset()

            self.assertEqual(res, 'l')

        def test3_mocklog_efficient_in_an_fsm_transition_with_mlog_and_StruclogMessage(self):
            """ test log is efficient in the transitions of the fsm"""
            self.pm.changeFsm('InitFsm', initial='waitingLC')
            self.assertEqual(self.pm.currentFsm, self.pm.InitFsm)
            event = {'evName': 'on_msg_in_w', 'brand': 'PassapE6000 ', 'soft': 'toto', 'hard': 'titi'}
            # get a test_configuration so that it exists
            conf = super().get_test_configuration(__file__, forceNew=True)

            self.pm.currentFsm.trigger(**event)

            self.assertTrue(search_in_log(self.mocklog.log.call_args_list, '',
                                          'on event : on_msg_in_w', logging.DEBUG, ))
            self.assertTrue(search_in_log(self.mocklog.log.call_args_list, '',
                                          'hardwareCheck', logging.DEBUG,))
            self.assertTrue(search_in_log(self.mocklog.log.call_args_list,
                                          'Passap', logging.DEBUG, key='brand'))

        def test4_mocklog_efficient_in_an_fsm_transition_with_Struclog(self):
            """ test log is efficient in the transitions of the fsm"""
            self.pm.changeFsm('KnitFsm', initial='knit')
            self.assertEqual(self.pm.currentFsm, self.pm.KnitFsm)
            event = {'evName': 'on_msg_in_e_20'}

            self.pm.currentFsm.trigger(**event)

            self.assertTrue(search_in_log(self.mocklog.log.call_args_list,
                                          'warning speed', logging.DEBUG,))

        def test5_mocklog_efficient_in_an_fsm_transition_with_Struclog(self):
            """ test log is efficient in the transitions of the fsm"""
            self.pm.changeFsm('KnitFsm', initial='knit')
            self.assertEqual(self.pm.currentFsm, self.pm.KnitFsm)
            event = {'evName': 'on_msg_in_e_20'}

            self.pm.currentFsm.trigger(**event)

            self.assertTrue(search_in_log(self.mocklog.log.call_args_list, '',
                                          'warning speed', logging.DEBUG,))

        def test6_mocklog_efficient_in_pm_function_with_StrucLog(self):
            """ test that call to presenter is efficient"""
            self.pm.changeFsm('InitFsm', initial='waitingLC')
            self.assertEqual(self.pm.currentFsm, self.pm.InitFsm)
            event = {'evName': 'on_msg_in_w', 'brand': 'PassapE6000 ', 'soft': 'toto', 'hard': 'titi'}
            # get a test_configuration so that it exists
            conf = super().get_test_configuration(__file__, forceNew=True)

            self.pm.currentFsm.trigger(**event)

            self.assertTrue(search_in_log(self.mocklog.log.call_args_list,
                                          'message sent', logging.DEBUG,))

        def test7_mocklog_efficient_in_pm_function_with_mlog(self):
            """ test that call to presenter is efficient"""
            self.pm.changeFsm('InitFsm', initial='waitingLCAckR')
            self.assertEqual(self.pm.currentFsm, self.pm.InitFsm)
            event = {'evName': 'on_msg_in_A'}

            self.pm.currentFsm.trigger(**event)

            self.assertTrue(search_in_log(self.mocklog.log.call_args_list,
                                          'FSM', logging.DEBUG, ))

            self.assertTrue(search_in_log(self.mocklog.log.call_args_list,
                                          'started', logging.DEBUG,))

        def test8_one_mock_faor_all_logs(self):
            """test that fsm calls to log and pm calls to log are registered in the same log"""
            event = {'mesNumber': 3}
            self.pm.errorBufferMess(repeat=False, **event)

            event = {'evName': 'toto'}
            self.pm.trigger(**event)
            self.assertTrue(search_in_log(
                self.mocklog.log.call_args_list,
                'error 4*',
                logging.CRITICAL))

        def tearDown(self):

            pass

    unittest.main()



