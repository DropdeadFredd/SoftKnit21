#!/usr/bin/env python
# coding:utf-8
"""
  This file is part of SoftKnit21 framework

License
=======

 Copyright (C) <2019>  <Odile Troulet-Lamberttex@orange.fr>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or  any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.



Module purpose
==============

reading  messages from the lock controler

Implements
==========

 - B{Lock Controler Reader}

Documentation
=============

    receive messages from the lock controler

Usage
=====


@author: Odile Troulet-Lambert
@copyright: Odile Troulet-Lambert
@license: AGPL
"""

from gettext import gettext as _
from threading import Thread, get_ident
from lock_controler_dialog.LC_message_received import LCMessageReceived
from common.tools_for_log import StrucLogMessage
from common.SoftKnit21Errors import LCMessageError
import logging


from common.tools_for_log import logconf, get_TID

mlog = logging.getLogger(__name__)


class LCMesReader (Thread):
    """ reads messages from the lock contoler in a specific thread:
    builds the LCmessage_received dictionnary

    @serialport  : port usb port wher Lock controler is connected
    @type : Serial instance
    
    @m message being received
    @ type : LCMessageReceived

    @payload_length : length of payload of being received message
    @type : None or integer

    @message_type : type of message beeing received
    @type : ascii character in the range of message types of the protocol
    
    @queue : the events queue used by lockReader to write events and by protocol manager to read the events
    @type : an instance of a python simple QueueObject 
    """

    def __init__(self, serialport, queue):
        # create the new thread with the name lock_reader
        super().__init__(name='LCReader')
        self.serialport = serialport
        # flush buffers
        self.serialport.reset_input_buffer()
        self.queue = queue
        self.m = None
        return

    def isStartByte(self, byte):
        import sys
        """ test if  byte is a start byte"""
        if byte >= int.from_bytes(b'\x80', sys.byteorder):
            return True
        else:
            return False

    def handle_bytes(self, n_bytes):
        i = 0

        while (n_bytes is not None) and (n_bytes != b'') and (n_bytes != None) and n_bytes != []:
            if self.isStartByte(n_bytes[i]):
                # this is the message type.  start of a new message
                # check that the previous one was correct
                if self.m != None:
                    if not self.m.isValid():
                        mlog.log(logging.CRITICAL, f'message {self.m.mtype} is not valid')
                        mlog.log(logging.CRITICAL, StrucLogMessage(**self.m.toDict(both=True)))

                # new message
                self.m = LCMessageReceived(chr(n_bytes[i] - 128))
                if self.m.mtype.isupper():
                    # single byte message
                    try:
                        event = self.m.createEvent()
                        mlog.log(logging.DEBUG, f'in handle bytes triggered event :')
                        mlog.log(logging.DEBUG, StrucLogMessage(**event))
                        self.queue.put(**event)
                        self.m = None
                    except (LCMessageError, AttributeError) as e:
                        mlog.log(logging.CRITICAL, StrucLogMessage(**self.m.toDict(both=True)))
                        mlog.log(logging.CRITICAL, e.args, exc_info=e, stack_info=True)
                elif self.m.mtype.islower():
                    # there is a payload
                    self.m.lenpayload = 0
                else:
                    # wrong byte; need to convert it back to bytes to display error message
                    mlog.log(logging.CRITICAL, f'{self.m.mtype.encode()} in{n_bytes} : wrong message type received from LC')
                    mlog.log(logging.CRITICAL, StrucLogMessage(**self.m.toDict(both=True)))
            elif self.m != None and self.m.lenpayload == 0 and self.m.mtype.islower():
                # this is the payload length
                self.m.lenpayload = n_bytes[i]
            elif self.m != None and len(self.m.payload) < self.m.lenpayload and self.m.lenpayload > 0:
                # this is a payload byte
                self.m.payload.add(n_bytes[i])
            elif self.m != None and len(self.m.payload) == self.m.lenpayload:
                # this is the checksum
                self.m.set_checksum(n_bytes[i])
                try:
                    event = self.m.createEvent()
                    mlog.log(logging.DEBUG, f'in handle_bytes triggered event :')
                    mlog.log(logging.DEBUG, StrucLogMessage(**event))
                    self.queue.put(** event)
                    self.m = None
                except (LCMessageError, AttributeError) as e:
                    mlog.log(logging.CRITICAL, StrucLogMessage(**self.m.toDict(both=True)))
                    mlog.log(logging.CRITICAL, e.args, exc_info=e, stack_info=True)

            else:
                # there is an error in message bytes
                mlog.log(logging.CRITICAL, f'{n_bytes[i]} wrong byte received from LC')
                if self.m is not None:
                    mlog.log(logging.CRITICAL, StrucLogMessage(**self.m.toDict(both=True)))
            i += 1
            if i >= len(n_bytes):
                break

    def run(self):
        #import Lock_Controler.protocol_manager
        # to be replace in 3.8 with get_native_id
        self.TID = get_TID()
        self.identi = get_ident()
        mlog.log(logging.DEBUG, f'starting LockReader thread TID = {self.TID}, thread Name LCReader , ident {self.ident}')
        self._running = True
        while self._running:
            # read bloquant mais le système va rendre la main lorsque il n'y a rien à lire
            # self.handle_bytes(self.serial.read())
            if self.serialport.in_waiting > 0:
                self.handle_bytes(self.serialport.read(self.serialport.in_waiting))

    def stop_thread(self):
        self._running = False


if __name__ == '__main__':
    import unittest

    from common.tools_for_log import logconf

    mlog = logconf('__name__', test=True)

    from pathlib import Path
    current = Path(__file__)
    tests_dir = Path(current.parent, 'tests')
    loader = unittest.TestLoader()
    tests_suite = loader.discover(tests_dir, pattern='tests_' + current.stem + '*.py')
    testRunner = unittest.runner.TextTestRunner()
    testRunner.run(tests_suite)
